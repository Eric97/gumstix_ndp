/*
 * myKalman.h
 *
 *  Created on: 21-June-2016
 *      Author: Lin Feng
 */

#ifndef KALMAN_
#define KALMAN_

#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;


class KALMAN {

public:
	KALMAN(){};
	~KALMAN(){};

	//KalmanFilter m_KF; // Kalman Filter

	double m_Ts; // sampling time

	int InitKF(Mat state, Mat measurement); 
	int RunKF(Mat control, Mat measurement, Mat &prediction, Mat &estimated); 

	//int PredictKF(Mat &prediction, Mat &control);
	//int CorrectKF(Mat measurement, Mat &estimated);
	//int ReleaseKF();

protected:
	Mat A, B, C, D;
	Mat x;
	
	Mat P, Q, R, K; 
};

#endif
