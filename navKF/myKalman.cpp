/*
 * myKalman.cpp
 *
 *  Created on: 21-June-2016
 *      Author: Lin Feng
 */

//#include "stdafx.h"     // comment this line in linux
//#include <windows.h>	// comment this line in linux

#include <stdio.h>
#include <iostream>
#include <time.h>

#include <math.h>
#include <stdlib.h> 
#include <fstream>

#include "opencv2/opencv.hpp"
using namespace std;
using namespace cv;

#include "myKalman.h"

/**
 * Name:		InitKF
 * Function:	Initialize the Kalman Filter
 * Parameter:
 *  state : initial states
 *  measurement : initial measurement
 *  Return:		0 -- Succeed
 */
int KALMAN::InitKF(Mat state, Mat measurement)
{
	m_Ts = 0.02; // sampling time

	//-- A
	A = (Mat_<double>(9, 9) << 1,	0,		0,		m_Ts,     0,       0,		0,		0,		0,  
		                       0,	1,		0,		   0,  m_Ts,       0,		0,		0,		0, 
							   0,	0,		1,         0,     0,    m_Ts,		0,		0,		0, 
							   0,	0,		0,		   1,     0,       0,	 m_Ts,      0,      0,
							   0,   0,      0,         0,     1,       0,       0,   m_Ts,      0,
							   0,   0,      0,         0,     0,       1,       0,      0,   m_Ts,
							   0,   0,      0,         0,     0,       0,       1,      0,      0,
							   0,   0,      0,         0,     0,       0,       0,      1,      0,
							   0,   0,      0,         0,     0,       0,       0,      0,      1); 



	//-- B
	B = (Mat_<double>(9, 3) <<    0,		   0,       0,       
		                          0,		   0,       0,         
							      0,		   0,       0, 
							   m_Ts,           0,       0,
								  0,		m_Ts,       0,
								  0,           0,    m_Ts,
								  0,		   0,       0,       
		                          0,		   0,       0,         
							      0,		   0,       0); 
		
	//-- C
	C = (Mat_<double>(6, 9) <<  1,   0,      0,         0,     0,       0,		0,     0,       0,
		                        0,   1,      0,         0,     0,       0,		0,     0,       0,
							    0,   0,      1,         0,     0,       0,		0,     0,       0,
								0,   0,      0,         1,     0,       0,		0,     0,       0,
		                        0,   0,      0,         0,     1,       0,		0,     0,       0,
							    0,   0,      0,         0,     0,       1,		0,     0,       0); 

	
	//-- set Q, R and P
	Q = Mat::zeros(9, 9, CV_64F);
	Q.at<double>(0,0) = 0.005;
	Q.at<double>(1,1) = 0.005;
	Q.at<double>(2,2) = 0.005;
	Q.at<double>(3,3) = 0.0218;
	Q.at<double>(4,4) = 0.0221;
	Q.at<double>(5,5) = 0.0109;
	Q.at<double>(6,6) = 0.001;
	Q.at<double>(7,7) = 0.001;
	Q.at<double>(8,8) = 0.001;

	R = Mat::zeros(6, 6, CV_64F);	
	/* for normal GPS */
	/*
	R.at<double>(0,0) = 10;   
	R.at<double>(1,1) = 10;
	R.at<double>(2,2) = 10;
	R.at<double>(3,3) = 0.25;
	R.at<double>(4,4) = 0.25;
	R.at<double>(5,5) = 0.25;*/

	/* for RTK */
	R.at<double>(0,0) = 0.0318;
	R.at<double>(1,1) = 0.0256;
	R.at<double>(2,2) = 0.0026;
	R.at<double>(3,3) = 0.057;
	R.at<double>(4,4) = 0.082;
	R.at<double>(5,5) = 0.037;

	P = Mat::eye(9, 9, CV_64F); 
	P.at<double>(0,0) = 10;
	P.at<double>(1,1) = 10;
	P.at<double>(2,2) = 10;
	P.at<double>(3,3) = 1;
	P.at<double>(4,4) = 1;
	P.at<double>(5,5) = 1;
	P.at<double>(6,6) = 0.1;
	P.at<double>(7,7) = 0.1;
	P.at<double>(8,8) = 0.1;

	//-- set initial values of the states
	x = Mat::zeros(9,1, CV_64F);
	state.copyTo(x);
	return true; 
}


/**
 * Name:		RunKF
 * Function:	Run the Kalman Filter
 * Parameter:
 *  control ------------- control inputs
 *  measurement --------- measurements
 *  prediction ---------- prediction
 *  estimated ----------- estimation
 *  Return:		0 -- Succeed
 */
int KALMAN::RunKF(Mat control, Mat measurement, Mat &prediction, Mat &estimated)
{
	Mat u = control; 
	Mat z = measurement;

	// predict
	x  = A * x + B * u;

	x.copyTo(prediction);

	// estimated covariance
	P = A * P * A.t() + Q;

	// update
	Mat S = Mat::zeros(3, 3, CV_64F);
	
	S = C * P * C.t() + R; 

	K = P * C.t() * S.inv();

	x = x + K * ( z - C * x );

	P = P - K * C * P; 

	// output
	x.copyTo(estimated);

	return true; 
}
