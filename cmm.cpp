//cmm.cpp
//this is the implementation file for class clsCMM, which is responsible for the wireless modem communication with the ground station
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <termios.h>
#include <pthread.h>
#include <stdio.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <netinet/in.h>
//#include <hw/inout.h>

#include "uav.h"
#include "main.h"
#include "cmm.h"
#include "state.h"
#include "daq.h"
#include "svo.h"
#include "ctl.h"

#include "im8.h"
#include "aes_crypto.h"

extern clsState _state;
extern double Pmeasureall[12];
extern double Vmeasureall[12];
extern double Vdesireall[12];
extern double Psimeasureall[4];
extern int UAV1cnt,UAV2cnt,UAV3cnt;

BOOL clsCMM::Open()
{
//	return FALSE;
//	m_nsCMM = open("/dev/ser4", O_RDWR |O_NONBLOCK);

	if (_main.GetLionHubVersion() == LIONHUB_OLD) {
		m_nsCMM = open("/dev/serusb1", O_RDWR | O_NONBLOCK);
		printf("[CMM] wireless modem (/dev/serusb1) successfully opened\n");
	}
	else if (_main.GetLionHubVersion() == LIONHUB_NEW) {
		m_nsCMM = open("/dev/ser1", O_RDWR | O_NONBLOCK);
		printf("[CMM] wireless modem (/dev/ser1) successfully opened\n");
	}

	if (m_nsCMM == -1) { m_nsCMM = 0; return FALSE; }

	termios termCMM;
    tcgetattr(m_nsCMM, &termCMM);

	cfsetispeed(&termCMM, CMM_BAUDRATE);				//input and output baudrate
	cfsetospeed(&termCMM, CMM_BAUDRATE);

	termCMM.c_cflag = CS8 | CLOCAL | CREAD;
	termCMM.c_iflag = IGNBRK |IGNCR |IGNPAR;

	tcsetattr(m_nsCMM, TCSANOW, &termCMM);
	tcflush(m_nsCMM, TCIOFLUSH);

	return TRUE;
}

void clsCMM::Close()
{
	close(m_nsCMM);
}

void clsCMM::MakeTelegraph(TELEGRAPH *pTele, short code, double time, const void *pData, int nDataSize)
{
	char *pBuffer = pTele->content;

	pBuffer[0] = 0xFF;
	pBuffer[1] = 0x80 | _HELICOPTER;
	pBuffer[2] = 0x80 | ID_STATION;				//ground station

//	(short &)pBuffer[2] = nDataSize+10;
//	(short &)pBuffer[4] = code;
//	(double &)pBuffer[6] = time;

	PUTWORD(pBuffer+3, nDataSize+10);
	PUTWORD(pBuffer+5, code);
	PUTDOUBLE(pBuffer+7, time);

	::memcpy(pBuffer+15, pData, nDataSize);

	// encrypt the data
	unsigned char secreteData[2048];
	aes_ctf128_encrypt("mykey.txt", (unsigned char*)pBuffer + 5, nDataSize + 10, secreteData);
	memcpy(pBuffer + 5, secreteData, nDataSize + 10);
	// END

	unsigned short sum  = CheckSum(pBuffer+5, 10+nDataSize);
	PUTWORD(pBuffer+15+nDataSize, sum);

	pTele->size = nDataSize + 17;
}

void clsCMM::MakeTelegraph(TELEGRAPH *pTele, short code, double time, const void *pData, int nDataSize, int toID)
{
	char *pBuffer = pTele->content;

	pBuffer[0] = 0xFF;
	pBuffer[1] = 0x80 | _HELICOPTER;
	pBuffer[2] = 0x80 | toID;				// sendto ID

//	(short &)pBuffer[2] = nDataSize+10;
//	(short &)pBuffer[4] = code;
//	(double &)pBuffer[6] = time;

	PUTWORD(pBuffer+3, nDataSize+10);
	PUTWORD(pBuffer+5, code);
	PUTDOUBLE(pBuffer+7, time);

	::memcpy(pBuffer+15, pData, nDataSize);

	// encrypt the data
	unsigned char secreteData[2048];
	aes_ctf128_encrypt("mykey.txt", (unsigned char*)pBuffer + 5, nDataSize + 10, secreteData);
	memcpy(pBuffer + 5, secreteData, nDataSize + 10);
	// END

	unsigned short sum  = CheckSum(pBuffer+5, 10+nDataSize);
	PUTWORD(pBuffer+15+nDataSize, sum);

	pTele->size = nDataSize + 17;
}

/*void clsCMM::MakeTelegraph(TELEGRAPH *pTele, short code, double time, void *pData, int nDataSize)
{
	//telegraph format
	// from, to, size, (COMMAND_DATA, datatype, time, data), checksum				//bracketed is the package
	// 1      1    2        2            2       8     var    2
	char *pBuffer = pTele->content;

//	(double &)pBuffer[6] = time;
	pBuffer[0] = _HELICOPTER;				//_nHelicopter 6 or 7, 0x56 for helion, 0x57 for shelion
	pBuffer[1] = ID_STATION;				//ground station

	(short &)pBuffer[2] = nDataSize+12;
	(short &)pBuffer[4] = COMMAND_DATA;
	(short &)pBuffer[6] = code;				//code - data type

	double* pTime = &time;
	(double &)pBuffer[8] = time;
	::memcpy(pBuffer+16, pData, nDataSize);
	(unsigned short &)pBuffer[16+nDataSize] = CheckSum(pBuffer+4, 12+nDataSize);

	pTele->size = nDataSize + 18;
}
*/
BOOL clsCMM::InitThread()
{
	m_nBuffer = 0;
	m_cmd.code = 0;
	m_bVicon = FALSE;
	m_nMessage = 0;
	m_bCMMFlag = TRUE;
	m_myDecodeState = BEGIN;
	printf("[CMM] Start %d\n", pthread_self());

	return TRUE;
}

void clsCMM::SendAllMessages()
{
	pthread_mutex_lock(&m_mtxMessage);

	for (int i=0; i<=m_nMessage-1; i++) {
		SendMessage(m_szMessage[i]);
	}
	m_nMessage = 0;

	pthread_mutex_unlock(&m_mtxMessage);
}

int clsCMM::EveryRun()
{
    if (_cps != 13000000)
    	printf("[CMM1] _cps corrupted\n");

	TELEGRAPH tele;

	if (m_nCount % COUNT_CMM != 0 || (m_nCount == 0) || !GetCMMFlag()) return TRUE;

//	char a[4] = {0x55, 0x55, 0x55, 0x55};
//	write(m_nsCMM, a, 4);
//	return TRUE;
	SendAllMessages();

	double tState = _state.GetStateTime();
	UAVSTATE &state = _state.GetState();
	CHKSTATE &chkState = _state.GetChkState();
	UTCTIME utcTime = _state.GetUTCTime();
	chkState.battery = _svo.GetBatVoltage();

	BOOL bCMMState = tState > 0;
    if (bCMMState) {
    	CMM_STATE cmmState;
    	cmmState.time = tState;
    	cmmState.x = state.x; cmmState.y = state.y; cmmState.z = state.z;
    	cmmState.u = state.u; cmmState.v = state.v; cmmState.w = state.w;
    	cmmState.a = state.a; cmmState.b = state.b; cmmState.c = state.c;
    	cmmState.utcHour = utcTime.hour; cmmState.utcMin = utcTime.minutes; cmmState.utcSec = utcTime.seconds;
    	cmmState.nSatellites = chkState.nSatellites;
    	cmmState.battery = chkState.battery;
    	cmmState.latitude = state.latitude; cmmState.longitude = state.longitude; cmmState.altitude = state.altitude;

		MakeTelegraph(&tele, DATA_STATE_LOWOVERHEAD, tState, &cmmState, sizeof(CMM_STATE));
		SendTelegraph(&tele);
    }

	double tDAQ = _daq.GetDAQTime();
	BOOL bCMMDAQ = tDAQ > 0;
	if (bCMMDAQ) {
		MakeTelegraph(&tele, DATA_DAQ, tDAQ, &_daq.GetDAQData(), sizeof(DAQDATA));
//		SendTelegraph(&tele);
    }

	double tSVO = _svo.GetSVOTime();
	BOOL bCMMSVO = tSVO > 0;
	if (bCMMSVO) {
/*		CMM_SVOMAN cmmSvo;
		SVODATA svo = _svo.GetSVOData();
		cmmSvo.aileron = svo.aileron; cmmSvo.elevator = svo.elevator; cmmSvo.auxiliary = svo.auxiliary;
		cmmSvo.rudder = svo.rudder; cmmSvo.throttle = svo.throttle; cmmSvo.sv6 = svo.sv6;

		MakeTelegraph(&tele, DATA_SVO_LOWOVERHEAD, tSVO, &cmmSvo, sizeof(CMM_SVOMAN));
		SendTelegraph(&tele);*/

		CMM_NAV_STATE navState = _state.GetNAVState();
		int nGumstixNavMode = _svo.GetCurrentCTLMode();
		char buf[32] = GITVERSION;
		::memcpy(navState.verGumstix, buf, 32);
		navState.gumstixNavState = (uint8_t)nGumstixNavMode;
		navState.IMUStatus = (uint32_t)state.rfb;
//		printf("[CMM] nav state: PX4 main state %d, PX4 nav state %d, PX4 arm state %d, Gumstix state %d, Gumstix version %s\n",
//				navState.mainState, navState.navState, navState.armState, navState.gumstixNavState, navState.verGumstix);
		MakeTelegraph(&tele, DATA_NAV_STATE, tSVO, &navState, sizeof(CMM_NAV_STATE));
		SendTelegraph(&tele);
	}

	double tSIG = _state.GetSIGTime();
	BOOL bCMMSIG = tSIG > 0;
	if (bCMMSIG) {
		CMM_SVOSIG cmmSig;
		HELICOPTERRUDDER sig = _state.GetSIG();
		cmmSig.aileron = sig.aileron; cmmSig.elevator = sig.elevator; cmmSig.auxiliary = sig.auxiliary;
		cmmSig.rudder = sig.rudder; cmmSig.throttle = sig.throttle;

		MakeTelegraph(&tele, DATA_SIG_LOWOVERHEAD, tSIG, &cmmSig, sizeof(CMM_SVOSIG));
//		SendTelegraph(&tele);
	}

    if (_cps != 13000000)
    	printf("[CMM2] _cps corrupted\n");

#if (_DEBUG & DEBUGFLAG_CMM)
	char szCMM[256];
	strcpy(szCMM, "[CMM] Send data,");
//	if (bCMMGP6) strcat(szCMM, " GP6");
//	if (bCMMAH6) strcat(szCMM, " AH6");
//	if (bCMMSC6) strcat(szCMM, " SC6");
	if (bCMMState) strcat(szCMM, " STATE");
	if (bCMMDAQ) strcat(szCMM, " DAQ");
	if (bCMMSVO) strcat(szCMM, " SVO");
	if (bCMMEQU) strcat(szCMM, " EQU ");
	if (bCMMSIG) strcat(szCMM, " SIG");
	printf("%s\n", szCMM);
#endif
	return TRUE;
}

void clsCMM::ExitThread()
{
	printf("[CMM] quit\n");
}

void *clsCMM::InputThread(void *pParameter)
{
	clsCMM *pCMM = (clsCMM *)pParameter;

	pCMM->Input();

	printf("[CMM input] thread %d\n", pthread_self());
	return NULL;
}

void clsCMM::Input()
{
	printf("[CMM] Input start\n");

	ADDRESSEDPACKAGE package;
	while (1) {
		int nToRead = MAX_CMMBUFFER-m_nBuffer;
		char buffer[MAX_CMMBUFFER];
		int nRead = read(m_nsCMM, buffer, /*m_szBuffer+m_nBuffer,*/ nToRead);

		if (nRead < 0) {
			nRead = 0;				//read returns -1 on occassion of no byte read
			usleep(20000);
			continue;
		}
		printf("[CMM] ReadCommand, read byte %d, buffer size %d\n", nRead, m_nBuffer);
			for (int i=0; i<nRead; i++) {
				printf("%02x ", (unsigned char)buffer[i]);
			}
			printf("\n");

/*			write(m_nsCMM, buffer, nRead);
			continue;*/
//		m_nBuffer += nRead;

#if (_DEBUG & DEBUGFLAG_CMM)
		printf("[CMM] ReadCommand, read byte %d, buffer size %d\n", nRead, m_nBuffer);
#endif

		if (ParseBuffer(/*m_szBuffer*/buffer, nRead/*m_nBuffer*/, &package))	{
			ProcessPackage(&package);		//analysis buffer to extrace telegraph package from it
		}

		usleep(20000);
	}
}

void clsCMM::SendMessage(const char *psz)
{
	TELEGRAPH tele;

	MakeTelegraph(&tele, DATA_MESSAGE, GetTime(), psz, ::strlen(psz)+1);

	if (m_nsCMM != 0) {
		int nWrite = write(m_nsCMM, tele.content, tele.size);
//		unsigned char *buffer = (unsigned char *)tele.content;
/*		printf("[CMM] %d bytes sent, data %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n", nWrite,
				buffer[0], buffer[1], buffer[2], buffer[3], buffer[4],
				buffer[5], buffer[6], buffer[7], buffer[8], buffer[9]);*/
	}
//	else if (m_socket != 0) SendViaNet("255.255.255.255", tele.content, tele.size);
//	else if (m_socket != 0) SendViaNet(m_gcsIPAddr, /*GCS_IPADDR,*/ tele.content, tele.size);

	printf("[CMM] Message sent - %s\n", psz);
}

void clsCMM::SendRawMessage(char *message) {
	if (m_nsCMM != 0) {
		write(m_nsCMM, message, strlen(message));
	}
}

void clsCMM::SendPackage(ADDRESSEDPACKAGE *pPackage)
{
	TELEGRAPH tele;
	MakeTelegraph(pPackage, &tele);

	SendTelegraph(&tele);
}

void clsCMM::SendTelegraph(TELEGRAPH *pTele)
{
	if (m_nsCMM != 0) {
		int nWrite = write(m_nsCMM, pTele->content, pTele->size);
//		printf("[CMM] sent bytes %d\n", nWrite);
	}
//	else if (m_socket != 0) SendViaNet(m_gcsIPAddr, /*GCS_IPADDR,*/ pTele->content, pTele->size);
}

void clsCMM::MakeTelegraph(ADDRESSEDPACKAGE *pPackage, TELEGRAPH *pTele)
{
	int size = pPackage->package.size;

	pTele->size = size+6;

	// encrypt the data
	unsigned char secreteData[2048];
	aes_ctf128_encrypt("mykey.txt", (unsigned char*) pPackage->package.content, pPackage->package.size, secreteData);
	memcpy(pPackage->package.content, secreteData, pPackage->package.size);
	// END

	unsigned short sum = CheckSum(pPackage->package.content, pPackage->package.size);

	char *content = pTele->content;
	content[0] = 0x80 | pPackage->from;				//ground station
	content[1] = 0x80 | pPackage->to;

	PUTWORD(content, size);
//	(short &)content[2] = size;				//size

	::memcpy(content+4, pPackage->package.content, size);				//package
	PUTWORD(content+4+size, sum);
//	(unsigned short &)content[4+size] = sum;				//check
}

int clsCMM::SendViaNet(const char *host, char *buffer, int nBuffer)
{
	struct sockaddr_in to;
	to.sin_family = AF_INET;
	to.sin_port = htons(/*NETPORT_BROADCAST*/4404);

	to.sin_addr.s_addr = ::inet_addr(host);
	memset(to.sin_zero, '\0', sizeof(to.sin_zero));

	int nSend = sendto(m_socket, (void *)buffer, nBuffer, 0, (struct sockaddr *)&to, sizeof(to));
//	printf("[CMM] Net sent %d \n", nSend);

	return nSend;
}

void clsCMM::SendFormationData()
{
}

void clsCMM::GetHostIPaddr(char *ipaddrBuf) {
	char hostname[64] = {0};
	gethostname(hostname, 64);
	printf("Host name %s \n", hostname);

	ifaddrs *ifaddr;
	void *tmpAddr = NULL;

	getifaddrs(&ifaddr);
	while (ifaddr != NULL) {
		if (ifaddr->ifa_addr->sa_family == AF_INET) {
			tmpAddr = &((sockaddr_in *)ifaddr->ifa_addr)->sin_addr;

			if ( strcmp("mv0", ifaddr->ifa_name) == 0 ) {
				inet_ntop(AF_INET, tmpAddr, m_ipaddrBuf, INET_ADDRSTRLEN);
				printf("%s IP addr %s \n", ifaddr->ifa_name, m_ipaddrBuf);
			}
		}
		ifaddr = ifaddr->ifa_next;
	}

}

BOOL clsCMM::StartInputThread(int priority)
{
    pthread_attr_t attribute;
    pthread_attr_init(&attribute);
    pthread_attr_setdetachstate(&attribute, PTHREAD_CREATE_DETACHED);
    pthread_attr_setinheritsched(&attribute, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(&attribute, SCHED_RR);

    sched_param_t param;
    pthread_attr_getschedparam(&attribute, &param);
    param.sched_priority = priority;
    pthread_attr_setschedparam(&attribute, &param);

    pthread_create(&m_idInputThread, &attribute, &clsCMM::InputThread, this);

    return TRUE;
}

clsCMM::clsCMM()
{
	m_nsCMM = 0;

	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutex_init(&m_mtxCmd, &attr);
	pthread_mutex_init(&m_mtxMessage, &attr);
}

clsCMM::~clsCMM()
{
	pthread_mutex_destroy(&m_mtxCmd);
	printf("[clsCMM] destruct\n");
}

void clsCMM::PutMessage(char *pszMessage)
{
	pthread_mutex_lock(&m_mtxMessage);
	if (m_nMessage < MAX_MESSAGE)
		::strcpy(m_szMessage[m_nMessage++], pszMessage);
	pthread_mutex_unlock(&m_mtxMessage);
}

void clsCMM::GetCommand(COMMAND *pCmd)
{
	pthread_mutex_lock(&m_mtxCmd);

	if (m_cmd.code == 0) pCmd->code = 0;
	else {
		*pCmd = m_cmd;
		m_cmd.code = 0;				//clear
	}

	pthread_mutex_unlock(&m_mtxCmd);
}

void clsCMM::PutCommand(COMMAND *pCmd)
{
	pthread_mutex_lock(&m_mtxCmd);

	m_cmd = *pCmd;

	pthread_mutex_unlock(&m_mtxCmd);
}

BOOL clsCMM::StartListenThread(int priority)
{
    pthread_attr_t attribute;
    pthread_attr_init(&attribute);
    pthread_attr_setdetachstate(&attribute, PTHREAD_CREATE_DETACHED);
    pthread_attr_setinheritsched(&attribute, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(&attribute, SCHED_RR);

    sched_param_t param;
    pthread_attr_getschedparam(&attribute, &param);
    param.sched_priority = priority;
    pthread_attr_setschedparam(&attribute, &param);

    pthread_t id;
    pthread_create(&id, &attribute, clsCMM::ListenThread, this);

    return TRUE;
}

void *clsCMM::ListenThread(void *pParameter)
{
	clsCMM *pCMM = (clsCMM *)pParameter;

	pCMM->Listen();

	return NULL;
}

void clsCMM::Listen()
{
	//begin receive data
	struct sockaddr_in from;
	socklen_t fromlen = sizeof(from);

	ADDRESSEDPACKAGE package;

	printf("[CMM] Net started\n");
	while (1) {
		char buffer[MAX_CMMBUFFER];
		int nRecv = ::recvfrom(m_socket, buffer, MAXSIZE_TELEGRAPH-m_nBufferNet, 0, (struct sockaddr *)&from, &fromlen);
		if (nRecv == -1) {
			printf("[CMM] socket disconnected.\n");
			break;
		}
//		printf("[CMM] %d bytes received from %s\n", nRecv, inet_ntoa(from.sin_addr));

		if (ParseBuffer(buffer, nRecv, &package)) {
			ProcessPackage(&package);		//analysis buffer to extrace telegraph package from it
		}
		//so far only process packages from ground station
	}

	::close(m_socket);
}

BOOL clsCMM::ParseBuffer(char *pBuffer, int &nBuffer, ADDRESSEDPACKAGE *pPackage)
{
	//ParseBuffer analysis if there is a package in buffer,
	//if yes, extract it to pPackage and reset the pBuffer and nBuffer and return true,
	//otherwise do nothing and return false
//	memcpy(m_szBuffer + m_nBuffer, pBuffer, nBuffer);
//	pPackage->package.size = 0;
//    char *data = m_szBuffer;
//
//	while( data - m_szBuffer < nBuffer ){
//	        if (m_myDecodeState == BEGIN){
//	            if ((unsigned char &)(*data) == 0xFF){
//	                m_myDecodeState = FROM;
//	            }
//	        }
//	        else if (m_myDecodeState == FROM){
//	            if (*data > 0 && *data <= 99 || *data & 0x80) {
//	                m_from = *data;
//	                if (m_from & 0x80) m_from &= ~0x80;				//eliminate the leading 1, new protocol
//	                m_myDecodeState = TO;				//from
//	            }
//	            else{
//	                m_myDecodeState = BEGIN;
//	            }
//	        }
//	        else if(m_myDecodeState == TO){
//	            if (*data > 0 && *data <= 99 || *data & 0x80) {
//	                m_to = *data;
//	                m_to &= ~0x80;				//eliminate the leading 1
//	                if (m_to == _HELICOPTER || m_to == ID_ALL){
//	                	m_myDecodeState = PACKAGE_SIZE;
//	                }
//	                else{
//	                	m_myDecodeState = BEGIN;
//	                }
//	            }
//	            else {
//	                m_myDecodeState = BEGIN;
//	            }
//	        }
//	        else if (m_myDecodeState == PACKAGE_SIZE){
//	            if (data + 1 - m_szBuffer ==  nBuffer) {
//	                // TODO: break from while loop
//	                break;
//	            }
//
////	            m_myPackageSize = (short &)(*data);
//	            memcpy(&m_packageSize, data, sizeof(short));
//	            if (m_packageSize < 1 || m_packageSize > MAXSIZE_PACKAGE) {				//improper package, reset
//	                m_myDecodeState = BEGIN;
//	            }
//	            else {
//	                m_myDecodeState = PACKAGE_CONTENT;
//	                data++; //compensate the one more bytes consumed by "size (2 bytes)"
//	            }
//	        }
//	        else if (m_myDecodeState == PACKAGE_CONTENT){
//				if (data + m_packageSize + 2 > m_szBuffer + nBuffer) {
//	                // TODO: break from while loop
//	                break;
//	            }
//
//	            unsigned short sum = CheckSum(data, m_packageSize);
//	            //unsigned short check = (unsigned short &)(*(data + size));
//	            unsigned short check;
//	            memcpy(&check, data+m_packageSize, sizeof(unsigned short));
//
//	            if (sum != check) {				//improper package, reset
//	                m_myDecodeState = BEGIN;
//	            }
//	            else{
//	                //success
//					m_myDecodeState = BEGIN;
//
//	                pPackage->from = m_from;
//	                pPackage->to = m_to;
//	                pPackage->package.size = m_packageSize;
//	                ::memcpy(pPackage->package.content, data, m_packageSize);
//
//	                char *package;
//	                package = pPackage->package.content;
//	                short code = (short &)package[0];
////	                if (code == DATA_MESSAGE || code == 100){
////	                    int nFrom = pPackage->from;
////	                    //qDebug() << "[processPackage] from " << nFrom;
////	                }
//
//	                data += (m_packageSize + 1);
//	            }
//	        }
//	        data++;
//			int length = data - m_szBuffer;
//	    }// end while loop
//
//		//m_myDataByteArray.remove(0, data - m_myDataByteArray.data());
//		//if (data == '\0') m_myDataByteArray.clear();
//
//		char swap[MAX_CMMBUFFER];
//		m_nBuffer = nBuffer - (data - m_szBuffer);
//		memcpy(swap, data, m_nBuffer);
//		memcpy(m_szBuffer, swap, m_nBuffer);
//
//		if (pPackage->package.size == 0) return false;
//		return true;

	char *pChar = pBuffer;
	char *pCharMax = pBuffer + nBuffer - 1;

//	enum { BEGIN, HEADER, SIZE, PACKAGE, CHECK } state = BEGIN;
	enum { BEGIN, FROM, TO, SIZE, PACKAGE, CHECK } state = BEGIN;

	char from, to;
	from = '0'; to = '0';
	short size;

	//pointers to identify packages in buffer
	//pThisPackage point to the first package in buffer, pNextPackage point the the next package
	//if there is no package in buffer, pThisPackage is null, if there is package in buffer, but not completed, then pThisPackage point to the package, but pNextPackage is null
	//if buffer contains a complete package, then pThisPackage point to the package and pNextPackage point to the first char after this package
	char *pThisPackage = NULL;
	char *pNextPackage = NULL;

	while (pChar <= pCharMax) {

		if (state == BEGIN){
			if ((unsigned char &)(*pChar) == 0xFF){
				state = FROM;
			}
			pChar++; continue;
		}

		if (state == FROM) {
			from = pChar[0];
			if ((*pChar >= 0 && *pChar <= 99) || (*pChar & 0x80)) {
				from = pChar[0];
				pThisPackage = pChar;
				if (from & 0x80) from &= ~0x80;				//eliminate the leading 1, new protocol
				state = TO;				//from
			}
			else{
				state = BEGIN;
			}
			pChar++; continue;
		}

		if (state == TO) {
			if ((*pChar >= 0 && *pChar <= 99) || (*pChar & 0x80)) {
				to = pChar[0];
				to &= ~0x80;				//eliminate the leading 1
				state = SIZE;
			}
			else if (to > 1 || to < 100) {
				pThisPackage = NULL;
				state = BEGIN;
			}
			else {
				pThisPackage = NULL;
				state = BEGIN;
			}

			pChar ++; continue;
		}

		if (state == SIZE) {
			if (pChar + 1 > pCharMax) break;					//non-complete break

//			size = (short &)(*pChar);
			memcpy(&size, pChar, sizeof(short));
			if (size < 1 || size > MAXSIZE_PACKAGE) {				//improper package, reset
				pThisPackage = NULL;
				state = BEGIN;
				continue;
			}
			else {
				state = PACKAGE;
				pChar += 2;
				continue;
			}
		}

		if (state == PACKAGE) {
			if (pChar + size + 1 > pCharMax) break;				//non-complete break;

			unsigned short sum = CheckSum(pChar, size);
			unsigned short check = GETWORD(pChar+size);

			if (sum != check) {				//improper package, reset
				pThisPackage = NULL;
				state = BEGIN;
				continue;
			}

			//success
			pPackage->from = from;
			pPackage->to = to;
			pPackage->package.size = size;
			::memcpy(pPackage->package.content, pChar, size);

			pNextPackage = pChar + size + 2;
			break;
		}
	}

	if (pThisPackage == NULL) {				//no package in buffer, clear buffer
		nBuffer = 0;
		return FALSE;
	}

	if (pNextPackage == NULL) return FALSE;				//package not complete, also return false

	if (pNextPackage > pCharMax)
		nBuffer = 0;				//buffer exactly end at this package, clear buffer
	else {				//otherwise, shift left content to the head of buffer (clear this package)
		nBuffer = pCharMax - pNextPackage + 1;
		::memcpy(pBuffer, pNextPackage, nBuffer);
	}

	return TRUE;
}

void clsCMM::ProcessPackage(ADDRESSEDPACKAGE *pPackage)
{
	if (pPackage->to != _HELICOPTER && pPackage->to != ID_ALL) return;				//drop packages not for this helicopter

	int nFrom = pPackage->from;

//	printf("[cmm] before decryption: ");
//	for (int i = 0; i < pPackage->package.size; i++){
//		printf("%02x ", pPackage->package.content[i]);
//	}
//	printf("\n");

    unsigned char recover[4096];
    aes_ctf128_decrypt("mykey.txt", (unsigned char*)pPackage->package.content, pPackage->package.size, recover); //decrypt data
    memcpy(pPackage->package.content, recover, pPackage->package.size);
//    printf("[cmm] after decryption: ");
//    for (int i = 0; i < pPackage->package.size; i++){
//    		printf("%02x ", pPackage->package.content[i]);
//    	}
//    printf("\n");

	char *package = pPackage->package.content;

	COMMAND cmd;
	cmd.code = (short &)package[0];

	if ( GetCommandString(cmd.code) != NULL ) {
		printf("[CMM] Command received %d(%s) from %d\n", cmd.code, GetCommandString(cmd.code), nFrom);

		char string[256];
		sprintf(string, "command received %d(%s) from %d", cmd.code, GetCommandString(cmd.code), nFrom);
		SendMessage(string);
	}

	switch (cmd.code) {
	case COMMAND_QUIT:
	case COMMAND_MAGTEST:
	case COMMAND_MAGSET:
	case COMMAND_HOVER:
	case COMMAND_FILTER:
	case COMMAND_LAND:
	case COMMAND_ENGINEUP:
	case COMMAND_ENGINEDOWN:
	case COMMAND_EMERGENCY:
	case COMMAND_EMERGENCYGROUND:
	case COMMAND_NOTIFY:
	case COMMAND_NONOTIFY:
	case COMMAND_LEADERUPDATE:
	case COMMAND_RETURNHOME:
		break;

	case COMMAND_LIFT:
	case COMMAND_DESCEND:
	case COMMAND_ENGINE:
		COPYDOUBLE(cmd.parameter, package+2);
		break;

	case COMMAND_TAKEOFF:
		::memcpy(cmd.parameter, package+2, sizeof(double));
		break;

	case COMMAND_HEADTO:
	case COMMAND_COORDINATE:
		::memcpy(cmd.parameter, package+2, 3*sizeof(double));
		break;
	case COMMAND_FLYTOGPS:
		::memcpy(cmd.parameter, package+2, 2*sizeof(double));
		break;

	case COMMAND_RUN:
	case COMMAND_PARA:
	case COMMAND_TEST:				//parameter is an integer
	case COMMAND_GPATH: {
//		(int &)cmd.parameter[0] = (int &)package[2];
		COPYLONG(cmd.parameter, package+2);
		int nTest = (int &)package[2];
		if (nTest == SYSTEM_REBOOT) {
			printf("System is going to shutdown and reboot...\n");
			int rc = 0;
			rc = system("shutdown");
//			execv("/proc/boot/shutdown", NULL);
		}
		break;
	}

	case COMMAND_FORMATION: {		// parameter is the path number of the leader
//	case COMMAND_ATTACK:		// for later cooperative behavior usage
//		(COOP_PKT &)cmd.parameter[0] = (COOP_PKT &)package[2]; //extract coop_pkt from CMM
//		_net.ToggleNetStart();
		COPYLONG(cmd.parameter, package+2);
		int nFormation = GETLONG(package+2);
		if (nFormation == 1)
			SetGPS0Flag();
		else if (nFormation == 2)
			SetFormationFlag();
		else if (nFormation == 0)
			ResetFormationFlag();
		break;
	}

	case COMMAND_STOPFORMATION:
//		_coop.ResetConnectFlag();
		ResetFormationFlag();
		break;

	case COMMAND_PATH:
//		(int &)cmd.parameter[0] = (int &)package[2];			//path id
//		(int &)cmd.parameter[4] = (int &)package[6];			//tracking option
		COPYLONG(cmd.parameter, package+2);
		COPYLONG(cmd.parameter+4, package+6);
		break;

	case COMMAND_TRACK:
		::memcpy(cmd.parameter, package+2, 2*sizeof(int)+3*sizeof(double));
		break;

	case COMMAND_DYNAMICPATH:
	case COMMAND_DYNAMICPATHRESET:
		::memcpy(cmd.parameter, package+2, 5*sizeof(double));				//t,x,y,z,c
		break;

	case COMMAND_HFLY:				//parameter (u,v,h,r)
	case COMMAND_FLY:				//parameter (u,v,w,r)
	case COMMAND_HOLD:				//(x,y,z,c)
	case COMMAND_HOLDPI:
	case COMMAND_CFLY:				//(u,v,w,r)
	case COMMAND_TAKEOFFA:			//(x,y,z,c)
	case COMMAND_LANDA:				//(x,y,z,c)
		::memcpy(cmd.parameter, package+2, 4*sizeof(double));
		break;

	case COMMAND_CHIRP:
		::memcpy(cmd.parameter, package+2, sizeof(int)+4*sizeof(double));
		break;

	case COMMAND_PLAN:
//		(int &)cmd.parameter[0] = (int &)package[2];
		COPYLONG(cmd.parameter, package+2);
		printf("[CMM] Command plan, parameter %d\n", (int)GETLONG(package+2));
		break;

	case COMMAND_CAMRUN:
//		_state.m_visionFilter.SetFusionFlag();
//		_state.SetStateFromVisionFlag();
		break;

	case COMMAND_CAMSTOP: break;
	case COMMAND_CAMQUIT: break;
	case COMMAND_CAMTRACK:	break;

	case COMMAND_CAMVBS:
//		_state.EnableVision();
	case COMMAND_CAMTHOLD:
	    ::memcpy(cmd.parameter, package+2, sizeof(double));
		break;

	case COMMAND_MODE:
		COPYLONG(cmd.parameter, package+2);
		printf("[CMM] Command mode, parameter %d\n", (int)GETLONG(package+2));
		break;

	case DATA_VICON: {
		SetViconFlag();
		_state.SetbMeasurementUpdate();
//		AccountCMMRecord(nFrom);
		VICON_DATA viconData;
		int nObjLen = GETLONG(package+12);
//		viconData.x = GETDOUBLE(package+16+nObjLen); viconData.y = GETDOUBLE(package+16+nObjLen+8); viconData.z = GETDOUBLE(package+16+nObjLen+16);
//		viconData.a = GETDOUBLE(package+16+nObjLen+24); viconData.b = GETDOUBLE(package+16+nObjLen+32); viconData.c = GETDOUBLE(package+16+nObjLen+40);
		memcpy(&viconData, package+16+nObjLen, sizeof(VICON_DATA));
		CollectViconData(viconData);
		return;
		break;
	}
	default:
		break;				//for unknown command only code is transfered
	}

	PutCommand(&cmd);
}

BOOL clsCMM::InitSocket()
{
	m_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if ( m_socket == -1 ) {
		printf("[CMM] Error in creating socket.\n");
		return FALSE;
	}

	BOOL bBroadcast = TRUE;
	::setsockopt(m_socket, SOL_SOCKET, SO_BROADCAST, &bBroadcast, sizeof(BOOL));				//for broadcasting

	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
//	in_addr inaddr;
//	inet_aton("192.168.2.3", &inaddr);
//	addr.sin_addr.s_addr = inaddr.s_addr;

	addr.sin_port = htons(/*NETPORT_BROADCAST*/ 4404);
	memset( addr.sin_zero, '\0', sizeof(addr.sin_zero) );

	if ( bind(m_socket, (sockaddr *)&addr, sizeof(addr)) == -1 ) {
		printf("[CMM] Bind error\n");
		return FALSE;
	}

	char fileName[] = "quadlion.txt";
	if ( !GetGCSIPAddr(fileName, m_gcsIPAddr) ) {
		printf("[CMM] GCS IP address cannot be found!\n");
		return FALSE;
	}

	//init buffer
	m_nBufferNet = 0;
	m_bGPS0 = FALSE;
	GetHostIPaddr(m_ipaddrBuf);
	memset(m_cmmRcd, 0, sizeof(CMM_RECORD));
	return TRUE;
}

BOOL clsCMM::GetGCSIPAddr(char *fileName, char *gcsIPAddr)
{
	FILE *pFile = fopen(fileName, "r");
	if (pFile == NULL) {
		printf("GCS IP File cannot be found!\n");
		return FALSE;
	}

	char linebuf[128];
	while ( fgets(linebuf, 128, pFile) != NULL ) {
		if (strstr(linebuf, "GCS_IP_ADDR = ") != NULL) {
//			fputs(linebuf, stdout);
			if (sscanf(linebuf, "GCS_IP_ADDR = %s", gcsIPAddr) == 1)
				printf("GCS IP addr is %s\n", gcsIPAddr);
		}
	}
	fclose(pFile);

	return TRUE;
}

void clsCMM::CollectViconData(VICON_DATA viconData)
{
	m_viconData.x = viconData.x/1000.0; m_viconData.y = viconData.y/1000.0; m_viconData.z = viconData.z/1000.0;
	m_viconData.a = viconData.a; m_viconData.b = viconData.b; m_viconData.c = viconData.c;
//	printf("Vicon data: x %.3f, y %.3f, z %.3f, a %.3f, b %.3f, c %.3f \n",
//			m_viconData.x, m_viconData.y, m_viconData.z, m_viconData.a, m_viconData.b, m_viconData.c);
}

void clsCMM::AccountTCPIPRecord(char *ipaddrbuf) {
	if ( strcmp(IPADDR_UAV1, ipaddrbuf) == 0 ) 	m_tcpipRcd[0].nRcvPkt ++;
	else if ( strcmp(IPADDR_UAV2, ipaddrbuf) == 0) m_tcpipRcd[1].nRcvPkt ++;
	else if ( strcmp(IPADDR_UAV3, ipaddrbuf) == 0) m_tcpipRcd[2].nRcvPkt ++;
}

void clsCMM::AccountCMMRecord(int nFromID) {
	if (nFromID == ID_UAV1) {
		m_cmmRcd[0].nSenderID = nFromID;	m_cmmRcd[0].nRcvPkt ++;
	}
	else if (nFromID == ID_UAV2) {
		m_cmmRcd[1].nSenderID = nFromID;	m_cmmRcd[1].nRcvPkt ++;
	}
	else if (nFromID == ID_UAV3) {
		m_cmmRcd[2].nSenderID = nFromID;	m_cmmRcd[2].nRcvPkt ++;
	}

}

void clsCMM::CollectFormationData(int nFrom, FORMATION_DATA formationData)
{

}

void clsCMM::SetCmmConfig(char *devPort, short size, short flag, int baudrate)
{
	memcpy(m_cmmConfig.devPort, devPort, size);
	m_cmmConfig.flag = flag;
	m_cmmConfig.baudrate = baudrate;
}

void clsCMM::SetTcpConfig(char *ipaddr, short size, int portNum, short protocol, short flag)
{
	memcpy(m_tcpConfig.ipaddress, ipaddr, size);
	m_tcpConfig.portNum = portNum;
	m_tcpConfig.protocol = protocol;
	m_tcpConfig.flag = flag;
}
