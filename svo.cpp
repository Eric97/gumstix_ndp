//svo.cpp
//implementation file for servo related class & functions

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
using namespace std;

#include "matrix.h"

#include "uav.h"
#include "svo.h"
#include "state.h"
#include "ctl.h"

extern clsState _state;
extern EQUILIBRIUM _equ_Hover;
extern int ID_QUADLION;
BOOL clsSVO::InitThread()
{
	if (_HELICOPTER == ID_QUADLION) {
	    m_nsSVO = open("/dev/ser2", O_RDWR | O_NONBLOCK);
	    if (m_nsSVO == -1) { printf("[SVO] Open servo serial port (/dev/ser2) failed.\n"); return FALSE; }

	    termios termSVO;
	    tcgetattr(m_nsSVO, &termSVO);

		cfsetispeed(&termSVO, 230400 /*SVO_BAUDRATE*/);				//input and output baudrate
		cfsetospeed(&termSVO, 230400 /*SVO_BAUDRATE*/);

		termSVO.c_cflag = CS8 | CLOCAL | CREAD;
	//	termSVO.c_iflag = IGNBRK | IGNCR | IGNPAR;

		tcsetattr(m_nsSVO, TCSANOW, &termSVO);
		tcflush(m_nsSVO, TCIOFLUSH);
		printf("[SVO] UAV100 Start\n");
	}

	usleep(10000);				//wait for 10 ms

	//init variables
	m_tSVO0 = ::GetTime();
	m_nSVO = 0;
	m_tTrimvalue = -1;
	m_trimT0 = -1;

	m_avgTrimvalue.aileron = m_svoEqu.aileron = _equ_Hover.ea;
	m_avgTrimvalue.elevator = m_svoEqu.elevator = _equ_Hover.ee;
	m_avgTrimvalue.auxiliary =  m_svoEqu.auxiliary = _equ_Hover.eu;
	m_avgTrimvalue.rudder = m_svoEqu.rudder = _equ_Hover.er;
	m_avgTrimvalue.throttle = m_svoEqu.throttle = _equ_Hover.et;

	m_svoSet = m_svoEqu;

	m_limit = 0.02;
	/* below for the online trimvalue updating */
	m_bTrimvalue = FALSE;
	m_nTrimCount = 0;

	m_ctlMode = m_ctlPrevMode = 0;
	m_nSemiOnGroundCnt = 0;
	memset(&m_mavlinkIMU, 0, sizeof(mavlink_highres_imu_t));
    memset(&m_mavlinkAttitude, 0, sizeof(mavlink_attitude_t));
	printf("[SVO] Start thread %d\n", pthread_self());
	return TRUE;
}

void clsSVO::PutCommand(COMMAND *pCmd)
{
	pthread_mutex_lock(&m_mtxCmd);
	m_cmd = *pCmd;
	pthread_mutex_unlock(&m_mtxCmd);
}

void clsSVO::GetCommand(COMMAND *pCmd)
{
	pthread_mutex_lock(&m_mtxCmd);

	if(m_cmd.code == 0)
		pCmd->code = 0;
	else
	{
		*pCmd = m_cmd;
		m_cmd.code = 0;
	}

	pthread_mutex_unlock(&m_mtxCmd);
}

void clsSVO::SetTrimvalue()
{
	m_svoEqu.aileron = 0.5*_equ_Hover.ea + 0.5*m_avgTrimvalue.aileron;
	m_svoEqu.elevator = 0.5*_equ_Hover.ee + 0.5*m_avgTrimvalue.elevator;
	m_svoEqu.auxiliary = 0.5*_equ_Hover.eu + 0.5*m_avgTrimvalue.auxiliary;
	m_svoEqu.rudder = 0.5*_equ_Hover.er + 0.5*m_avgTrimvalue.rudder;
	m_svoEqu.throttle = 0.5*_equ_Hover.et + 0.5*m_avgTrimvalue.throttle;

	m_svoSet = m_svoEqu;	// put the servo deflections to the new trimvalues

	m_tTrimvalue = ::GetTime();
}

BOOL clsSVO::ProcessCommand(COMMAND *pCmd)
{
	COMMAND &cmd = *pCmd;
//	char *paraCmd = cmd.parameter;
	BOOL bProcess = TRUE;

	switch(cmd.code)
	{
	case COMMAND_GETTRIM:
//		m_bTrimvalue = TRUE;
		/* set all variables concering get trimvalue to zeros */
		m_nTrimCount = 0;
		m_avgTrimvalue.aileron = _equ_Hover.ea;
		m_avgTrimvalue.auxiliary = _equ_Hover.eu;
		m_avgTrimvalue.elevator = _equ_Hover.er;
		m_avgTrimvalue.rudder = _equ_Hover.er;
		m_avgTrimvalue.throttle = _equ_Hover.et;
		break;

	case COMMAND_HOLD:
		m_bTrimvalue = TRUE;
		m_trimT0 = ::GetTime();
		break;

	default:
		bProcess = FALSE;
		break;
	}
	return bProcess;
}

BOOL clsSVO::ValidateTrimvalue()
{
	UAVSTATE &state = _state.GetState();
	BOOL bValid =
		::fabs(state.u) <= THRESHOLDHIGH_U &&
		::fabs(state.v) <= THRESHOLDHIGH_V &&
		::fabs(state.w) <= THRESHOLDHIGH_W &&
		::fabs(state.p) <= THRESHOLDHIGH_P &&
		::fabs(state.q) <= THRESHOLDHIGH_Q &&
		::fabs(state.r) <= THRESHOLDHIGH_R;

	return bValid;
}

int clsSVO::EveryRun()
{
    if (_cps != 13000000)
    	printf("[SVO1] _cps corrupted\n");
	/// get autocontrol signal from innerloop
	HELICOPTERRUDDER sig;
	_state.GetSIG(&sig);

	/// send to servo board
	m_svoSet.aileron = sig.aileron;
	m_svoSet.elevator = sig.elevator;
	m_svoSet.auxiliary = sig.auxiliary;
	m_svoSet.rudder = sig.rudder;
	m_svoSet.throttle = sig.throttle;

//	if (m_nCount % 50 == 0) {
//		printf("SVO throttle sig %.3f\n", sig.throttle);
//	}

	if (_state.GetSimulationType() == 1) {
		// simulation mode
		SetRudder(&m_svoSet);
	}
	else if (_state.GetSimulationType() == 0 || _state.GetSimulationType() == 2)
	{
		// flight mode
		char buf[300];
		mavlink_message_t message;

/*		mavlink_set_quad_swarm_roll_pitch_yaw_thrust_t sp;
		sp.group 		= 0;
		sp.mode 		= 2;
		sp.roll[0] 		= INT16_MAX * sig.aileron;
		sp.pitch[0] 	= INT16_MAX * sig.elevator;
		sp.yaw[0] 		= INT16_MAX * sig.rudder;
		sp.thrust[0] 	= UINT16_MAX * sig.throttle;

		sig.auxiliary = -0.65; //sin(0.2*PI*::GetTime());
		sp.roll[1] 		= INT16_MAX * sig.auxiliary;

		mavlink_msg_set_quad_swarm_roll_pitch_yaw_thrust_encode(0, 200, &message, &sp);
		unsigned len = mavlink_msg_to_send_buffer((uint8_t*)buf, &message);*/

		mavlink_gumstix_2_pxhawk_t data2PxHawk;
		memset(&data2PxHawk, 0, sizeof(data2PxHawk));

		data2PxHawk.controlSignal[0] = sig.aileron; // sin(0.2*PI*::GetTime());
		data2PxHawk.controlSignal[1] = sig.elevator;
		data2PxHawk.controlSignal[2] = sig.rudder;
		data2PxHawk.controlSignal[3] = sig.throttle;
		data2PxHawk.controlSignal[4] = sig.auxiliary;

		if (_state.GetIMUStuckFlag() || !_ctl.GetTrackingOKFlag()) {
			data2PxHawk.alt = 1;
		}
		else
			data2PxHawk.alt = 0;

		if (_ctl.GetDisarmFlag()) {
			data2PxHawk.alt = 2;
		}

		mavlink_msg_gumstix_2_pxhawk_encode(10, 200, &message, &data2PxHawk);
		unsigned len = mavlink_msg_to_send_buffer((uint8_t*)buf, &message);

		int nWrite = write(m_nsSVO, buf, len);
//		printf("[svo] write %d bytes\n", nWrite);
	}

	/// read manual servo data requested in the last loop
	BOOL bGetData = GetDataFromPIXHAWK(&m_svoRawPIXHAWK);

	TranslatePIXHAWK(&m_svoRawPIXHAWK, &m_svo0);
	SetCTLMode();

	// check the time interval between current time and last reading time
	double dt = ::GetTime() - m_tSVO0;
	if (dt > 1.0) {
		_state.m_safetyStatus.bPixhawkRcv = false;
//		printf("[SVO] Pixhawk reading stuck\n");
	}
	else
		_state.m_safetyStatus.bPixhawkRcv = true;

	if (!bGetData) return TRUE;
/*	if (m_nCount % _DEBUG_COUNT == 0) {
		printf("[SVO] manual data, ea %d, ee %d, eu %d, er %d, et %d, toggle %d\n",
		svoraw.aileron, svoraw.elevator, svoraw.auxiliary, svoraw.rudder, svoraw.throttle, svoraw.sv6);
	}*/

#if (_DEBUG & DEBUGFLAG_SVO)
if (m_nCount % _DEBUG_COUNT == 0) {
	printf("[SVO] manual data, ea %.3f, ee %.3f, eu %.3f, er %.3f, et %.3f\n",
	svo.aileron, svo.elevator, svo.auxiliary, svo.rudder, svo.throttle);
}
#endif

//	m_tSVO0 = /*m_tRetrieve*/ GetTime();
	m_svoRaw0 = m_svoRawPIXHAWK;

//	m_svo0.sv6 = svo.sv6;
//	m_svo0 = svo;

	/// copy into memory servo time, raw data and translated data
	pthread_mutex_lock(&m_mtxSVO);
	if ( (m_nSVO >= 0) && (m_nSVO < MAX_SVO) ) {
		m_tSVO[m_nSVO] = m_tSVO0;
		m_svoRaw[m_nSVO] = m_svoRaw0;
		m_svo[m_nSVO++] = m_svo0;
	}
	pthread_mutex_unlock(&m_mtxSVO);

    if (_cps != 13000000)
    	printf("[SVO2] _cps corrupted\n");

	return TRUE;
}

BOOL clsSVO::CalcNewTrim()
{
	if( ValidateTrimvalue() )
	{
		m_avgTrimvalue.aileron = SVO_WEIGHT1*m_svo0.aileron + (1-SVO_WEIGHT1)*m_avgTrimvalue.aileron;
		m_avgTrimvalue.elevator = SVO_WEIGHT1*m_svo0.elevator + (1-SVO_WEIGHT1)*m_avgTrimvalue.elevator;
		m_avgTrimvalue.auxiliary = SVO_WEIGHT1*m_svo0.auxiliary + (1-SVO_WEIGHT1)*m_avgTrimvalue.auxiliary;
		m_avgTrimvalue.rudder = SVO_WEIGHT1*m_svo0.rudder + (1-SVO_WEIGHT1)*m_avgTrimvalue.rudder;
		m_avgTrimvalue.throttle = SVO_WEIGHT1*m_svo0.throttle + (1-SVO_WEIGHT1)*m_avgTrimvalue.throttle;
		return TRUE;
	}
	return FALSE;
}

void clsSVO::ExitThread()
{
	::close(m_nsSVO);
	printf("[SVO] quit\n");
}

void clsSVO::SetRudder(HELICOPTERRUDDER *pRudder)
{
	HELICOPTERRUDDER rudder = *pRudder;
	rudder.aileron = range(rudder.aileron, -0.85, 0.85);
	rudder.elevator = range(rudder.elevator, -0.85, 0.85);
	rudder.auxiliary = range(rudder.auxiliary, -0.85, 0.85);
	rudder.rudder = range(rudder.rudder, -0.85, 0.85);
	rudder.throttle = range(rudder.throttle, -0.85, 0.85);

	CAM_PANTILT &camPanTilt = _state.GetCamPanTilt();
	double pan = camPanTilt.pan;
	double tilt = camPanTilt.tilt;
//	printf("[SVO] cam pan %.3f, tilt %.3f \n", pan, tilt);
//	printf("[SVO] aileron %.3f \n", rudder.aileron);
//	printf("[SVO] elevator %.3f \n", rudder.elevator);
//	printf("[SVO] rudder %.3f \n", rudder.rudder);
	char szSVOCommand[32]; int nPosition;
//	nPosition = 15000+::sin(0.2*PI*::GetTime())*2000;
	nPosition = 15000+(int)(5000*rudder.aileron);
	sprintf(szSVOCommand, "SV0 M%d\r", nPosition);
	write(m_nsSVO, szSVOCommand, 11);
	sprintf(szSVOCommand, " M%d\r", nPosition);
	write(m_nsSVO, szSVOCommand, 11);

	nPosition = 15000+(int)(5000*rudder.elevator);
//	nPosition = 15000+::sin(0.2*PI*::GetTime())*1000;
	sprintf(szSVOCommand, "SV4 M%d\r", nPosition);
	write(m_nsSVO, szSVOCommand, 11);
	sprintf(szSVOCommand, " M%d\r", nPosition);
	write(m_nsSVO, szSVOCommand, 11);

	nPosition = 15000+(int)(5000*rudder.throttle);
	sprintf(szSVOCommand, "SV2 M%d\r", nPosition);
	write(m_nsSVO, szSVOCommand, 11);
	sprintf(szSVOCommand, " M%d\r", nPosition);
	write(m_nsSVO, szSVOCommand, 11);

	nPosition = 15000+(int)(5000*rudder.rudder);
//	printf("Rudder: %d\n", nPosition);
//	nPosition = 15000+::sin(0.2*PI*::GetTime())*2000;
	sprintf(szSVOCommand, "SV1 M%d\r", nPosition);
	write(m_nsSVO, szSVOCommand, 11);
	sprintf(szSVOCommand, " M%d\r", nPosition);
	write(m_nsSVO, szSVOCommand, 11);

		// pan
//		nPosition = 15000+::sin(0.8*PI*::GetTime())*2000;
		double pan1 = range(pan, -PI/6, PI/6);
		nPosition = 15000 + 900 + (2000/18)*(pan1*180/PI);
		sprintf(szSVOCommand, "SV7 M%d\r", nPosition);
		write(m_nsSVO, szSVOCommand, 11);
		sprintf(szSVOCommand, " M%d\r", nPosition);
		write(m_nsSVO, szSVOCommand, 11);

		// tilt
//		nPosition = 15000+(int)(5000*tilt);
		double tilt1 = range(tilt + 15*PI/180, -PI/10, PI/6);
		nPosition = 15000 + 300 + (2000/18)*(tilt1*180/PI);
//		nPosition = 15000 + 300;
//		nPosition = 15000+::cos(0.8*PI*::GetTime())*2000;
		sprintf(szSVOCommand, "SV6 M%d\r", nPosition);
		write(m_nsSVO, szSVOCommand, 11);
		sprintf(szSVOCommand, " M%d\r", nPosition);
		write(m_nsSVO, szSVOCommand, 11);
}

void clsSVO::SetCamera(double camera)
{
	char szCmd[32];

	//Assert the camera is installed PI/4 down
	double angle = camera + PI/4;
	angle = range(angle, -PI/4, PI/4);

	int nPosition = 15000 + (int)(5000*angle/(PI/4));
	sprintf(szCmd, "SV5 M%d\r", nPosition);
	write(m_nsSVO, szCmd, 11);
}

void clsSVO::WriteCommand()
{
	write(m_nsSVO, "M?9\r", 4);
	write(m_nsSVO, "M?10\r",5);
	write(m_nsSVO, "M?11\r",5);
	write(m_nsSVO, "M?12\r",5);
	write(m_nsSVO, "M?13\r",5);
	write(m_nsSVO, "M?8\r", 4);

	m_tRequest = GetTime();
}

// GremLion setting
void clsSVO::WriteCommand1()
{
//	tcflush(m_nsSVO, TCIOFLUSH);

/*	write(m_nsSVO, "M?10\r",5);
	write(m_nsSVO, "M?8\r",4);
	write(m_nsSVO, "M?11\r",5);
	write(m_nsSVO, "M?9\r",4);
	write(m_nsSVO, "M?13\r",5);
	write(m_nsSVO, "M?14\r",5);*/

	write(m_nsSVO, "M?12\r",5);		// elevator
	write(m_nsSVO, "M?10\r",5);		// throttle
	write(m_nsSVO, "M?8\r",4);		// aileron
	write(m_nsSVO, "M?10\r",5);		// auxiliary
	write(m_nsSVO, "M?9\r",4);		// rudder
	write(m_nsSVO, "M?15\r",5);		// toggle
	m_tRequest = GetTime();
}

BOOL clsSVO::GetDataFromPIXHAWK(SVORAWDATA *pData) {

	mavlink_status_t lastStatus;
	lastStatus.packet_rx_drop_count = 0;

	uint8_t cp[2014];
	int count = read(m_nsSVO, cp, 2014);

//	printf("[SVO] read %d bytes \n", count);

	for (int i=0; i<count; i++)	{
		mavlink_message_t message;
		mavlink_status_t status;
		uint8_t msgReceived = false;

		// Check if a message could be decoded, return the message in case yes
		msgReceived = mavlink_parse_char(MAVLINK_COMM_1, cp[i], &message, &status);
		if (lastStatus.packet_rx_drop_count != status.packet_rx_drop_count)
		{
//			printf("ERROR: DROPPED %d PACKETS\n", status.packet_rx_drop_count);
			if (1)
			{
//				unsigned char v=cp[i];
//				fprintf(stderr,"%02x ", v);
			}
		}
		lastStatus = status;

		if (msgReceived) {

			if (0) { // for debug
				fprintf(stderr,"Received serial data: ");
				unsigned int i;
				uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
				unsigned int messageLength = mavlink_msg_to_send_buffer(buffer, &message);
				if (messageLength > MAVLINK_MAX_PACKET_LEN)
				{
					fprintf(stderr, "\nFATAL ERROR: MESSAGE LENGTH IS LARGER THAN BUFFER SIZE\n");
				}
				else
				{
					for (i=0; i<messageLength; i++)
					{
						unsigned char v=buffer[i];
						fprintf(stderr,"%02x ", v);
					}
					fprintf(stderr,"\n");
				}
			}

			static unsigned int scaled_imu_receive_counter = 0;
			switch (message.msgid) {

			case MAVLINK_MSG_ID_HIGHRES_IMU:

				mavlink_highres_imu_t imu;
				mavlink_msg_highres_imu_decode(&message, &imu);
				_state.UpdatePX4IMUState(&imu);

	 if (scaled_imu_receive_counter%10 == 0) {
//	 printf("Got message HIGHRES_IMU \n");
//	 printf("\t time: %llu\n", imu.time_usec);
//	 printf("\t acc  (NED):\t% f\t% f\t% f (m/s^2)\n", imu.xacc, imu.yacc, imu.zacc);
//	 printf("\t gyro (NED):\t% f\t% f\t% f (rad/s)\n", imu.xgyro, imu.ygyro, imu.zgyro);
//	 printf("\t mag  (NED):\t% f\t% f\t% f (Ga)\n", imu.xmag, imu.ymag, imu.zmag);
//	 printf("\t baro: \t %f (mBar)\n", imu.abs_pressure);
//	 printf("\t altitude: \t %f (m)\n", imu.pressure_alt);
//	 printf("\t temperature: \t %f C\n", imu.temperature);
//	 printf("\n");
	 }
				scaled_imu_receive_counter++;
				 m_tSVO0 = ::GetTime();
	break;

/*			case MAVLINK_MSG_ID_ATTITUDE:

				mavlink_attitude_t attitude;
				mavlink_msg_attitude_decode(&message, &attitude);

			 if (scaled_att_receive_counter%10 == 0) {
			 printf("Got message ATTITUDE \n");
			 printf("\t time: %llu\n", attitude.time_boot_ms);
			 printf("\t attitude:\t% f\t% f\t% f (rad)\n", attitude.roll, attitude.pitch, attitude.yaw);
			 printf("\t att speed:\t% f\t% f\t% f (rad/s)\n", attitude.rollspeed, attitude.pitchspeed, attitude.yawspeed);
			 printf("\n");
			 }

				scaled_att_receive_counter++;
				break;
*/

/*			case MAVLINK_MSG_ID_MANUAL_SETPOINT:

				mavlink_manual_setpoint_t manualSig;
				mavlink_msg_manual_setpoint_decode(&message, &manualSig);
	// mavlink_rc_channels_scaled_t rc_channels_scaled;
	// mavlink_msg_rc_channels_scaled_decode(&message, &rc_channels_scaled);
	//
	// printf("Got message RC_channel_scaled \n");
	// printf("attitude:\t% f\t% f\t% f (rad)\n", attitude.roll, attitude.pitch, attitude.yaw);
	// printf("att speed:\t% f\t% f\t% f (rad/s)\n", attitude.rollspeed, attitude.pitchspeed, attitude.yawspeed);
	// printf("\n");
	//

	 cout << "Got message RC_channel_scaled" << endl;
	 cout << "Ch1: " << manualSig.roll << endl;
	 cout << "Ch2: " << manualSig.pitch << endl;
	 cout << "Ch3: " << manualSig.yaw << endl;
	 cout << "Ch4: " << manualSig.thrust << endl;
	 cout << "Ch6: " << manualSig.mode_switch << endl;

	break;
*/
			case MAVLINK_MSG_ID_SYS_STATUS: {
				mavlink_sys_status_t sysStatus;
				mavlink_msg_sys_status_decode(&message, &sysStatus);
//				printf("[mavlink] main status %d, nav status %d, ver %i\n", sysStatus.main_state, sysStatus.nav_state, (uint16_t)sysStatus.version);
//				printf("[mavlink] version %d, preflightcheck %d, total time %d, this time %d\n",
//						(uint16_t)sysStatus.version, (uint16_t)sysStatus.errors_count4, (uint16_t)sysStatus.errors_count1, (uint16_t)sysStatus.errors_count2);
//				printf("[mavlink] battery voltage %.3f\n", sysStatus.voltage_battery / 1000.0f);
//				printf("[mavlink] GPS time hour %d, minutes %d, seconds %d\n",
//						sysStatus.onboard_control_sensors_enabled, sysStatus.onboard_control_sensors_health, sysStatus.onboard_control_sensors_present);
				uint16_t status = sysStatus.errors_count3;
//				printf("status in decimal %d\n", status);
				uint8_t armStatus = 0;
				if (status & 0x0001)
					armStatus = 1;
				else if (status & 0x0002)
					armStatus = 2;
				else if (status & 0x0003)
					armStatus = 3;

				uint8_t geoFence = 0;
				if (status & 0x0004)
					geoFence = 1; // soft geofence violation
				else if (status & 0x0008)
					geoFence = 2;	// hard geofence violation

				uint8_t gumstixStatus = 0;
				if (status & 0x0040)
					gumstixStatus = 1; // gumstix faulty

//				printf("[mavlink] arm status %d, geofence %d, gumstixStatus %d\n", armStatus, geoFence, gumstixStatus);

				_state.UpdatePX4UTCTime((uint8_t)sysStatus.onboard_control_sensors_enabled,
						(uint8_t)sysStatus.onboard_control_sensors_health, (uint8_t)sysStatus.onboard_control_sensors_present,
						(int)sysStatus.load);
				SetBatVoltage(sysStatus.voltage_battery / 1000.0f);
				_state.UpdateSysStatus(sysStatus.main_state, sysStatus.nav_state, sysStatus.errors_count3,
						sysStatus.errors_count4, sysStatus.version, sysStatus.errors_count1, sysStatus.errors_count2);
				break;
			}

			case MAVLINK_MSG_ID_RC_CHANNELS_RAW:
				mavlink_rc_channels_raw_t rcRaw;
				mavlink_msg_rc_channels_raw_decode(&message, &rcRaw);
//				 cout << "Got message RC_channel_raw" << endl;
//				 printf("RC raw: chan1 %d, chan2 %d, chan3 %d, chan4 %d, chan6 %d\n",
//						 rcRaw.chan1_raw, rcRaw.chan2_raw, rcRaw.chan3_raw, rcRaw.chan4_raw, rcRaw.chan6_raw);
				 pData->aileron = rcRaw.chan1_raw; pData->elevator = rcRaw.chan2_raw;
				 pData->throttle = rcRaw.chan3_raw; pData->rudder = rcRaw.chan4_raw;
				 pData->sv6 = rcRaw.chan6_raw;
				break;

			case MAVLINK_MSG_ID_LOCAL_POSITION_NED:
				mavlink_local_position_ned_t pxPos;
				mavlink_msg_local_position_ned_decode(&message, &pxPos);
				_state.UpdatePX4PosState(&pxPos);
//				printf("PX4 pos: %.3f, %.3f, %.3f\n", pxPos.vx, pxPos.vy, pxPos.vz);
				break;

			case MAVLINK_MSG_ID_ATTITUDE:
				mavlink_attitude_t pxAtt;
				mavlink_msg_attitude_decode(&message, &pxAtt);
				_state.UpdatePX4AttState(&pxAtt);
//				printf("PX4 att: %.3f, %.3f, %.3f\n", pxAtt.roll, pxAtt.pitch, pxAtt.yaw);
				break;

			case MAVLINK_MSG_ID_GLOBAL_POSITION_INT:
				#ifndef _RTK
				if ( _state.GetSimulationType() == SIMULATIONTYPE_STATE) {
					mavlink_global_position_int_t globalPosInt;
					mavlink_msg_global_position_int_decode(&message, &globalPosInt);
					_state.UpdatePX4GPS(&globalPosInt);
	//				printf("PX4 gps lat %.6f, long %.6f\n", (double)globalPosInt.lat/1e7, (double)globalPosInt.lon/1e7);
	//				printf("PX4 ground vel ug %.3f, vg %.3f, wg %.3f\n", (double)globalPosInt.vx/100, (double)globalPosInt.vy/100, (double)globalPosInt.vz/100);
				}
				#endif
				break;
	default:
	break;
			}
		}
	}

	return TRUE;
}

BOOL clsSVO::GetData(SVORAWDATA *pData)
{
	char szSVO[256];

	SVORAWDATA svo;

	int nRead = read(m_nsSVO, szSVO, 256);
	szSVO[nRead] = '\0';

	unsigned short check;
	int nScanf = sscanf(szSVO, "%hd%hd%hd%hd%hd%hd%hd",
		&svo.elevator, &svo.throttle, &svo.aileron, &svo.auxiliary, &svo.rudder, &svo.sv6, &check);

	if (nScanf != 6) {
		return FALSE;
	}

	*pData = svo;
	m_tRetrieve = m_tRequest + 0.004;

	return TRUE;
}

clsSVO::clsSVO()
{
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutex_init(&m_mtxSVO, &attr);
}

clsSVO::~clsSVO()
{
	pthread_mutex_destroy(&m_mtxSVO);
}

void clsSVO::TranslatePIXHAWK(SVORAWDATA *pSVORAW, SVODATA *pSVO)
{
/*	if (m_nCount % 50 == 0) {
		printf("[SVO] PIXHAWK raw ail %d, ele %d, thr %d, rud %d\n",
				pSVORAW->aileron, pSVORAW->elevator, pSVORAW->throttle, pSVORAW->rudder);
	}*/
	if (abs(pSVORAW->aileron - 1518.5) < 19.5) 	pSVO->aileron = 0;
	else if ((pSVORAW->aileron - 1518.5) > 19.5) 	pSVO->aileron = (double)(pSVORAW->aileron - 1518.5 - 19.5)/400.0;
	else if ((pSVORAW->aileron - 1518.5) < -19.5) 	pSVO->aileron = (double)(pSVORAW->aileron - 1518.5 + 19.5)/400.0;

	if (abs(pSVORAW->elevator - 1518.5) < 19.5) 	pSVO->elevator = 0;
	else if ((pSVORAW->elevator - 1518.5) > 19.5) 	pSVO->elevator = (double)(pSVORAW->elevator - 1518.5 - 19.5)/400.0;
	else if ((pSVORAW->elevator - 1518.5) < -19.5) pSVO->elevator = (double)(pSVORAW->elevator - 1518.5 + 19.5)/400.0;

	if (abs(pSVORAW->throttle - 1518.5) < 19.5) 	pSVO->throttle = 0;
	else if ((pSVORAW->throttle - 1518.5) > 19.5) 	pSVO->throttle = (double)(pSVORAW->throttle - 1518.5 - 19.5)/400.0;
	else if ((pSVORAW->throttle - 1518.5) < -19.5) pSVO->throttle = (double)(pSVORAW->throttle - 1518.5 + 19.5)/400.0;

	if (abs(pSVORAW->rudder - 1518.5) < 19.5) 		pSVO->rudder = 0;
	else if ((pSVORAW->rudder - 1518.5) > 19.5) 	pSVO->rudder = (double)(pSVORAW->rudder - 1518.5 - 19.5)/400.0;
	else if ((pSVORAW->rudder - 1518.5) < -19.5)	pSVO->rudder = (double)(pSVORAW->rudder - 1518.5 + 19.5)/400.0;

	pSVO->sv6 = (double)((pSVORAW->sv6) - 1518.5) / 400.0;
}

void clsSVO::SetCTLMode() {
	if (m_svo0.sv6 <= -0.3) {
		m_ctlMode = CTL_MODE_MANUAL;
//		printf("MODE: manual\n");
	}
	else if (m_svo0.sv6 >= -0.2 && m_svo0.sv6 <= 0.2) {
		m_ctlMode = CTL_MODE_SEMIAUTO;
//		printf("MODE: semi\n");
	}
	else if (m_svo0.sv6 > 0.2 && m_svo0.sv6 < 1.2) {
		m_ctlMode = CTL_MODE_AUTO;
//		printf("MODE: auto\n");
	}
	else {
		m_ctlMode = CTL_MODE_AUTO;
		printf("MODE: unknown\n");
	}

	int nUAVStatus = _ctl.GetUAVStatus();
	if (m_ctlMode == CTL_MODE_SEMIAUTO && nUAVStatus == STATUS_GROUND && m_svo0.throttle >= -0.1) {
		m_nSemiOnGroundCnt++;
//		printf("Semi on Ground takeoff %d\n", m_nSemiOnGroundCnt);
		if (m_nSemiOnGroundCnt == 100)
			SetSemiOnGroundTimeFlag();
	}
/*	else if (m_ctlMode == CTL_MODE_SEMIAUTO && nUAVStatus == STATUS_GROUND && m_svo0.throttle < -0.1) {
		m_nSemiOnGroundCnt = 0;
	}*/
	else if (nUAVStatus == STATUS_AIR) {
//		printf("Reset semi counter to 0\n");
		m_nSemiOnGroundCnt = 0;
	}

	int nModeTransit = 0;
	if ( m_ctlMode == CTL_MODE_AUTO && m_ctlPrevMode == CTL_MODE_SEMIAUTO )
		nModeTransit = CTL_MODE_TRANSIT_SEMI2AUTO;
	else if ( m_ctlMode == CTL_MODE_AUTO && m_ctlPrevMode == CTL_MODE_MANUAL )
		nModeTransit = CTL_MODE_TRANSIT_MANUAL2AUTO;

	else if ( m_ctlMode == CTL_MODE_SEMIAUTO && m_ctlPrevMode == CTL_MODE_AUTO )
	{
		if (nUAVStatus == STATUS_AIR) {
			if (_svo.GetSVOData().throttle > -0.3)
				nModeTransit = CTL_MODE_TRANSIT_AUTO2SEMI;
		}
		else
			nModeTransit = CTL_MODE_TRANSIT_AUTO2SEMI; // in the ground status
	}
	else if ( m_ctlMode == CTL_MODE_SEMIAUTO && m_ctlPrevMode == CTL_MODE_MANUAL )
		nModeTransit = CTL_MODE_TRANSIT_MANUAL2SEMI;

	else if ( m_ctlMode == CTL_MODE_MANUAL && m_ctlPrevMode == CTL_MODE_AUTO ) {
		if (nUAVStatus == STATUS_AIR) {
			if (_svo.GetSVOData().throttle > -0.3)
				nModeTransit = CTL_MODE_TRANSIT_AUTO2MANUAL;
		}
		else
			nModeTransit = CTL_MODE_TRANSIT_AUTO2MANUAL;	// in the ground status
	}

	else if ( m_ctlMode == CTL_MODE_MANUAL && m_ctlPrevMode == CTL_MODE_SEMIAUTO )
		nModeTransit = CTL_MODE_TRANSIT_SEMI2MANUAL;

	else nModeTransit = 0;

	m_fctlModeTransit = nModeTransit;
	m_ctlPrevMode = m_ctlMode;
}

void clsSVO::Translate_HeLion(SVORAWDATA *pSVORAW, SVODATA *pSVO)
{
	pSVO->aileron = (double)(pSVORAW->aileron-15000)/5000;		// normalized to -1 ~ 1
	pSVO->elevator = (double)(pSVORAW->elevator-15000)/5000;	// normalized to -1 ~ 1
	pSVO->auxiliary = (double)(pSVORAW->auxiliary-15000)/5000;	// normalized to -1 ~ 1
	pSVO->rudder = (double)(pSVORAW->rudder-15000)/5000;		// normalized to -1 ~ 1
	pSVO->throttle = (double)(pSVORAW->throttle-10000)/10000;	// normalized to  0 ~ 1

	pSVO->sv6 = (double)(pSVORAW->sv6-15000)/5000;
}

void clsSVO::Translate_GremLion(SVORAWDATA *pSVORAW, SVODATA *pSVO)
{
	pSVO->aileron = (double)(pSVORAW->aileron-15290)/5000;		// normalized to -1 ~ 1
	pSVO->elevator = (double)(pSVORAW->elevator-15516)/5000;	// normalized to -1 ~ 1
	pSVO->auxiliary = (double)(pSVORAW->auxiliary-15000)/5000;	// normalized to -1 ~ 1
	pSVO->rudder = (double)(pSVORAW->rudder-14685)/5000;		// normalized to -1 ~ 1
	pSVO->throttle = (double)(pSVORAW->throttle-15000)/5000;	// normalized to -1 ~ 1
	pSVO->sv6 = (double)(pSVORAW->sv6-15000)/5000;
}

void clsSVO::Translate_GremLion(SVORAWDATA *pSVORAW, SVODATA *pSVO, SVORAWDATA *pSVOTrim, BOOL bManualTrim)
{
	if ( bManualTrim ) {
		pSVO->aileron = (double)(pSVORAW->aileron-pSVOTrim->aileron)/5000;		// normalized to -1 ~ 1
		pSVO->elevator = (double)(pSVORAW->elevator-pSVOTrim->elevator)/5000;		// normalized to -1 ~ 1
		pSVO->auxiliary = (double)(pSVORAW->auxiliary-15000)/5000;	// normalized to -1 ~ 1
		pSVO->rudder = (double)(pSVORAW->rudder-pSVOTrim->rudder)/5000;			// normalized to -1 ~ 1
		pSVO->throttle = (double)(pSVORAW->throttle- 15000 /*pSVOTrim->throttle*/)/5000;		// normalized to -1 ~ 1
		pSVO->sv6 = (double)(pSVORAW->sv6-15000)/5000;
	} else {
		pSVO->aileron = (double)(pSVORAW->aileron-15000)/5000;		// normalized to -1 ~ 1
		pSVO->elevator = (double)(pSVORAW->elevator-15000)/5000;		// normalized to -1 ~ 1
		pSVO->auxiliary = (double)(pSVORAW->auxiliary-15000)/5000;	// normalized to -1 ~ 1
		pSVO->rudder = (double)(pSVORAW->rudder-15000)/5000;			// normalized to -1 ~ 1
		pSVO->throttle = (double)(pSVORAW->throttle-15000)/5000;		// normalized to -1 ~ 1
		pSVO->sv6 = (double)(pSVORAW->sv6-15000)/5000;
	}
}

void clsSVO::SetManualTrimRawData(SVORAWDATA svoraw)
{
	m_svoManualTrimRaw.aileron = svoraw.aileron;
	m_svoManualTrimRaw.elevator = svoraw.elevator;
	m_svoManualTrimRaw.auxiliary = svoraw.auxiliary;
	m_svoManualTrimRaw.rudder = svoraw.rudder;
	m_svoManualTrimRaw.throttle = svoraw.throttle;

}

void clsSVO::SetSvoConfig(char *devPort, short size, short flag, int baudrate)
{
	memcpy(m_svoConfig.devPort, devPort, size);
	m_svoConfig.flag = flag;
	m_svoConfig.baudrate = baudrate;
}
