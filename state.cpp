//state.h
//this is the implementation file for clsState, implementing state update and model simulation
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
using namespace std;

#include "state.h"
#include "ctl.h"
#include "parser.h"
#include "uav.h"
#include "cmm.h"

extern clsParser _parser;
extern clsState _state;
extern clsCMM _cmm;
extern EQUILIBRIUM _equ_Hover;
extern void Quad_model(cv::Mat cmd, cv::Mat &X_n_phi, cv::Mat &X_n_theta, double &y_n_for, double &y_n_lat, double &y_n_heav, double &y_n_head, double &x_pos, double &y_pos, double &z_pos, cv::Mat &ya);
extern void matrixOmegaE(cv::Mat eulers, cv::Mat &OmegaE);
extern void matrixOmega(cv::Mat omegab, cv::Mat &Omega);
extern void matrixBgb(cv::Mat u, cv::Mat &Bgb);
extern void qmodel(cv::Mat u, cv::Mat &y);
extern void Runge_Kutta_Mat(cv::Mat A, cv::Mat B, cv::Mat &X_n, double u, double d_t);
extern void Runge_Kutta(double a, double b, double &x_n, double u, double d_t);

void clsState::UpdateSIG(HELICOPTERRUDDER *pSIG)
{
	m_tSIG0 = GetTime();
	if (pSIG != NULL) m_sig0 = *pSIG;

	pthread_mutex_lock(&m_mtxSIG);
	if ( (m_nSIG >= 0) && (m_nSIG <= MAX_SIG) ) {
		m_tSIG[m_nSIG] = m_tSIG0;
		m_sig[m_nSIG++] = m_sig0;
	}
	pthread_mutex_unlock(&m_mtxSIG);
}

void clsState::Simulate()
{
	if (m_nSimulationType == SIMULATIONTYPE_MODEL)
	{
		ModelSimulate();
	}

	if (m_nSimulationType == SIMULATIONTYPE_STATE)
//		StateSimulate();
		StateSimulationFromPX4();

	if (m_nSimulationType == SIMULATIONTYPE_DATA) {
		if (m_pData != NULL) DataSimulate();
		else StateSimulate();
	}

	//push to record
	pthread_mutex_lock(&m_mtxState);
	if (m_nState != MAX_STATE) {
		m_tState[m_nState] = m_tState0;				//record
		m_state[m_nState++] = m_state0;
	}
	pthread_mutex_unlock(&m_mtxState);

	m_nCount ++;
}

void clsState::DataSimulate()
{
	double t = GetTime();

	m_tState1 = t;

	int nSize = sizeof(double)+sizeof(UAVSTATE);
	m_state1 = (UAVSTATE &)m_pData[m_iData*nSize+8];
	if (m_iData < m_nData-1) m_iData ++;

	Filter();				//enter filter and put filtered result in m_state0
}

void clsState::StateSimulate()
{
	double t = GetTime();

#if (_DEBUG & DEBUGFLAG_SIMULATION)
if (m_nCount % _DEBUG_COUNT == 0) {
	printf("[CTL] StateSimulate, time %f, tState0 %f\n", t, m_tState0);
}
#endif

	double t2 = t - m_t0Simulation;

	double SECTION = 5;				//5 second per section

	int section = (int)floor((t2-10)/SECTION);				//empty first 10 second
	double tSection = t2 - section*SECTION;

	double signal = sin(2*PI*tSection);				//1 second per period

	m_tState0 = t;
	memset(&m_state0, 0, sizeof(UAVSTATE));
	m_state0.z = 0;

	switch (section) {
	case 0: m_state0.x = signal;	break;
	case 1: m_state0.y = signal;	break;
	case 2: m_state0.z = signal;	break;
	case 3: m_state0.u = signal;	break;
	case 4: m_state0.v = signal;	break;
	case 5: m_state0.w = signal;	break;
	case 6: m_state0.a = 0.1*signal;	break;
	case 7: m_state0.b = 0.1*signal;	break;
	case 8: m_state0.c = 0.1*signal;	break;
	case 9: m_state0.p = signal;	break;
	case 10: m_state0.q = signal;	break;
	case 11: m_state0.r = signal;	break;
	}

	if (m_state0.a > PI) m_state0.a -= 2*PI;
	else if (m_state0.a < -PI) m_state0.a += 2*PI;

	if (m_state0.b > PI) m_state0.b -= 2*PI;
	else if (m_state0.b < -PI) m_state0.b += 2*PI;

	B2G(&m_state0.a, &m_state0.u, &m_state0.ug);
}

void clsState::UpdatePX4IMUState(mavlink_highres_imu_t *px4Imu)
{
	m_statePX4.acx = px4Imu->xacc; m_statePX4.acy = px4Imu->yacc; m_statePX4.acz = px4Imu->zacc;
}

void clsState::UpdatePX4PosState(mavlink_local_position_ned_t *px4Pos)
{
	m_statePX4.x = px4Pos->x; m_statePX4.y = px4Pos->y; m_statePX4.z = px4Pos->z;
//	m_statePX4.u = px4Pos->vx; m_statePX4.v = px4Pos->vy; m_statePX4.w = px4Pos->vz;
	m_statePX4.ug = px4Pos->vx; m_statePX4.vg = px4Pos->vy; m_statePX4.wg = px4Pos->vz;
}

void clsState::UpdatePX4AttState(mavlink_attitude_t *px4Att)
{
	m_statePX4.a = px4Att->roll; m_statePX4.b = px4Att->pitch; m_statePX4.c = px4Att->yaw;
	m_statePX4.p = px4Att->rollspeed; m_statePX4.q = px4Att->pitchspeed; m_statePX4.r = px4Att->yawspeed;
}

void clsState::UpdatePX4GPS(mavlink_global_position_int_t *pGlobalPosInt)
{
//	printf("PX4 lat %.7f, lon %.7f\n", (double)pGlobalPosInt->lat / (double)1e7, (double)pGlobalPosInt->lon / (double)1e7);

	m_statePX4.latitude = ((double)pGlobalPosInt->lat / (double)1e7) * PI /180;
	m_statePX4.longitude = ((double)pGlobalPosInt->lon / (double)1e7) * PI / 180;
	m_statePX4.altitude = (double)pGlobalPosInt->alt / 1000;
	if (!m_bCoordinated) {
			Coordinate(m_statePX4.longitude, m_statePX4.latitude, m_statePX4.altitude);
			m_bCoordinated = TRUE;
	}
}

void clsState::StateSimulationFromPX4()
{
	double t = GetTime();
	if (m_tState0 < 0) {
		::memset(&m_statePX4, 0, sizeof(m_statePX4));
//		_parser.GetVariable("gps0_lat", &m_latitude0);
//		_parser.GetVariable("gps0_long", &m_longitude0);
		m_latitude0 = m_latitude0 * PI / 180; //1.29844*PI/180;
		m_longitude0 = m_longitude0 * PI / 180; //103.80729*PI/180;
	}

	::memcpy(&m_state0, &m_statePX4, sizeof(UAVSTATE));
	m_tState0 = t;

	if (m_state0.a > PI) m_state0.a -= 2*PI;
	else if (m_state0.a < -PI) m_state0.a += 2*PI;

	if (m_state0.b > PI) m_state0.b -= 2*PI;
	else if (m_state0.b < -PI) m_state0.b += 2*PI;

	G2B(&m_state0.a, &m_state0.ug, &m_state0.u);
}

void clsState::LinearModelSimulate()
{
}

void clsState::ModelSimulate()
{
//	BOOL bNonlinearModel = TRUE;
	BOOL bNonlinearModel = FALSE;
	if (bNonlinearModel) {
	}
	else {
		LinearModelSimulate();
	}
}

void clsState::SetRPTState0()
{
	double abc0[3] = {0, 0, m_c0};
	double posn[3] = {m_state0.x, m_state0.y, m_state0.z};
	N2G(abc0, posn, &m_RPTState0.xg);

	double veln[3] = {m_state0.ug, m_state0.vg, m_state0.wg};
	N2G(abc0, veln, &m_RPTState0.ug);

	m_RPTState0.xn = posn[0]; m_RPTState0.yn = posn[1]; m_RPTState0.zn = posn[2];
}

void clsState::SetRPTState()
{
	double abc0[3] = {0, 0, m_c0};
	double posn[3] = {m_state0.x, m_state0.y, m_state0.z};
	N2G(abc0, posn, &m_RPTState.xg);

	double veln[3] = {m_state0.ug, m_state0.vg, m_state0.wg};
	N2G(abc0, veln, &m_RPTState.ug);
}

double clsState::Rand()
{
	double dRand = (double)rand()/RAND_MAX*2-1;
	double dRand2 = dRand*dRand;
	if (dRand < 0) dRand2 = -dRand2;

	return dRand2;
}

static double _xc[3] = { 0, 0, 0 };
static clsVector xc(3, _xc, TRUE);

clsState::clsState()
{
	m_nCount = 0;

	m_tState0 = -1;
	m_tState1 = -1;

	m_c0 = 0;
	::memset(&m_RPTState, 0, sizeof(m_RPTState));
	::memset(&m_state1, 0, sizeof(m_state1));
	::memset(&m_state0, 0, sizeof(m_state0));				//all set to zero
	m_bFilter = FALSE; //TRUE;
	m_bVision = FALSE;
	m_bStateFromVision = FALSE;
	m_nState = 0;

	m_bCoordinated = FALSE;
	m_bEulerInited = FALSE;
	m_bGPSReset = FALSE;
	m_bIMUStuck = FALSE;

	m_tSIG0 = -1;
	m_nSIG = 0;

	::memset(&m_event, 0, sizeof(m_event));

	m_camera = 0;

	m_nSimulationType = 0;				//no simulation

	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);

	pthread_mutex_init(&m_mtxState, &attr);
	pthread_mutex_init(&m_mtxSIG, &attr);
}

clsState::~clsState()
{
}

BOOL clsState::LoadData(const char *pszFile)
{
	FILE *pfData = ::fopen(pszFile, "rb");
	if (pfData == NULL) return FALSE;

	::fseek(pfData, 0, SEEK_END);
	long length = ::ftell(pfData);

	int nSize = sizeof(double) + sizeof(UAVSTATE);
	int nData = length/(nSize-3*sizeof(double));
	//in "two.dat", only 25 variables are recorded, no as, bs and rfb in the end

	::rewind(pfData);
	m_pData = (char *)::malloc(nSize*nData);
	int iData = 0;

	double t;
	UAVSTATE state = {0};
	while (1) {
		if (::fread(&t, sizeof(double), 1, pfData) != 1) break;
		if (::fread(&state, sizeof(UAVSTATE)-3*sizeof(double), 1, pfData) != 1) break;				//as, bs and rfb is missed in two.dat
		(double &)m_pData[iData*nSize] = t;
		(UAVSTATE &)m_pData[iData*nSize+8] = state;
		iData ++;
	}
	m_nData = iData;

	::fclose(pfData);
	return TRUE;
}

void clsState::Update(double tPack, IM8PACK *pPack)		/* IG500N */
{
	Update1(tPack, pPack);				//update m_state1

	Filter();				//enter filter and put filtered result in m_state0

	//push UAV state to record
	pthread_mutex_lock(&m_mtxState);

	if ( (m_nState >= 0) && (m_nState < MAX_STATE) ) {
		m_tState[m_nState] = m_tState0;				//record
		m_state[m_nState++] = m_state0;
	}

	pthread_mutex_unlock(&m_mtxState);

	m_nCount ++;
};

void clsState::UpdateFromPX4(double tPack)
{
	Update1FromPX4(tPack);				//update m_state1

	Filter();				//enter filter and put filtered result in m_state0

	//push UAV state to record
	pthread_mutex_lock(&m_mtxState);
	if (m_nState != MAX_STATE) {
		m_tState[m_nState] = m_tState0;				//record
		m_state[m_nState++] = m_state0;
	}
	pthread_mutex_unlock(&m_mtxState);

	m_nCount ++;
};

void clsState::Update1(double tPack, IM8PACK *pPack)		/* IG500N */
{
	double posIM8[3], posAntenna[3];
	_im8.GetIM8Position(posIM8);
	_im8.GetAntennaPosition(posAntenna);

	double xa = posAntenna[0]; double ya = posAntenna[1]; double za = posAntenna[2];

	switch (pPack->gp8.type) {
	case DATA_GP8: {
		GP8PACK &gp8 = pPack->gp8;
	#ifdef _RTK
		m_state1.p = m_statePX4.p; m_state1.q = m_statePX4.q; m_state1.r = m_statePX4.r;
		m_state1.a = m_statePX4.a; m_state1.b = m_statePX4.b;
		m_state1.c = m_statePX4.c;
	#else
		m_state1.p = gp8.p; m_state1.q = gp8.q; m_state1.r = gp8.r;

		m_state1.a = gp8.a; m_state1.b = gp8.b;
		m_state1.c = gp8.c;
	#endif
		if (_cmm.GetViconFlag()) {
			VICON_DATA viconData = _cmm.GetViconData();
			m_state1.c = viconData.c;
		}
		//verify
		//relationship between signal and actual value -
		//signal = real + complementation, i.e., real = signal - complementation(error)
		//calculate errors in position and velocity caused by antenna position
		double duvw[3] = {
			gp8.q*za-gp8.r*ya,
			gp8.r*xa-gp8.p*za,
			gp8.p*ya-gp8.q*xa
		};
		double nduvw[3];
		double abc[3] = {m_state1.a, m_state1.b, m_state1.c};
		B2G(abc, duvw, nduvw);	// to NED

		double Mgf[3][3];
		clsMetric::AttitudeToTransformMatrix(&m_state1.a, NULL, Mgf);

		double acxa = gp8.acx + Mgf[2][0]*_gravity;
		double acya = gp8.acy + Mgf[2][1]*_gravity;
		double acza = gp8.acz + Mgf[2][2]*_gravity;
		m_state1.acx = acxa; m_state1.acy = acya; m_state1.acz = acza;
		m_state1.as = gp8.temp1; m_state1.bs = gp8.temp2;
		m_state1.rfb = (double)gp8.deviceStatus;

/*		uint32_t deviceStatus = gp8.deviceStatus;
		printf("device status %d\n", deviceStatus);
		if ((deviceStatus & 0x00000001))
			printf("calibration structure is well initialized\n");
		if ((deviceStatus & 0x00000002))
			printf("settings structure is well initialized\n");
		if ((deviceStatus & 0x00000004))
			printf("X accelerometer has passed self test\n");
		if ((deviceStatus & 0x00000008))
			printf("Y accelerometer has passed self test\n");
		if ((deviceStatus & 0x00000010))
			printf("Z accelerometer has passed self test\n");
		if ((deviceStatus & 0x00000020))
			printf("readings of accelerometers do not exceed operating range\n");
		if ((deviceStatus & 0x00000040))
			printf("X gyroscope has passed self test\n");
		if ((deviceStatus & 0x00000080))
			printf("Y gyroscope has passed self test\n");
		if ((deviceStatus & 0x00000100))
			printf("Z gyroscope has passed self test\n");
		if ((deviceStatus & 0x00000200))
			printf("readings of gyroscope do not exceed operating range\n");
		if ((deviceStatus & 0x00000400))
			printf("magnetic field calibration looks OK\n");
		if ((deviceStatus & 0x00000800))
			printf("altimeter could initialize\n");
		if ((deviceStatus & 0x00001000))
			printf("GPS receive could initialize properly\n");
		if ((deviceStatus & 0x00002000))
			printf("gravity is observable sufficiently for proper Kalman filter operation\n");
		if ((deviceStatus & 0x00004000))
			printf("heading is observable sufficiently for proper Kalman filter operation\n");
		if ((deviceStatus & 0x00008000))
			printf("velocity is observable sufficiently for proper Kalman filter operation\n");
		if ((deviceStatus & 0x00010000))
			printf("position is observable sufficiently for proper Kalman filter operation\n");*/


		/*
		 * No GPS signal, the vision data such as body velocity is used,
		 * Ground velocity and position are also derived based on the vision information
		 */
		if ( gp8.nGPS < 4 || ((gp8.gpsinfo & 0x03) != 3) ) {
			// Without GPS environment
			m_utcTime.year = gp8.year; m_utcTime.month = gp8.month; m_utcTime.day = gp8.day;
			m_utcTime.hour = gp8.hour; m_utcTime.minutes = gp8.minutes; m_utcTime.seconds = gp8.seconds;
			m_utcTime.nanoseconds = gp8.nanoseconds;
			_state.m_safetyStatus.bGPS = TRUE;
		}
		else if (_cmm.GetViconFlag()) {
			VICON_DATA viconData = _cmm.GetViconData();
			m_KalmanState.a = m_state1.a; m_KalmanState.b = m_state1.b; m_KalmanState.c = m_state1.c;
			m_KalmanState.p = m_state1.p; m_KalmanState.q = m_state1.q; m_KalmanState.r = m_state1.r;
			m_KalmanState.acx = m_state1.acx; m_KalmanState.acy = m_state1.acy; m_KalmanState.acz = m_state1.acz;
	//		m_KalmanState.acx = 0; m_KalmanState.acy = 0; m_KalmanState.acz = 0;
			m_KalmanState.u = m_state1.u; m_KalmanState.v = m_state1.v; m_KalmanState.w = 0;
			m_KalmanState.x = viconData.x; m_KalmanState.y = viconData.y; m_KalmanState.z = viconData.z;
//			printf("[state] vicon x %.3f, y %.3f, z %.3f, c %.3f\n", viconData.x, viconData.y, viconData.z, viconData.c);

			KalmanTimeUpdate(m_state1, m_KalmanState);
			KalmanMeasurementUpdate(m_state1, m_KalmanState);
		}
		else if ( gp8.nGPS > 4 || ((gp8.gpsinfo & 0x03) == 3) ) {
			if (!m_bCoordinated || GetGPSResetFlag()) {
					Coordinate(gp8.longitude, gp8.latitude, gp8.altitude);
					SetInitPressure(gp8.pressure);
					m_bCoordinated = TRUE;
					ResetGPSResetFlag();
					m_bLaserFilterInitialization = FALSE;

					#ifdef _RTK
					Mat_<double> state(9,1);
					state.setTo(Scalar(0));
					Mat_<double> measurement(6,1);
					measurement.setTo(Scalar(0));
					m_clsNavKF.InitKF(state, measurement);
	//					printf("[state] coordinated\n\n");
					#endif
			}		//record the initial orientation

			// updated GPS position if valid
			//update GPS related data
			//if original long, lat and alt not set, set first
			m_state1.longitude = gp8.longitude;
			m_state1.latitude = gp8.latitude;
			m_state1.altitude = gp8.altitude;

			//calculate position by longitude, latitude and altitude
			m_state1.x = (m_state1.latitude-m_latitude0)*_radius;
			m_state1.y = (m_state1.longitude-m_longitude0)*_radius * cos(m_state1.latitude);
			m_state1.z = /*CalculateHeightViaBaro(gp8.pressure); */ m_altitude0-m_state1.altitude;
			m_state1.as = CalculateHeightViaBaro(gp8.pressure);

			double npos[3] = {m_state1.x, m_state1.y, m_state1.z};
			m_state1.x = npos[0];
			m_state1.y = npos[1];
			m_state1.z = npos[2];
			m_state1.u = gp8.u; m_state1.v = gp8.v; m_state1.w = gp8.w;


			#ifdef _RTK
				//for RTK, the velocity u v w is already in NED frame...
				m_state1.ug = gp8.u; m_state1.vg = gp8.v; m_state1.wg = gp8.w;
				/* Kalman filter for novatel RTK + accelerations... */
				//-- input
				double xacc, yacc, zacc, a, b, c;
				xacc = yacc = zacc = a = b = c = 0;
				GetNEDAcceleration(xacc, yacc, zacc, a, b, c);
				Mat_<double> control(3,1);
				control.setTo(Scalar(0));
				control.at<double>(0) = xacc;
				control.at<double>(1) = yacc;
				control.at<double>(2) = zacc;

				m_state1.acx = xacc; m_state1.acy = yacc; m_state1.acz = zacc;
				m_state1.a = a; m_state1.b = b; m_state1.c = c;

				//-- measurements
				Mat_<double> measurement(6,1);
				measurement.setTo(Scalar(0));
				measurement.at<double>(0) = (double) m_state1.x;
				measurement.at<double>(1) = (double) m_state1.y;
				measurement.at<double>(2) = (double) m_state1.z;
				measurement.at<double>(3) = (double) m_state1.ug;
				measurement.at<double>(4) = (double) m_state1.vg;
				measurement.at<double>(5) = (double) m_state1.wg;

				Mat_<double> prediction(9,1);
				prediction.setTo(Scalar(0));
				Mat_<double> estimated(9,1);
				estimated.setTo(Scalar(0));
				m_clsNavKF.RunKF(control, measurement, prediction, estimated);
				m_state1.x   = estimated.at<double>(0);
				m_state1.y   = estimated.at<double>(1);
				m_state1.z 	 = estimated.at<double>(2);
				m_state1.ug  = estimated.at<double>(3);
				m_state1.vg  = estimated.at<double>(4);
				m_state1.wg  = estimated.at<double>(5);
				if (m_nCount%50 == 0) {
						printf("[rtk lock] pos: %.2f %.2f %.2f vel: %.2f %.2f %.2f\n\n", m_state1.x, m_state1.y, m_state1.z, m_state1.ug, m_state1.vg, m_state1.wg);
				}
			#else
				B2G(abc, &m_state1.u, &m_state1.ug);

			// --------- Kalman filter for the z-axix, provide height and wg -----------
/*			memcpy(&m_KalmanState, &m_state1, sizeof(UAVSTATE));
			KalmanTimeUpdateZAxis(m_state1, m_KalmanState);
			KalmanMeasurementUpdateZAxis(m_state1, m_KalmanState);*/

			// --------- filter used from Peidong -----------
			LaserHeightFusion();
			m_state1.z = laserZaxisFilter.GetEstiState().estx;
//			m_state1.wg = laserZaxisFilter.GetEstiState().estv;

			//printf("[state] m_state1: %.2f %.2f\n", laserZaxisFilter.GetEstiState().estx, laserZaxisFilter.GetEstiState().estv);
			#endif

			m_utcTime.year = gp8.year; m_utcTime.month = gp8.month; m_utcTime.day = gp8.day;
			m_utcTime.hour = gp8.hour; m_utcTime.minutes = gp8.minutes; m_utcTime.seconds = gp8.seconds;
			m_utcTime.nanoseconds = gp8.nanoseconds;

			m_chkState.b3DLock = (int)(gp8.gpsinfo & 0x03);
			m_chkState.nSatellites = gp8.nGPS;

			if (m_chkState.b3DLock == 3)
				_state.m_safetyStatus.bGPS = true;
			else
				_state.m_safetyStatus.bGPS = false;
/*			if (m_nCount % 50 == 0) {
				printf("[IM8 baro filter]: z %.3f, wg %.3f\n", m_state1.z, m_state1.wg);
			}*/
		}
		m_tState1 = tPack;
		break;
	}

	}
}

void clsState::Update1FromPX4(double tPack)
{
	::memcpy(&m_state1, &m_statePX4, sizeof(UAVSTATE));
	m_tState1 = tPack;

	G2B(&m_state1.a, &m_state1.ug, &m_state1.u);

//	printf("NED velocity: %.3f, %.3f, %.3f\n", m_state1.ug, m_state1.vg, m_state1.wg);
	//calculate position by longitude, latitude and altitude
	m_state1.x = (m_state1.latitude-m_latitude0)*_radius;
	m_state1.y = (m_state1.longitude-m_longitude0)*_radius * cos(m_state1.latitude);
	m_state1.z = m_altitude0-m_state1.altitude;
}

double clsState::CalculateHeightViaBaro(double pressure)
{
	double frac = pressure/m_pressure0;

	return -44307 * (1 - pow(frac, 0.1902));
}

BOOL clsState::KalmanTimeUpdateZAxis(UAVSTATE &uavstate, const UAVSTATE KalmanState)
{
	clsVector m_x_ZAxis(2, _m_x_ZAxis, TRUE);
    double _u = uavstate.acz; clsVector u(1, &_u, TRUE);

    double _Ax[2]; clsVector Ax(2, _Ax, TRUE);
    clsMatrix::X(m_Kalman_A_ZAxis, m_x_ZAxis, Ax);

    double _Bu[2]; clsVector Bu(2, _Bu, TRUE);
    clsMatrix::X(m_Kalman_B_ZAxis, u, Bu);

    m_x_ZAxis = Ax;
    m_x_ZAxis += Bu;

    double _AP[2][2]; clsMatrix AP;
    AP.Reset(2, 2, (double *)_AP, TRUE);
    clsMatrix::X(m_Kalman_A_ZAxis, m_Kalman_P_ZAxis, AP);

    double _At[2][2]; clsMatrix At;
    At.Reset(2, 2, (double *)_At, TRUE);
    clsMatrix::T(m_Kalman_A_ZAxis, At);

    double _APAt[2][2]; clsMatrix APAt;
    APAt.Reset(2, 2, (double *)_APAt, TRUE);
    clsMatrix::X(AP, At, APAt);

    APAt += m_BQBt_ZAxis;
    m_Kalman_P_ZAxis = APAt;

    return TRUE;
}


BOOL clsState::KalmanTimeUpdate(UAVSTATE &uavstate, const UAVSTATE KalmanState)
{
	clsVector m_xVision_WF(6, _m_xVision_WF, TRUE);

    double ab[3] = {KalmanState.acx, KalmanState.acy, KalmanState.acz};
    double an[3] = {0};
    double angle[3] = {uavstate.a, uavstate.b, uavstate.c};
    B2G(angle, ab, an);

    double _u[3] = {an[0], an[1], an[2]};
    clsVector u(3, _u, TRUE);

    double _Ax[6]; clsVector Ax(6, _Ax, TRUE);
    clsMatrix::X(m_Kalman_A_WF, m_xVision_WF, Ax);
//    printf("Ax: %f, %f, %f\n", Ax[0], Ax[1], Ax[2]);

    double _Bu[6]; clsVector Bu(6, _Bu, TRUE);
    clsMatrix::X(m_Kalman_B_WF, u, Bu);
//    printf("Bu: %f, %f, %f\n", Bu[0], Bu[1], Bu[2]);
    m_xVision_WF = Ax;
    m_xVision_WF += Bu;
//    printf("xVision: %f, %f, %f, %f, %f, %f\n",
//                xVision[0], xVision[1], xVision[2], xVision[3], xVision[4], xVision[5]);

    double _AP[6][6]; clsMatrix AP;
    AP.Reset(6, 6, (double *)_AP, TRUE);
    clsMatrix::X(m_Kalman_A_WF, m_Kalman_P_WF, AP);

    double _At[6][6]; clsMatrix At;
    At.Reset(6, 6, (double *)_At, TRUE);
    clsMatrix::T(m_Kalman_A_WF, At);

    double _APAt[6][6]; clsMatrix APAt;
    APAt.Reset(6, 6, (double *)_APAt, TRUE);
    clsMatrix::X(AP, At, APAt);

    APAt += m_BQBt_WF;
    m_Kalman_P_WF = APAt;

    // state update after fusion
    uavstate.x = m_xVision_WF[0]; uavstate.y = m_xVision_WF[1]; uavstate.z = m_xVision_WF[2];
    uavstate.ug = m_xVision_WF[3]; uavstate.vg = m_xVision_WF[4]; uavstate.wg = m_xVision_WF[5];
    double ug[3] = {m_xVision_WF[3], m_xVision_WF[4], m_xVision_WF[5]};
    G2B(angle, ug, &uavstate.u);

    return TRUE;
}

BOOL clsState::KalmanMeasurementUpdateZAxis(UAVSTATE &uavstate, const UAVSTATE KalmanState)
{
	clsVector m_x_ZAxis(2, _m_x_ZAxis, TRUE);

    double _yv = KalmanState.as;
//    double angle[3] = {uavstate.a, uavstate.b, uavstate.c};

//    _yv[0] = KalmanState.as;
//    _yv[1] = KalmanState.wg;

    clsVector yv(1, &_yv, TRUE);

    double _H[2][1]; clsMatrix H;
    H.Reset(2, 1, (double *)_H, TRUE);

    double _PCt[2][1]; clsMatrix PCt;
    PCt.Reset(2, 1, (double *)_PCt, TRUE);

    double _Ct[2][1]; clsMatrix Ct;
    Ct.Reset(2, 1, (double *)_Ct, TRUE);
    clsMatrix::T(m_Kalman_C_ZAxis, Ct);
    clsMatrix::X(m_Kalman_P_ZAxis, Ct, PCt);

    double _CP[1][2]; clsMatrix CP;
    CP.Reset(1, 2, (double *)_CP, TRUE);
    clsMatrix::X(m_Kalman_C_ZAxis, m_Kalman_P_ZAxis, CP);

    double _CPCt[1][1]; clsMatrix CPCt;
    CPCt.Reset(1, 1, (double *)_CPCt, TRUE);
    clsMatrix::X(CP, Ct, CPCt);
    CPCt += m_Kalman_R_ZAxis;

    double _CPCtInverse[1][1]; clsMatrix CPCtInverse;
    CPCtInverse.Reset(1, 1, (double *)_CPCtInverse, TRUE);
    clsMatrix::R(CPCt, CPCtInverse);
    clsMatrix::X(PCt, CPCtInverse, H);

    double _Cx; clsVector Cx(1, &_Cx, TRUE);
    clsMatrix::X(m_Kalman_C_ZAxis, m_x_ZAxis, Cx);
    yv -= Cx;

    double _Hyv[2]; clsVector Hyv(2, _Hyv, TRUE);
    clsMatrix::X(H, yv, Hyv);

    m_x_ZAxis += Hyv;

    double _EYE[2][2] = {
                { 1,0 },
                { 0,1}
    };
    clsMatrix EYE;
    EYE.Reset(2, 2, (double *)_EYE, TRUE);

    double _HC[2][2]; clsMatrix HC;
    HC.Reset(2, 2, (double *)_HC, TRUE);
    clsMatrix::X(H, m_Kalman_C_ZAxis, HC);
    EYE -= HC;

    clsMatrix::X(EYE, m_Kalman_P_ZAxis, m_Kalman_P_ZAxis);          // P update

    // state update after fusion
    uavstate.z = m_x_ZAxis[0];
//    uavstate.wg = m_x_ZAxis[1];

	return TRUE;
}

BOOL clsState::KalmanMeasurementUpdate(UAVSTATE &uavstate, const UAVSTATE KalmanState)
{
	clsVector m_xVision_WF(6, _m_xVision_WF, TRUE);

//    double tpos[3] = {KalmanState.u, KalmanState.v, 0};

    double _yv[3] = {0};
    double angle[3] = {uavstate.a, uavstate.b, uavstate.c};

//    B2G(angle, tpos, _yv);
//    _yv[2] = _yv[1]; _yv[1] = _yv[0]; _yv[0] = KalmanState.z;

    _yv[0] = KalmanState.x;
    _yv[1] = KalmanState.y;
    _yv[2] = KalmanState.z;

   // printf("_yv: %f, %f, %f\n", _yv[0], _yv[1], _yv[2]);
    clsVector yv(3, _yv, TRUE);

    double _H[6][3]; clsMatrix H;
    H.Reset(6, 3, (double *)_H, TRUE);

    double _PCt[6][3]; clsMatrix PCt;
    PCt.Reset(6, 3, (double *)_PCt, TRUE);

    double _Ct[6][3]; clsMatrix Ct;
    Ct.Reset(6, 3, (double *)_Ct, TRUE);
    clsMatrix::T(m_Kalman_C_WF, Ct);
    clsMatrix::X(m_Kalman_P_WF, Ct, PCt);

    double _CP[3][6]; clsMatrix CP;
    CP.Reset(3, 6, (double *)_CP, TRUE);
    clsMatrix::X(m_Kalman_C_WF, m_Kalman_P_WF, CP);

    double _CPCt[3][3]; clsMatrix CPCt;
    CPCt.Reset(3, 3, (double *)_CPCt, TRUE);
    clsMatrix::X(CP, Ct, CPCt);
    CPCt += m_Kalman_R_WF;

    double _CPCtInverse[3][3]; clsMatrix CPCtInverse;
    CPCtInverse.Reset(3, 3, (double *)_CPCtInverse, TRUE);
    clsMatrix::R(CPCt, CPCtInverse);
    clsMatrix::X(PCt, CPCtInverse, H);

    double _Cx[3]; clsVector Cx(3, _Cx, TRUE);
    clsMatrix::X(m_Kalman_C_WF, m_xVision_WF, Cx);
    yv -= Cx;

    double _Hyv[6]; clsVector Hyv(6, _Hyv, TRUE);
    clsMatrix::X(H, yv, Hyv);

    m_xVision_WF += Hyv;

    double _EYE[6][6] = {
                { 1,0,0,0,0,0 },
                { 0,1,0,0,0,0 },
                { 0,0,1,0,0,0 },
                { 0,0,0,1,0,0 },
                { 0,0,0,0,1,0 },
                { 0,0,0,0,0,1 }
    };
    clsMatrix EYE;
    EYE.Reset(6, 6, (double *)_EYE, TRUE);

    double _HC[6][6]; clsMatrix HC;
    HC.Reset(6, 6, (double *)_HC, TRUE);
    clsMatrix::X(H, m_Kalman_C_WF, HC);
    EYE -= HC;

    clsMatrix::X(EYE, m_Kalman_P_WF, m_Kalman_P_WF);          // P update

    // state update after fusion
    uavstate.x = m_xVision_WF[0]; uavstate.y = m_xVision_WF[1]; uavstate.z = m_xVision_WF[2];
    uavstate.ug = m_xVision_WF[3]; uavstate.vg = m_xVision_WF[4]; uavstate.wg = m_xVision_WF[5];
    double ug[3] = {m_xVision_WF[3], m_xVision_WF[4], m_xVision_WF[5]};
    G2B(angle, ug, &uavstate.u);

	return TRUE;
}

clsFilter::clsFilter()
{
	Reta = 10;
	Reps[0][0] = 0.1; Reps[0][1] = 0;
	Reps[1][0] = 0; Reps[1][1] = 0.1;

	//initialize
	t = -1;
}

void clsFilter::Update(double tx, double xm, double um)
{
	if (t <= 0) {
		t = tx;
		xi = xm;

		ee = 0;
		bee = 0;

		ue = um;

		xei = xm;
		xeif = xei;

		xif = xi;

		xmf = xm;
		xmfv = xmf;

		P[0][0] = P[0][1] = P[1][0] = P[1][1] = 0;
		Q[0][0] = Q[0][1] = Q[1][0] = Q[1][1] = 0;

		return;
	}

	double dt = tx -t;
	t = tx;

	//integration of um
	xi += um*dt;
	double em = xi - xm;

	//P = A*Q*A'+Reps
	P[0][0] = Q[0][0]+dt*(Q[0][1]+Q[1][0])+dt*dt*Q[1][1]+Reps[0][0];
	P[0][1] = Q[0][1]+dt*Q[1][1]+Reps[0][1];
	P[1][0] = Q[1][0]+dt*Q[1][1]+Reps[1][0];
	P[1][1] = Q[1][1]+Reps[1][1];

	//B = P*C'/(C*P*C'+Reta);
	double B[2] = { P[0][0]/(P[0][0]+Reta), P[1][0]/(P[0][0]+Reta) };

	//e = A*e+B*(em-C*A*e)
	double error = em-ee-dt*bee;
	double ee2 = ee+dt*bee+B[0]*error;
	double bee2 = bee+B[1]*error;

	ee = ee2; bee = bee2;

	//Q = P*(I-B*C);
	if (Q[0][0] < 1e6 && Q[0][1] < 1e6 && Q[1][0] < 1e6 && Q[1][1] < 1e6) {
	//in this work the Q will increase infinitely, and B will converge to a
	//value, this is to prevent Q increase inifinitely and B is kept at a
	//convergent value
		Q[0][0] = (1-B[0])*P[0][0]-B[1]*P[0][1];
		Q[0][1] = P[0][1];
		Q[1][0] = (1-B[0])*P[1][0]-B[1]*P[1][1];
		Q[1][1] = P[1][1];
	}

	ue = um - bee;
	xei += ue*dt;

	//low-pass filter
	double r = 0.01;

	xeif = (1-r)*xeif + r*xei;
	xmf = (1-r)*xmf + r*xm;
	xmfv = xmf - (xeif - xei);

//	xif = (1-r)*xif+r*xi;
//	xmfv = xmf - (yif-yi);
}

void clsState::Filter()
{
	m_tState0 = m_tState1;
	m_state0 = m_state1;

	return;
	if (!m_bFilter) return;				//skip filter

	//filter x, y, z and made verifications
	m_fx.Update(m_tState1, m_state1.x, m_state1.ug);
	m_fy.Update(m_tState1, m_state1.y, m_state1.vg);
	m_fz.Update(m_tState1, m_state1.z, m_state1.wg);

	m_state0.x = m_fx.GetPositionEstimation();
	m_state0.ug = m_fx.GetVelocityEstimation();

	m_state0.y = m_fy.GetPositionEstimation();
	m_state0.vg = m_fy.GetVelocityEstimation();

	m_state0.z = m_fz.GetPositionEstimation();
	m_state0.wg = m_fz.GetVelocityEstimation();

	G2B(&m_state0.a, &m_state0.ug, &m_state0.u);
}

BOOL clsState::Emergency()
{
	//judge based on state1 (not state0, state0 is filtered result)
//	double t = ::GetTime();
//	BOOL bValid0 = /*m_tState0 < 0 ||*/ t - m_tState0 < 0.1;
	//regular each period 0.02 second, if larger than 0.1, that means 5 periods blocked

//	BOOL bValid1 = Valid(&m_state1);
	BOOL bValid2 = Valid(&m_state0);				//use for model simulation

	return /*!bValid0 || !bValid1 || */ !bValid2;
}

BOOL clsState::Valid(UAVSTATE *pState)
{
	BOOL bValid =
		::fabs(pState->x) <= 5000 &&
		::fabs(pState->y) <= 5000 &&
		pState->z <= 400 && pState->z >= -500 &&
		::fabs(pState->u) <= 20 &&
		::fabs(pState->v) <= 10 &&
		::fabs(pState->w) <= 20 &&
		::fabs(pState->a) <= /*PI/4*/ PI/3 &&
		::fabs(pState->b) <= /*PI/4*/ PI/3 &&
		::fabs(pState->p) <= 2 &&
		::fabs(pState->q) <= 2 &&
		::fabs(pState->r) <= 2 &&
		::fabs(pState->as) <= 0.5 &&
		::fabs(pState->bs) <= 0.5 &&
		::fabs(pState->rfb) <= 0.5;

	return bValid;
}

void clsState::Init()
{
	m_Ad.Reset(3, 3, (double *)_m_Ad, TRUE);
	m_Bd.Reset(3, 12, (double *)_m_Bd, TRUE);
	m_Cd.Reset(3, 3, (double *)_m_Cd, TRUE);
	m_Dd.Reset(3, 12, (double *)_m_Dd, TRUE);

	_parser.GetVariable("_Ad", m_Ad);
	_parser.GetVariable("_Bd", m_Bd);
	_parser.GetVariable("_Cd", m_Cd);
	_parser.GetVariable("_Dd", m_Dd);

	m_tObserve = -1;

	if (_HELICOPTER == ID_QUADLION) {
		m_Kalman_A_WF.Reset(6, 6, (double *)_m_Kalman_A_WF, TRUE);
		m_Kalman_B_WF.Reset(6, 3, (double *)_m_Kalman_B_WF, TRUE);
		m_Kalman_C_WF.Reset(3, 6, (double *)_m_Kalman_C_WF, TRUE);
		m_Kalman_Q_WF.Reset(3, 3, (double *)_m_Kalman_Q_WF, TRUE);
		m_Kalman_R_WF.Reset(3, 3, (double *)_m_Kalman_R_WF, TRUE);
		m_Kalman_P_WF.Reset(6, 6, (double *)_m_Kalman_P_WF, TRUE);
		m_BQBt_WF.Reset(6, 6, (double *)_m_BQBt_WF, TRUE);

		_parser.GetVariable("_Kalman_A_WF", m_Kalman_A_WF);
		_parser.GetVariable("_Kalman_B_WF", m_Kalman_B_WF);
		_parser.GetVariable("_Kalman_C_WF", m_Kalman_C_WF);
		_parser.GetVariable("_Kalman_Q_WF", m_Kalman_Q_WF);
		_parser.GetVariable("_Kalman_R_WF", m_Kalman_R_WF);
		_parser.GetVariable("_Kalman_P_WF", m_Kalman_P_WF);
		_parser.GetVariable("_BQBt_WF", m_BQBt_WF);

		::memset(_m_xVision_WF, 0, sizeof(double)*6);
		m_bMeasureUpdate = FALSE; 	// initial no vision fusion, start when vision data come

		// load Z axis needed Kalman matrices
		m_Kalman_A_ZAxis.Reset(2, 2, (double *)_m_Kalman_A_ZAxis, TRUE);
		m_Kalman_B_ZAxis.Reset(2, 1, (double *)_m_Kalman_B_ZAxis, TRUE);
		m_Kalman_C_ZAxis.Reset(1, 2, (double *)_m_Kalman_C_ZAxis, TRUE);
		m_Kalman_Q_ZAxis.Reset(1, 1, (double *)_m_Kalman_Q_ZAxis, TRUE);
		m_Kalman_R_ZAxis.Reset(1, 1, (double *)_m_Kalman_R_ZAxis, TRUE);
		m_Kalman_P_ZAxis.Reset(2, 2, (double *)_m_Kalman_P_ZAxis, TRUE);
		m_BQBt_ZAxis.Reset(2, 2, (double *)_m_BQBt_ZAxis, TRUE);

		_parser.GetVariable("_Kalman_A_ZAxis", m_Kalman_A_ZAxis);
		_parser.GetVariable("_Kalman_B_ZAxis", m_Kalman_B_ZAxis);
		_parser.GetVariable("_Kalman_C_ZAxis", m_Kalman_C_ZAxis);
		_parser.GetVariable("_Kalman_Q_ZAxis", m_Kalman_Q_ZAxis);
		_parser.GetVariable("_Kalman_R_ZAxis", m_Kalman_R_ZAxis);
		_parser.GetVariable("_Kalman_P_ZAxis", m_Kalman_P_ZAxis);
		_parser.GetVariable("_BQBt_ZAxis", m_BQBt_ZAxis);

		::memset(_m_x_ZAxis, 0, sizeof(double)*2);
	}

	m_equ = _equ_Hover;				//equ for model simulation

	m_pData = NULL;
	m_nData = 0;
	m_iData = 0;

	m_safetyStatus.bGPS = m_safetyStatus.bIMUData = m_safetyStatus.bIMURcv = false;
	m_safetyStatus.bPixhawkData = m_safetyStatus.bPixhawkRcv = false;

	m_bLaserFilterInitialization = FALSE;

	::memset(&m_navState, 0, sizeof(m_navState));
}

void clsState::LaserHeightFusion() {
	if ((!m_bLaserFilterInitialization)) {
			SINGLEAXISSTATE initialState;
			initialState.estx = m_state0.as;
			initialState.estv = m_state0.wg;
			initialState.esta = m_state0.acz;
			initialState.estBias = m_state0.z - m_state0.as;

			laserZaxisFilter.Init(initialState);

			laserZaxisFilter.SetKalmanA("_Kalman_A_Baro");
			laserZaxisFilter.SetKalmanC("_Kalman_C_Baro");
			laserZaxisFilter.SetKalmanQ("_Kalman_Q_Baro");
			laserZaxisFilter.SetKalmanRwLaser("_Kalman_RwLaser");
			laserZaxisFilter.SetKalmanRwoLaser("_Kalman_RwoLaser");

			laserZaxisFilter.SetKalmanStartT(::GetTime());
			laserZaxisFilter.SetKalmanInitializationPeriod(5); //5s
			laserZaxisFilter.SetLaserLostTH(1); // 1meter

			laserZaxisFilter.SetInnovationFilterCovariance(0.1*0.1);
			m_bLaserFilterInitialization = true;
		}
	else if (m_bLaserFilterInitialization) {
			SINGLEAXISMEASUREMENT measurement;

			measurement.bCorrectMeasurement = TRUE; //_urg.GetLaserMeasurementFlag();
			measurement.ig500nZ = m_state0.z;
			measurement.ig500nWg = m_state0.wg;

			double accb[3] = {m_state0.acx, m_state0.acy, m_state0.acz};
			double abc[3] = {m_state0.a, m_state0.b, m_state0.c};
			double accg[3] = {0};
			B2G(abc, accb, accg);
			measurement.ig500nAg = accg[2]; //m_state0.acz;
			measurement.laserZ = m_state0.as; //_urg.GetLaserHeight();
			laserZaxisFilter.UpdateFilter(measurement);
		}
}

void clsState::GetNEDAcceleration(double &acx, double &acy, double &acz,  double &a, double &b, double &c){
	double acxyzB[3] = {m_statePX4.acx, m_statePX4.acy, m_statePX4.acz};
	double agxyzN[3];
	double abc[3] = {m_statePX4.a, m_statePX4.b, m_statePX4.c};
	B2G(abc, acxyzB, agxyzN);	// to NED

	acx = agxyzN[0];
	acy = agxyzN[1];
	acz = agxyzN[2] + _gravity;

	a = abc[0];
	b = abc[1];
	c = abc[2];

//	printf("[svo] abc: %.2f %.2f %.2f\n", abc[0]*180/PI, abc[1]*180/PI, abc[2]*180/PI);
//	printf("[svo] acc: %.2f %.2f %.2f\n", acxyzB[0], acxyzB[1], acxyzB[2]);
}
