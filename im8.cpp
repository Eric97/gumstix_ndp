//im8.cpp
//implementation file for reading data from IMU - IG500N

#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>

#include "uav.h"
#include "im8.h"
#include "state.h"

extern clsState _state;
/*
 * IG500N
 */
clsIM8::clsIM8()
{
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutex_init(&m_mtxIM8, &attr);

	m_xIM8 = 0;
	m_yIM8 = 0;
	m_zIM8 = 0;

	m_xAntenna = 0; //-1.2;
	m_yAntenna = 0;
	m_zAntenna = 0; //-0.13;

	m_enumDecodeState = BGN_SYNC1;
	m_ulRTKCRC = 0;
	m_nMsgId = 0;
	m_nMsgLgth = 0;
	m_nHderLgth = 0;
}

clsIM8::~clsIM8()
{
	pthread_mutex_destroy(&m_mtxIM8);
}

BOOL clsIM8::InitThread()
{
    //initial setting
	FD_ZERO(&rfd);
	m_timeOut.tv_sec = 0;
	m_timeOut.tv_usec = IMU_TIMEOUT_READ;

    m_nsIM8= ::open("/dev/ser3", O_RDWR | O_NONBLOCK );
	if (m_nsIM8 == -1) {
			printf("[IM8] Open IMU serial port (/dev/ser3) failed!\n");
			return FALSE;
	}

    // Setup the COM port
    termios term;
    tcgetattr(m_nsIM8, &term);

	#ifdef _RTK
	#undef IM8_BAUDRATE
	#define IM8_BAUDRATE 38400
	#endif

    cfsetispeed(&term, IM8_BAUDRATE);				//input and output baudrate
    cfsetospeed(&term, IM8_BAUDRATE);

    term.c_cflag = CS8 | CLOCAL | CREAD;				//communication flags
//    term.c_iflag &= ~IXOFF;
//    term.c_iflag = /*IGNBRK | IGNCR |*/ IGNPAR;

	tcsetattr(m_nsIM8, TCSANOW, &term);

	tcflush(m_nsIM8, TCIOFLUSH);
//	IM8RAWPACK packraw[MAX_IM8PACK];

	//initialize variables
	m_tRetrieve = m_tRequest = -1;

	m_nIM8 = 0;
	m_nLost = 0;

	m_tGP8 = ::GetTime();
	m_tAH8 = -1;
	m_tSC8 = -1;

	m_nBuffer = 0;

	m_bit = 0;

	m_pfStuckLog = fopen("imu_stuck_log.txt", "w");
	if (m_pfStuckLog == NULL) return FALSE;

    printf("[IM8] Start thread %d\n", pthread_self());		// after filtering the 1st packet, then start real imu data capture
	return TRUE;
}

void clsIM8::SetIMUConfig(short imuID, short flag, int baudrate)
{
	m_imuConfig.imuID = imuID;
	m_imuConfig.flag = flag;
	m_imuConfig.baudrate = baudrate;
}

int clsIM8::EveryRun()
{
    if (_cps != 13000000)
    	printf("[IMU1] _cps corrupted\n");

	int nMode = _state.GetSimulationType();
	if (nMode == SIMULATIONTYPE_MODEL || nMode == SIMULATIONTYPE_STATE)	return TRUE;	//return if in simulation mode

//	_state.UpdateFromPX4(::GetTime());
//	return TRUE;

#ifdef _RTK
	IM8PACK _RTKOutputs; // for consistent, store RTK pos and vel to IM8RAWPACK
	if (GetRTKOutput(_RTKOutputs)){
		// read in successfully...
		m_tGP8 = ::GetTime();
		memset(&m_gp8, 0, sizeof(IM8PACK));
		memcpy(&m_gp8, &_RTKOutputs, sizeof(IM8PACK));
		_state.Update(m_tGP8, &m_gp8);
	}
#else
	IM8RAWPACK packraw[MAX_IM8PACK];
	int nGet = GetPack(packraw, MAX_IM8PACK);	// # of imu packets get

	// check the time internal between each data reading, if bigger than a certain time, consider stuck
	double dt = ::GetTime() - m_tGP8;
	if (dt > 1.0) {
		_state.m_safetyStatus.bIMURcv = false;
		_state.SetIMUStuckFlag(); // set once and will go into SEMI forever in case IMU reading come back
		printf("[IM8] Reading stuck\n");
	}
	else
		_state.m_safetyStatus.bIMURcv = true;

	if (nGet == 0) {
		double tLost = ::GetTime();
		fprintf(m_pfStuckLog, "Get0 at %.4f %d\n", tLost, m_nCount);
//		return TRUE;
	}

	Translate();

	if ( m_nCount % 50 == 0 ) {
//		printf("# of satallites %d\n", (int)(m_gp8.gp8.nGPS));
//		printf("GPS fixed :%d\n", (m_gp8.gp8.gpsinfo & 0x03));
//		printf("temperature %.3f, %.3f\n", (m_gp8.gp8.temp1), (m_gp8.gp8.temp2));
//		printf("device status %d\n", (m_gp8.gp8.deviceStatus));
//		printf("UTC time Year %d, month %d, day %d, hour %d, minutes %d, seconds %d\n ",
//				m_gp8.gp8.year, m_gp8.gp8.month, m_gp8.gp8.day, m_gp8.gp8.hour, m_gp8.gp8.minutes, m_gp8.gp8.seconds);
	}

	m_tGP8 = ::GetTime();
	_state.Update(m_tGP8, /*&pack*/ &m_gp8);
#endif

	pthread_mutex_lock(&m_mtxIM8);
	if ( (m_nIM8 >= 0) && (m_nIM8 < MAX_IM8PACK) ) {
		m_tIM8[m_nIM8] = m_tGP8;
//		m_im8Raw[m_nIM8] = *pPackRaw;
		m_im8[m_nIM8++] = /*pack*/ m_gp8;
	}
	pthread_mutex_unlock(&m_mtxIM8);

    if (_cps != 13000000)
    	printf("[IMU2] _cps corrupted\n");

	return TRUE;
}

void clsIM8::ExitThread()
{
	::close(m_nsIM8);
	printf("[IM8] quit\n");
}

BOOL clsIM8::GetLastPack(int nType, double* pTime, IM8PACK* pPack)
{
	if (nType == DATA_GP8) {
		if (m_tGP8 < 0) return FALSE;
		else {
			*pTime = m_tGP8;
			*pPack = m_gp8;
			return TRUE;
		}
	}

/*	if (nType == DATA_AH8) {
		if (m_tAH8 < 0) return FALSE;
		else {
			*pTime = m_tAH8;
			*pPack = m_ah8;
			return TRUE;
		}
	}*/

	if (nType == DATA_SC8) {
		if (m_tSC8 < 0) return FALSE;
		else {
			*pTime = m_tSC8;
			*pPack = m_sc8;
			return TRUE;
		}
	}
	return FALSE;
}

BOOL clsIM8::GetLastPack(int nType, double *pTime, IM8RAWPACK *pRaw)
{
	if (nType == DATA_GP8RAW) {
		if (m_tGP8 < 0) return FALSE;
		else {
			*pTime = m_tGP8;
			*pRaw = m_gp8Raw;
			return TRUE;
		}
	}

/*	if (nType == DATA_AH8RAW) {
		if (m_tAH8 < 0) return FALSE;
		else {
			*pTime = m_tAH8;
			*pRaw = m_ah8Raw;
			return TRUE;
		}
	}*/

	if (nType == DATA_SC8RAW) {
		if (m_tSC8 < 0) return FALSE;
		else {
			*pTime = m_tSC8;
			*pRaw = m_sc8Raw;
			return TRUE;
		}
	}
	return FALSE;
}

void clsIM8::SetType(char chType)
{
//	char bufGP6[] = {0x55,0x55,0x53,0x46,0x01,0x00,0x03,0x00,0x41,0x00,0xDE};
//	char bufAH6[] = {0x55,0x55,0x53,0x46,0x01,0x00,0x03,0x00,0x41,0x00,0xDE};
//	char bufSC6[] = {0x55,0x55,0x53,0x46,0x01,0x00,0x03,0x00,0x41,0x00,0xDE};

//	char bufGP8[] = {0x55,0x55,0x53,0x46,0x05,0x01,0x00,0x03,0x4E,0x30,0x36,0x71};	// N0
	char bufGP8[] = {0x55,0x55,0x53,0x46,0x05,0x01,0x00,0x03,0x4E,0x31,0x26,0x50};	// N1
	char bufAH8[] = {0x55,0x55,0x53,0x46,0x05,0x01,0x00,0x03,0x41,0x30,0x26,0x4F};	// A0
	char bufSC8[] = {0x55,0x55,0x53,0x46,0x05,0x01,0x00,0x03,0x53,0x30,0x43,0x5E};	// S0

	char *pBuffer = NULL;
	if (chType == 'N') pBuffer = bufGP8;
	else if (chType == 'A') pBuffer = bufAH8;
	else pBuffer = bufSC8;

	::write(m_nsIM8, pBuffer, 12);
}

void clsIM8::SetRate(int nRate)
{
	// NAV420 continuous mode configuration
/*	char buf0Hz[]	= {0x55,0x55,0x53,0x46,0x01,0x00,0x01,0x00,0x00,0x00,0x9B};
	char buf100Hz[]	= {0x55,0x55,0x53,0x46,0x01,0x00,0x01,0x00,0x01,0x00,0x9C};
	char buf50Hz[]	= {0x55,0x55,0x53,0x46,0x01,0x00,0x01,0x00,0x02,0x00,0x9D};
	char buf25Hz[]	= {0x55,0x55,0x53,0x46,0x01,0x00,0x01,0x00,0x04,0x00,0x9F};
	char buf20Hz[]	= {0x55,0x55,0x53,0x46,0x01,0x00,0x01,0x00,0x05,0x00,0xA0};*/

	// NAV440 continuous mode configuration
	char buf0Hz[] 	= {0x55,0x55,0x53,0x46,0x05,0x01,0x00,0x01,0x00,0x00,0x40,0x81};
	char buf100Hz[] = {0x55,0x55,0x53,0x46,0x05,0x01,0x00,0x01,0x00,0x01,0x50,0xA0};
	char buf50Hz[]	= {0x55,0x55,0x53,0x46,0x05,0x01,0x00,0x01,0x00,0x02,0x60,0xC3};

	char *pBuffer = NULL;

	if (nRate == 0) pBuffer = buf0Hz;
	else if (nRate == 100) pBuffer = buf100Hz;
	else if (nRate == 50) pBuffer = buf50Hz;
//	else if (nRate == 25) pBuffer = buf25Hz;
//	else if (nRate == 20) pBuffer = buf20Hz;
	else return;

	write(m_nsIM8, pBuffer, 12);
}

void clsIM8::Poll(char chType)
{
//	char bufGP6[] = {0x55,0x55,0x47,0x50,0x4E,0x00,0xE5};	// previous NAV420
//	char bufGP8[] = {0x55,0x55,0x47,0x50,0x02,0x4e,0x30,0x84,0xb9}; 	// N0
	char bufGP8[] = {0x55,0x55,0x47,0x50,0x02,0x4e,0x31,0x94,0x98}; 	// N1
	char bufAH8[] = {0x55,0x55,0x47,0x50,0x02,0x41,0x30,0x94,0x87};		// A0
	char bufSC8[] = {0x55,0x55,0x47,0x50,0x02,0x53,0x30,0xf1,0x96};		// S0

	char *pBuffer = NULL;
	if (chType == 'N')
		pBuffer = bufGP8;
	else if (chType == 'A')
		pBuffer = bufAH8;
	else
		pBuffer = bufSC8;

	m_tRequest = GetTime();
}

void clsIM8::PushPack(double tPack, IM8RAWPACK *pPackRaw)
{
	IM8PACK pack;
	Translate(pPackRaw, &pack);

//#if (_DEBUG & DEBUGFLAG_IM8)
if (m_nCount % _DEBUG_COUNT == 0) {
	GP8PACK &gp8 = pack.gp8;
//	AH8PACK &ah8 = pack.ah8;
//	SC8PACK &sc8 = pack.sc8;

//	printf("[IM8] PushPack, time %.3f, pack %c\n", tPack, pPackRaw->gp8.type);

	switch (pack.gp8.type) {
	case DATA_GP8:
		printf("[IM8] GP8 data, roll angle %.3f, pitch angle %.3f, heading angle %.3f\n",
			gp8.a, gp8.b, gp8.c);
//		printf("[IM8] GP8 data, longtitude %.3f, latitude %.3f, altitude %.3f\n",
//			gp8.longitude, gp8.latitude, gp8.altitude);
		break;
/*	case DATA_AH8:
		printf("[IM8] AH8 data, roll angle %.3f, pitch angle %.3f, heading angle %.3f\n",
			ah8.a, ah8.b, ah8.c);
		break;
	case DATA_SC8:
		printf("[IM8] SC8 data, acx %.3f, acy %.3f, acz %.3f\n",
			sc8.acx, sc8.acy, sc8.acz);
		break; */
	}
}
//#endif

	switch (pack.gp8.type) {
	case DATA_GP8:
		m_tGP8 = tPack;
		m_gp8Raw = *pPackRaw;
		m_gp8 = pack;
		break;
/*	case DATA_AH8:
		m_tAH8 = tPack;
		m_ah8Raw = *pPackRaw;
		m_ah8 = pack;
		break;*/
	case DATA_SC8:
		m_tSC8 = tPack;
		m_sc8Raw = *pPackRaw;
		m_sc8 = pack;
		break;
	}

	if (_state.GetSimulationType() == 0)
		_state.Update(tPack, &pack);

	pthread_mutex_lock(&m_mtxIM8);
	if (m_nIM8 != MAX_IM8PACK) {
		m_tIM8[m_nIM8] = tPack;
		m_im8Raw[m_nIM8] = *pPackRaw;
		m_im8[m_nIM8++] = pack;
	}
	pthread_mutex_unlock(&m_mtxIM8);
}

int clsIM8::GetPack(IM8RAWPACK raw[], int nMaxPack)
{
	int nRead = read(m_nsIM8, m_szBuffer /*+ m_nBuffer*/, MAX_IM8BUFFER /*- m_nBuffer*/);		//get data as many as it can
//	printf("Byte read: %d\n", nRead);
	if (nRead == 0 || nRead == -1) {
		double tLost = ::GetTime();
		fprintf(m_pfStuckLog, "No data at %.3f\n", tLost);
	}

	if (nRead > 0) {
		m_nBuffer = nRead;
	}
	else {
		m_nBuffer = 0;
	}
//		m_nBuffer += nRead;

#if (_DEBUG & DEBUGFLAG_IM8)
if (m_nCount % _DEBUG_COUNT == 0)
{
		printf("[IM8] IM8GetPack, bytes read %d, new length %d\n", nRead, m_nBuffer);
		for (int i=0; i<=m_nBuffer-1; i++)
		{
			printf("%02x ", (unsigned char)m_szBuffer[i]);
		}
		printf("\n");
}
#endif

	int nPack = 0;
	int iNext = -1;
	int nSize = -1;
	for (int i = 0; i <= m_nBuffer - 1; )
	{
		if ( (unsigned char)m_szBuffer[i] != 0xff ) {
			i++;
			continue;
		}
		if (i == m_nBuffer-1) {
			iNext = i;
			break;
		}

		if (m_szBuffer[i+1] != 0x02) {
			i += 2;
			continue;
		}
		if (i == m_nBuffer - 2) {
			iNext = i;
			break;
		}

		if ( (unsigned char)m_szBuffer[i+2] != 0x90 ) {
			i += 3;
			continue;
		}
		if (i == m_nBuffer - 3)	{
			iNext = i;
			break;
		}

		nSize = (unsigned short)((unsigned char)m_szBuffer[i+3] | (unsigned char)m_szBuffer[i+4]);
//		cout<<"packet size: "<<nSize<<endl; // nSize = 92

		if (i+nSize > m_nBuffer) {
			iNext = i;
			break;
		}

		if ( (unsigned char)m_szBuffer[i+nSize+7] != 0x03 ) {		// end of pkt: 0x03
			i += 4;
			continue;
		}

//		unsigned short crcCalc = CheckCRC(m_szBuffer+i+2, nSize-4);
//		unsigned short crcCheck = m_szBuffer[i+nSize-2] << 8 | (unsigned char)m_szBuffer[i+nSize-1];
		unsigned short crcCalc = CheckCRC1((const unsigned char *)(m_szBuffer+i+2), nSize+3);
		unsigned short crcCheck = (unsigned short)((unsigned char)m_szBuffer[i+nSize+5] << 8 | (unsigned char)m_szBuffer[i+nSize+6]);

		if (crcCalc != crcCheck) {
			i += 4;
//			cout<<"checksum error"<<endl;
			continue;
		}

		memcpy(m_szDataBuf, m_szBuffer+i+5, nSize);
		if (++nPack == nMaxPack) {
			// hardly reach here...
			iNext = i + nSize + 8;
			if (iNext > m_nBuffer - 1)
				iNext = -1;
			break;
		}
		iNext = i + nSize + 8;
		i += nSize + 8;			// now i indicates the next imu packet
//		printf("[IM8] iNext %d, i %d, m_nBuffer %d\n", iNext, i, m_nBuffer);
//		printf("[IM8] new start byte %02x \n", (unsigned char)m_szBuffer[i]);
	}	//end of for loop

	if (nPack==0)
	{
		m_nLost++;
		fprintf(m_pfStuckLog, "Lost at %.4f %d\n", ::GetTime(), m_nLost);
//		char str[20] = {0};
//		sprintf(str, "Lost packet %d\n", m_nLost);
//		_cmm.PutMessage(str);
//
//		printf("[IM6] IM6GetPack, bytes read %d, new length %d\n", nRead, m_nBuffer);
//		for (int i=0; i<=m_nBuffer /*nRead*/-1; i++)
//		{
//			printf("%02x ", (unsigned char)m_szBuffer[i]);
//		}
//		printf("\n");
//		printf("0 pkt\n");
	}

	if (iNext != -1) {
//		m_nBuffer -= iNext;
//		if ( (m_nBuffer > 0) && (m_nBuffer )
//		memcpy(m_szBuffer, m_szBuffer+iNext, m_nBuffer);
	}
	else {	// no data left in the buffer to process
		m_nBuffer = 0;
	}


#if (_DEBUG & DEBUGFLAG_IM8)
if (m_nCount % _DEBUG_COUNT == 0)
{
		printf("[IM8] IM8GetPack, buffer left %d\n", m_nBuffer);
		for (int i=0; i<=m_nBuffer-1; i++)
		{
			printf("%02x ", (unsigned char)m_szBuffer[i]);
		}
		printf("\n");
}
#endif


	return nPack;
}

void clsIM8::ExtractPackFromBuffer(char* pBuffer, IM8RAWPACK* pPack, int nSize)
{
/*	for (int i=0; i<n; i++) {
		printf("IM8RAWPACK: %02x ", pPack[i]);
	}
	printf("\n");*/
}

void clsIM8::Translate()
{
	m_gp8.gp8.type = DATA_GP8;
	m_gp8.gp8.a = (double)(*(float *)m_szDataBuf);
	m_gp8.gp8.b = (double)(*(float *)(m_szDataBuf+4));
	m_gp8.gp8.c = (double)(*(float *)(m_szDataBuf+8));

	m_gp8.gp8.p = (double)(*(float *)(m_szDataBuf+12));
	m_gp8.gp8.q = (double)(*(float *)(m_szDataBuf+16));
	m_gp8.gp8.r = (double)(*(float *)(m_szDataBuf+20));

	m_gp8.gp8.acx = (double)(*(float *)(m_szDataBuf+24));
	m_gp8.gp8.acy = (double)(*(float *)(m_szDataBuf+28));
	m_gp8.gp8.acz = (double)(*(float *)(m_szDataBuf+32));

	m_gp8.gp8.temp1 = (float)(*(float *)(m_szDataBuf+36));
	m_gp8.gp8.temp2 = (float)(*(float *)(m_szDataBuf+40));

	m_gp8.gp8.deviceStatus = GETLONG(m_szDataBuf + 44);
	m_gp8.gp8.gpstime = GETLONG(m_szDataBuf+48);
	m_gp8.gp8.gpsinfo = *(uint8_t *)(m_szDataBuf+52);
	m_gp8.gp8.nGPS = *(uint8_t *)(m_szDataBuf+53);

	m_gp8.gp8.pressure = GETLONG(m_szDataBuf + 54);

	m_gp8.gp8.latitude = GETDOUBLE(m_szDataBuf+58) * PI/180;
	m_gp8.gp8.longitude = GETDOUBLE(m_szDataBuf+66) * PI/180;
	m_gp8.gp8.altitude = GETDOUBLE(m_szDataBuf+74);

	m_gp8.gp8.u = (double)(GETFLOAT(m_szDataBuf+82));
	m_gp8.gp8.v = (double)(GETFLOAT(m_szDataBuf+86));
	m_gp8.gp8.w = (double)(GETFLOAT(m_szDataBuf+90));

	m_gp8.gp8.year = *(uint8_t *)(m_szDataBuf+94);
	m_gp8.gp8.month = *(uint8_t *)(m_szDataBuf+95);
	m_gp8.gp8.day = *(uint8_t *)(m_szDataBuf+96);
	m_gp8.gp8.hour = *(uint8_t *)(m_szDataBuf+97);
	m_gp8.gp8.minutes = *(uint8_t *)(m_szDataBuf+98);
	m_gp8.gp8.seconds = *(uint8_t *)(m_szDataBuf+99);
	m_gp8.gp8.nanoseconds = *(uint32_t *)(m_szDataBuf+100);
}

void clsIM8::Translate(IM8RAWPACK *pPackRaw, IM8PACK *pPack)
{
	GP8RAWPACK &gp8raw = pPackRaw->gp8;
	GP8PACK &gp8 = pPack->gp8;

	gp8.type = DATA_GP8;
	gp8.a = (double)((float)gp8raw.a);
	gp8.b = (double)((float)gp8raw.b);
	gp8.c = (double)((float)gp8raw.c);

	gp8.p = (double)((float)gp8raw.p);
	gp8.q = (double)((float)gp8raw.q);
	gp8.r = (double)((float)gp8raw.r);

	gp8.acx = (double)((float)gp8raw.acx);
	gp8.acy = (double)((float)gp8raw.acy);
	gp8.acz = (double)((float)gp8raw.acz);

	gp8.u = (double)((float)gp8raw.u);
	gp8.v = (double)((float)gp8raw.v);
	gp8.w = (double)((float)gp8raw.w);

	gp8.longitude = (double)gp8raw.longitude;
	gp8.latitude = (double)gp8raw.latitude;
	gp8.altitude = (double)gp8raw.altitude;

};

bool clsIM8::GetRTKOutput(IM8PACK& RTKOutputs){
	char buffer[1024];
//	printf("[GetRTKOutput]\n");
	int nRead = read(m_nsIM8, buffer, 1024);		//get data as many as it can

	if (nRead <= 0) return false;
//	printf("[im8] readin %d bytes data\n", nRead);

//	for (int i = 0; i < nRead; i++){
//		printf("%02x ", (unsigned char)buffer[i]);
//	}
//	printf("\n");

	if (m_nBuffer + nRead > MAX_IM8BUFFER){
		m_nBuffer = 0;
		return false;
	}
	memcpy(m_szBuffer + m_nBuffer, buffer, nRead);
	m_nBuffer += nRead;

	//decode the packages... always keep the newest data
	bool stop 	= false;
	bool decodeRTKSuccess = false;
	int  index 	= 0;

	unsigned long crc = 0;

	do{
		switch (m_enumDecodeState){
			case BGN_SYNC1:{
				if ((unsigned char)m_szBuffer[index++] == 0xAA){
					m_enumDecodeState = BGN_SYNC2;
					// cal CRC...
					CalculateRTKCRC32((unsigned char)m_szBuffer[index-1]);
					//printf("stop here: BGN_SYNC1\n");
				}
				break;
			}
			case BGN_SYNC2:{
				if ((unsigned char)m_szBuffer[index++] == 0x44){
					m_enumDecodeState = BGN_SYNC3;
					CalculateRTKCRC32((unsigned char)m_szBuffer[index-1]);
					//printf("stop here: BGN_SYNC2\n");
				}
				else{
					m_enumDecodeState = BGN_SYNC1;
					m_ulRTKCRC = 0;
				}
				break;
			}
			case BGN_SYNC3:{
				if ((unsigned char)m_szBuffer[index++] == 0x12){
					m_enumDecodeState = HDER_LGTH;
					CalculateRTKCRC32((unsigned char)m_szBuffer[index-1]);
					//printf("stop here: BGN_SYNC3\n");
				}
				else{
					m_enumDecodeState = BGN_SYNC1;
					m_ulRTKCRC = 0;
				}
				break;
			}
			case HDER_LGTH:{
				m_nHderLgth = (unsigned char)m_szBuffer[index++];
				CalculateRTKCRC32((unsigned char)m_szBuffer[index-1]);
				m_enumDecodeState = MSG_ID;
				//printf("stop here: HDER_LGTH: %d\n", m_nHderLgth);
				break;
			}
			case MSG_ID:{
				// check whether we have enough bytes to decode...
				if (index + 2 > m_nBuffer) {
					// stop the decoding, wait for more data coming...
					stop = true;
					break;
				}
				for (int i = 0; i < 2; i++){
					CalculateRTKCRC32((unsigned char)m_szBuffer[index + i]);
				}
				memcpy(&m_nMsgId, m_szBuffer + index, 2); // skip the 1 byte header lgth
				index += 2;
				m_enumDecodeState = MSG_SIZE;
				//printf("stop here: MSG_ID: %d\n", m_nMsgId);
				break;
			}
			case MSG_SIZE:{
				// check whether we have enough bytes to decode...
				if (index + 4 > m_nBuffer){
					// stop the decoding, wait for more data coming...
					stop = true;
					break;
				}
				for (int i = 0; i < 4; i++){
					CalculateRTKCRC32((unsigned char)m_szBuffer[index + i]);
				}
				memcpy(&m_nMsgLgth, m_szBuffer + index + 2, 2);
				index += 4;
				if (m_nMsgLgth > 512){
					// considered as wrong msg...
					m_enumDecodeState = BGN_SYNC1;
					printf("[im8] m_nMsgLgth: wrong size...\n");
					break;
				}
				m_enumDecodeState = MSG_CONTENT;
				//printf("stop here: MSG_SIZE: %d\n", m_nMsgLgth);
				break;
			}
			case MSG_CONTENT:{
				// check whether we have enough bytes to decode...
				if (index + (m_nHderLgth - 10) +  m_nMsgLgth > m_nBuffer){
					// stop the decoding, wait for more data coming...
					stop = true;
					break;
				}
				for (int i = 0; i < ((m_nHderLgth - 10) +  m_nMsgLgth); i++){
					CalculateRTKCRC32((unsigned char)m_szBuffer[index + i]);
				}
				memcpy(m_msgContent, m_szBuffer + index + (m_nHderLgth - 10), m_nMsgLgth);
				index += ((m_nHderLgth - 10) + m_nMsgLgth);
				m_enumDecodeState = CRC;
				//printf("stop here: MSG_CONTENT\n");
				break;
			}
			case CRC:{
				// check whether we have enough bytes to process...
				if (index + 4 > m_nBuffer){
					stop = true;
					break;
				}
				memcpy(&crc, m_szBuffer + index, 4);
				index += 4;
				m_enumDecodeState = BGN_SYNC1;
				// clear m_ulRTKCRC...
				if (crc == m_ulRTKCRC){
					// data is correct...
					// TODO: DO translations...
					TranslateRTKOutput(RTKOutputs, m_nMsgId, m_nMsgLgth, m_msgContent);
					decodeRTKSuccess = true;
					//printf("[im8] RTK check sum correct!\n");
				}
				m_ulRTKCRC = 0;
				//printf("stop here: CRC\n");
				break;
			}
		}
	}while(!stop && index < m_nBuffer && index > 0);

	// backup remaining unprocessed data
	char temp[1024];
	if (m_nBuffer - index > 0){
		memcpy(temp, m_szBuffer + index, m_nBuffer - index);
		memcpy(m_szBuffer, temp, m_nBuffer - index);
	}
	m_nBuffer = m_nBuffer - index;
	//printf("[im8] m_nBuffer: %d index: %d\n\n", m_nBuffer, index);
	return decodeRTKSuccess;
}

void clsIM8::TranslateRTKOutput(IM8PACK& RTKOutputs, short msgID, short msgLgth, char msgContent[]){
	RTKOutputs.gp8.type = DATA_GP8;

	switch (msgID){
	case MSG_BESTPOS_ID:{
		memcpy(&m_strucRTKPos, msgContent, msgLgth);
		RTKOutputs.gp8.latitude  = m_strucRTKPos.lat*PI/180.0; // in radians...
		RTKOutputs.gp8.longitude = m_strucRTKPos.lon*PI/180.0;
		RTKOutputs.gp8.altitude  = m_strucRTKPos.hgt;
		RTKOutputs.gp8.nGPS		 = m_strucRTKPos.numOfSatUsed;
		if (m_strucRTKPos.enumSolStatus == 0){ // solution computed...
			RTKOutputs.gp8.gpsinfo = 0x03;
		}
//		printf("[im8] size: %d %d\n", sizeof(m_strucRTKPos), msgLgth);

		break;
	}
	case MSG_BESTVEL_ID:{
		memcpy(&m_strucRTKVel, msgContent, msgLgth);
		double nedXVel = 0; double nedYVel = 0;
		nedXVel = m_strucRTKVel.horizontalSpeed*cos(m_strucRTKVel.heading*PI/180.0);
		nedYVel = m_strucRTKVel.horizontalSpeed*sin(m_strucRTKVel.heading*PI/180.0);

		RTKOutputs.gp8.u		 = nedXVel;
		RTKOutputs.gp8.v	     = nedYVel;
		RTKOutputs.gp8.w		 = m_strucRTKVel.verticalSpeed*(-1);
//		printf("[im8] RTKOutput Vel: %d %lf %lf %lf\n", m_strucRTKVel.enumSolStatus, RTKOutputs.gp8.u, RTKOutputs.gp8.v, RTKOutputs.gp8.v);
		break;
	}
	case MSG_UTCTIME_ID:{
		memcpy(&m_strucRTKTime, msgContent, msgLgth);
		RTKOutputs.gp8.year 	 =  m_strucRTKTime.utcYear;
		RTKOutputs.gp8.month 	 =  m_strucRTKTime.utcMonth;
		RTKOutputs.gp8.day 	 	 =  m_strucRTKTime.utcDay;
		RTKOutputs.gp8.hour 	 =  m_strucRTKTime.utcHour;
		RTKOutputs.gp8.minutes 	 =  m_strucRTKTime.utcMin;
		RTKOutputs.gp8.seconds 	 =  m_strucRTKTime.utcMilliSecond*0.001;
		RTKOutputs.gp8.nanoseconds 	 =  m_strucRTKTime.utcMilliSecond*1000000;
//		if (m_nCount % 2 == 0){
//			printf("[im8] RTKOutput UTCTime: %d %d %d %d %d %d\n", RTKOutputs.gp8.year, RTKOutputs.gp8.month, RTKOutputs.gp8.day, RTKOutputs.gp8.hour, RTKOutputs.gp8.minutes, RTKOutputs.gp8.seconds);
//		}
		break;
	}

	}
	if (m_nCount % 9 == 0){
				printf("\n[im8] RTKOutput: solType: %d gpsinfo: %02x posType: %d lat: %.10f %.10f %.10f sol_age: %f #Sat: %d #solSat: %d\n", m_strucRTKPos.enumSolStatus, RTKOutputs.gp8.gpsinfo, m_strucRTKPos.enumPosType, RTKOutputs.gp8.latitude, RTKOutputs.gp8.longitude, RTKOutputs.gp8.altitude, m_strucRTKPos.sol_age, RTKOutputs.gp8.nGPS, m_strucRTKPos.numOfSatUsed);
	}
}

unsigned long clsIM8::CalculateRTKCRC32(unsigned char ucBuffer){
	unsigned long ulTemp1;
	unsigned long ulTemp2;

	ulTemp1 = ( m_ulRTKCRC >> 8 ) & 0x00FFFFFFL;
	ulTemp2 = RTKCRC32Value( ((int) m_ulRTKCRC ^ ucBuffer ) & 0xff );
	m_ulRTKCRC = ulTemp1 ^ ulTemp2;

	return( m_ulRTKCRC );
}
unsigned long clsIM8::RTKCRC32Value (int i){
	int j;
	unsigned long ulCRC;
	ulCRC = i;
	for ( j = 8 ; j > 0; j-- ){
		if ( ulCRC & 1 )
			ulCRC = ( ulCRC >> 1 ) ^ CRC32_POLYNOMIAL;
		else
			ulCRC >>= 1;
	}
	return ulCRC;
}




