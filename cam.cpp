/*
 * cam.cpp
 *
 *  Created on: Mar 15, 2011

 */

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>

// include the c library: libspi-master.a
extern "C" {
	#include <hw/spi-master.h>
}

//#include "uav.h"
#include "cam.h"
#include "state.h"
#include "cmm.h"
#include "main.h"

extern EQUILIBRIUM _equ_Hover;
extern clsState _state;
extern clsCMM _cmm;

/*
 * clsCAM
 */
clsCAM::clsCAM()
{
	m_nsCAM = -1;
	m_ledStatus = 0;

	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutex_init(&m_mtxCAM, &attr);
}

clsCAM::~clsCAM()
{
}

BOOL clsCAM::Init()
{
	if (_main.GetLionHubVersion() == LIONHUB_OLD) {
		m_nsCAM = open("/dev/serusb2", O_RDWR /*| O_NONBLOCK*/);

		termios termCAM;
	    tcgetattr(m_nsCAM, &termCAM);

		cfsetispeed(&termCAM, CAM_BAUDRATE);				//input and output baudrate
		cfsetospeed(&termCAM, CAM_BAUDRATE);

		termCAM.c_cflag = CS8 |CLOCAL|CREAD;

		tcsetattr(m_nsCAM, TCSANOW, &termCAM);
		tcflush(m_nsCAM, TCIOFLUSH);
	}
	else if (_main.GetLionHubVersion() == LIONHUB_NEW) {
		spi_devinfo_t	info;
		m_nsCAM = spi_open("/dev/spi0");
		if (m_nsCAM == -1) {
			printf("[CAM] open spi0 failed!\n");
			return FALSE;
		}

		spi_cfg_t	cfg;
	//	cfg.mode        = 8 & SPI_MODE_CHAR_LEN_MASK | SPI_MODE_BODER_MSB ;
		cfg.mode        = 8 | SPI_MODE_BODER_MSB ;
	//    cfg.mode    = (7 & SPI_MODE_CHAR_LEN_MASK) | SPI_MODE_CKPOL_HIGH | SPI_MODE_CKPHASE_HALF;
		cfg.clock_rate  = 1000;

		if (spi_setcfg(m_nsCAM, 0x00 | SPI_DEV_DEFAULT, &cfg) != EOK) {
			spi_close(m_nsCAM);
			printf("spi_setcfg failed.\n");
			return -1;
		}

		int i = spi_getdevinfo(m_nsCAM, 0x00 | SPI_DEV_DEFAULT  , &info);
		if (i == EOK) {
	//		printf("device     = %x\n", info.device);
	//		printf("name       = %s\n", info.name);
	//		printf("mode       = %x\n", info.cfg.mode);
	//		printf("clock_rate = %d\n", info.cfg.clock_rate);
		}
		else
		{
			printf("i       = %d\n", i);
			printf("get spi device infor failed!!\n");
		}
	}

	return TRUE;
}

BOOL clsCAM::InitThread()
{
	m_nBuffer = 0;
	m_tInfo0 = -1;

	m_nInfo=0;

	m_DetectState = 0;
	::memset(&m_targetInfo0, 0, sizeof(TARGETINFO));
	::memset(&m_targetState0, 0, sizeof(TARGETSTATE));

	int index;

	for (index = 0; index < MAX_CAMINFO; index++) {
		m_tInfo[index]  = 0;
		m_info[index].l = 0;
		m_info[index].m = 0;
		m_info[index].n = 0;
	}

	printf("[CAM] start\n");

	return TRUE;
}

void *clsCAM::InputThread(void *pParameter)
{
	clsCAM *pCAM = (clsCAM *)pParameter;

	pCAM->Input();

	printf("[CAM input] thread %d\n", pthread_self());
	return NULL;
}

BOOL clsCAM::StartInputThread(int priority)
{
    pthread_attr_t attribute;
    pthread_attr_init(&attribute);
    pthread_attr_setdetachstate(&attribute, PTHREAD_CREATE_DETACHED);
    pthread_attr_setinheritsched(&attribute, PTHREAD_EXPLICIT_SCHED);
    pthread_attr_setschedpolicy(&attribute, SCHED_RR);

    sched_param_t param;
    pthread_attr_getschedparam(&attribute, &param);
    param.sched_priority = priority;
    pthread_attr_setschedparam(&attribute, &param);

	pthread_t thread;
    pthread_create(&thread, &attribute, &clsCAM::InputThread, this);

    return TRUE;
}

void clsCAM::Input()
{
	printf("[CAM] start (input thread) %d\n", pthread_self());

	COMMAND cmd;
	m_nCountCAMRD=0;

	while (1) {
		//if (!ReadCommand(&cmd)) { usleep(20000); continue; }

		//::memcpy(&info, cmd.parameter, 4*sizeof(double));
		/*info.l=(short &)cmd.parameter[0];
		info.m=(short &)cmd.parameter[2];
		info.n=(short &)cmd.parameter[4];	*/

		if (ReadCommand(&cmd)) {
		}

		m_nCountCAMRD++;
		usleep(100000);
	}
}

BOOL clsCAM::ReadCommand(COMMAND *pCmd)
{
	int nRead = read(m_nsCAM, m_buffer+m_nBuffer, MAX_CAMBUFFER-m_nBuffer);	//get data as many as it can

	if (nRead < 0) return FALSE;
//	if (nRead < 0) nRead = 0;

	m_nBuffer += nRead;
//	printf("Read %d bytes\n", nRead);
/*	for (int i=0; i<m_nBuffer; i++) {
		printf("%02x ", (unsigned char)m_buffer[i]);
	}
	printf("\n");*/

#if ( _DEBUG & DEBUGFLAG_CAM )
	if(m_nCountCAMRD%_DEBUG_COUNT_CAMRD ==0){
	if(nRead!=0){
		printf("\n");
		printf("[CAM] ReadCommand, read byte %d, buffer size %d\n", nRead, m_nBuffer);
//     	char *pdChar = m_buffer;
//    	printf("[CAM] ReadCommand Buffer=");
//    	while(pdChar< m_buffer+nRead){
//    		printf("%4x", *(unsigned char*)pdChar);  pdChar++;
//   		}
//    	printf("\n");
	}
	}
#endif

	COMMAND cmd = {0};

	char *pChar = m_buffer;
	char *pCharMax = m_buffer+m_nBuffer-1;
	enum { BEGIN, HEADER, SIZE, PACKAGE, CHECK } state = BEGIN;

	char package[MAXSIZE_PACKAGE];
	int nPackageSize = 0;

	char *pNextTelegraph = NULL;

	while (pChar <= pCharMax) {
		if (state == BEGIN) {
			if ( (unsigned char)*pChar == 0x8b /*-121*/ ) state = HEADER;
			pChar ++; continue;
		}

		if (state == HEADER) {
//			if ((*pChar >= 0 && *pChar <= 99) || *pChar & 0x80) state = SIZE;
			if ( (unsigned char)*pChar == 0x85 ) state = SIZE;
			else state = BEGIN;
			pChar ++; continue;
		}

		if (state == SIZE) {
			if (pChar + 1 > pCharMax) break;

//			nPackageSize = *(short *)pChar;
			nPackageSize = GETWORD(pChar);
			if (nPackageSize < 1 || nPackageSize > MAXSIZE_PACKAGE) { state = BEGIN; continue; }
			else { state = PACKAGE; pChar += 2; continue; }
		}

		if (state == PACKAGE) {
			if (pChar + nPackageSize + 1 > pCharMax) break;

			unsigned short sum = CheckSum(pChar, nPackageSize);
			unsigned short check = GETWORD(pChar+nPackageSize);

//#if (_DEBUG & DEBUGFLAG_CAM)
//		printf("[CAM] Readcommand PACKAGE: sum=%d, check=%d\n", sum, check);
//#endif
			if (sum != check) {
				printf("[CAM] checksum error\n");
				state = BEGIN; continue;
			}

			::memcpy(package, pChar, nPackageSize);		// copy the correct package

			pNextTelegraph = pChar + nPackageSize + 2;

//			break;
			pChar=pNextTelegraph;
		}
	}

	//if (pNextTelegraph == NULL) return FALSE;
	if ( (pNextTelegraph==NULL)) {//didn't find a completed data package
//		if (state!=PACKAGE) {// the state is not PACKAGE
//			m_nBuffer=0;
//		}
//		printf("pNextTelegraph NULL!\n");
		return FALSE;
	}

	if (pNextTelegraph > pCharMax) {
//		printf("pNextTelegraph > pCharMax!\n");
		m_nBuffer = 0;
	}
	else {
		m_nBuffer = pCharMax - pNextTelegraph + 1; //unfinished telegraph copy to buffer for prcessing in next cycle
		::memcpy(m_buffer, pNextTelegraph, m_nBuffer);
	}

	cmd.code = GETWORD(package);

//	m_DetectState = GETWORD(package+2);
	::memcpy(cmd.parameter, package+10, sizeof(/*TARGETINFO*/CAMTELEINFO) );
	*pCmd = cmd;

	return TRUE;
}

void clsCAM::Update()
{
//	UAVSTATE &state = _state.GetState();

	/*double l, m, n, h;

	h = state.z;

	m_tTARGETState0   = m_tInfo2;
	m_TARGETState0.tq = atan(-1*n/sqrt(l*l+m*m));
	m_TARGETState0.tr = atan(m/l);
	*/
}

void clsCAM::GetInfo()
{
	double tInfo = 0;
	int index, tIndex=0;

	for (index=0; index< MAX_CAMINFO; index++) {
		if ( m_tInfo[index]>tInfo ) {
			tIndex = index;
			tInfo  = m_tInfo[index];
		}
	}

	m_tInfo2 = tIndex;
	m_info2.l = m_info[tIndex].l;
	m_info2.m = m_info[tIndex].m;
	m_info2.n = m_info[tIndex].n;
}

void clsCAM::PutCommand(COMMAND *pCmd)
{
	pthread_mutex_lock(&m_mtxCmd);

	m_cmd = *pCmd;

	pthread_mutex_unlock(&m_mtxCmd);
}

void clsCAM::GetCommand(COMMAND *pCmd)
{
	pthread_mutex_lock(&m_mtxCmd);

	if (m_cmd.code == 0) pCmd->code = 0;
	else {
		*pCmd = m_cmd;
		m_cmd.code = 0;				//clear
	}

	pthread_mutex_unlock(&m_mtxCmd);
}

BOOL clsCAM::ProcessCommand(COMMAND *pCommand)
{
	COMMAND &cmd = *pCommand;
//	char *paraCmd = cmd.parameter;

//	int &behavior = m_behavior.behavior;
//	char *paraBeh = m_behavior.parameter;

	BOOL bProcess = TRUE;
//	_state.EnableVision();
	switch (cmd.code) {
	case COMMAND_CAMVBS:
		break;
	default:
		bProcess = FALSE;
		break;
	}
	return bProcess;
}

void clsCAM::MakeTelegraph(TELEGRAPH *pTele, short code, double time, void *pData, int nDataSize)
{
	char *pBuffer = pTele->content;

	//pBuffer[0] = 0x50 + _HELICOPTER;				//_nHelicopter 6 or 7, 0x56 for helion, 0x57 for shelion
	//pBuffer[1] = 0x55;				//ground station

//	pBuffer[0] = 0x57;				//_nHelicopter 6 or 7, 0x56 for helion, 0x57 for shelion
//	pBuffer[1] = 0x57;

	pBuffer[0] = 0x87;				//_nHelicopter 6 or 7, 0x56 for helion, 0x57 for shelion
	pBuffer[1] = 0x93;

	switch(code) {
		case COMMAND_CAMRUN:
		case COMMAND_CAMSTOP:
		case COMMAND_CAMQUIT:
		case COMMAND_CAMTHOLD: {
//			(short &)pBuffer[2] = nDataSize+2;
//			(short &)pBuffer[4] = code;
//			(double &)pBuffer[6] = *(double*)pData;
//			(unsigned short &)pBuffer[6+nDataSize] = CheckSum(pBuffer+4, 2+nDataSize);
//			pTele->size = nDataSize + 8;
			PUTWORD(pBuffer+2, nDataSize+2);
			PUTWORD(pBuffer+4, code);
			::memcpy(pBuffer+6, pData, nDataSize);

			unsigned short sum  = CheckSum(pBuffer+4, 2+nDataSize);
			PUTWORD(pBuffer+14+nDataSize, sum);

			pTele->size = nDataSize + 8;
			break; }
		case COMMAND_CAMUAVPOSE: {
//			(short &)pBuffer[2] = nDataSize+10;
//			(short &)pBuffer[4] = code;
//			(double &)pBuffer[6]= time;
//			::memcpy(pBuffer+14, pData, nDataSize);
//			(unsigned short &)pBuffer[14+nDataSize] = CheckSum(pBuffer+4, 10+nDataSize);

			PUTWORD(pBuffer+2, nDataSize+10);
			PUTWORD(pBuffer+4, code);
			PUTDOUBLE(pBuffer+6, time);

			::memcpy(pBuffer+14, pData, nDataSize);

//			unsigned short sum  = CheckSum(pBuffer+4, 10+nDataSize);
//			PUTWORD(pBuffer+14+nDataSize, sum);
			char chk = 0x88;
			memcpy(pBuffer + 14 + nDataSize, &chk, 1);
			memcpy(pBuffer + 14 + nDataSize + 1, &chk, 1);
			pTele->size = nDataSize + 16;
			break;}
	}

#if(_DEBUG  & DEBUGFLAG_CAMWR )
	switch(code) {
		case COMMAND_CAMRUN:
		case COMMAND_CAMSTOP:
		case COMMAND_CAMQUIT:
		case COMMAND_CAMTHOLD:
			printf("[CAM] Tele: code=%3d, parameter=%7.3f\n",(short &)pBuffer[4], (double &)pBuffer[6]);
			break;
		case COMMAND_CAMUAVPOSE:
			printf("[CAM] Tele: code=%3d, time=%7.3f\n",(short &)pBuffer[4], (double &)pBuffer[6]);
			printf("[CAM] Tele: data=");

			char *pChar;
			for (pChar=&pBuffer[14]; pChar<pBuffer+nDataSize+14; pChar+=sizeof(double) ) {
				printf("%6.2f", *(double *)pChar);
			}
			printf("\n");

			printf("[CAM] Tele (binary):");
			for (pChar=pBuffer; pChar<pBuffer+nDataSize+16; pChar++ ) {
				printf("%4x", *(unsigned char*)pChar);
			}
			printf("\n");
			break;
	}
#endif

}
int clsCAM::EveryRun()
{
    if (_cps != 13000000)
    	printf("[CAM1] _cps corrupted\n");
//	CAMINFO info;
//	if (!ReadCamInfo(&info)) return TRUE;
	// get the state of the UAV
	UAVSTATE &state = _state.GetState();

//	double t = _state.GetStateTime();
	double t = ::GetTime();


//	state.x=1; state.y=2; state.z=3;
//	state.u=4; state.v=5; state.w=6;
//	state.a=7; state.b=8; state.c=9;
//	state.p=10; state.q=11; state.r=12;
//	state.longitude=13; state.latitude=14; state.altitude=15;

	VICON_DATA viconData = _cmm.GetViconData();
/*	double x[18] = {
//		1.1, 2.2, 3.3,
		viconData.x, viconData.y, viconData.z,
		viconData.a, viconData.b, viconData.c,
		state.a - _equ_Hover.a, state.b - _equ_Hover.b, state.c - _equ_Hover.c,
		state.p - _equ_Hover.p, state.q - _equ_Hover.q, state.r - _equ_Hover.r,
		state.acx, state.acy, state.acz,
		state.longitude, state.latitude, state.altitude
	};*/

	UAVPOSE x = {0};
	x.x = state.x; x.y = state.y; x.z = state.z;
	x.a = state.a; x.b = state.b; x.c = state.c;
	x.acx = state.acx; x.acy = state.acy; x.acz = state.acz;
	x.u = state.u; x.v = state.v; x.w = state.w;
	x.latitude = state.latitude * 180 / PI; x.longitude = state.longitude * 180 / PI; x.altitude = state.altitude;

//	x.x = 1.0; x.y = 2.0; x.z = 1.5;
//	x.altitude = 5.0;
	COMMAND cmd;
	GetCommand(&cmd);

	if (cmd.code != 0) {
		if (ProcessCommand(&cmd)) cmd.code = 0;
	}

	double Parameter0;
	int nWrite;

	TELEGRAPH tele;

	//if (m_nCount != 0 && m_nCount != 100) return TRUE;				//send command in the beginning and 2s later, twice

	switch(cmd.code) {
		case COMMAND_CAMRUN:
		case COMMAND_CAMSTOP:
		case COMMAND_CAMQUIT:
		case COMMAND_CAMVBS:
		    Parameter0 = 1;
		    MakeTelegraph(&tele, cmd.code, t, &Parameter0, sizeof(double));
		    nWrite=write(m_nsCAM, tele.content, tele.size);
		    break;
		case COMMAND_CAMTHOLD:
		    Parameter0 = *(double *)cmd.parameter;
		    MakeTelegraph(&tele, cmd.code, t, &Parameter0, sizeof(double));
		    nWrite=write(m_nsCAM, tele.content, tele.size);
		    break;
	}

	if ( m_nCount % 5 == 0 ) {
//		_state.m_safetyStatus.bGPS = _state.m_safetyStatus.bIMUData = _state.m_safetyStatus.bPixhawkData = true;
		_state.m_safetyStatus.bIMUData = _state.m_safetyStatus.bPixhawkData = true;
		uint8_t nWriteBuf = 1;
		// any safety status is false && in manual mode, LED will flash red
		if ( FALSE /*!_state.GetSafetyStatus()*/ ) {
			nWriteBuf = 101;
			if (_main.GetLionHubVersion() == LIONHUB_OLD) {
				write(m_nsCAM, &nWriteBuf, sizeof(nWriteBuf));
			}
			else if (_main.GetLionHubVersion() == LIONHUB_NEW) {
				spi_write(m_nsCAM, SPI_DEV_LOCK, &nWriteBuf, sizeof(nWriteBuf));
			}
//			printf("[CAM] safety failed!\n");
		}
		else if ( /*_state.GetSafetyStatus() && */ _svo.GetCurrentCTLMode() == CTL_MODE_MANUAL) {
			nWriteBuf = 102;
			if (_main.GetLionHubVersion() == LIONHUB_OLD) {
				write(m_nsCAM, &nWriteBuf, sizeof(nWriteBuf));
			}
			else if (_main.GetLionHubVersion() == LIONHUB_NEW) {
				spi_write(m_nsCAM, SPI_DEV_LOCK, &nWriteBuf, sizeof(nWriteBuf));
			}
//			printf("[CAM] safety OK! %d\n", nWrite);
		}
		else if ( /* _state.GetSafetyStatus() && */ _svo.GetCurrentCTLMode() == CTL_MODE_AUTO && !_ctl.GetSyncPathFlag()) {
			nWriteBuf = 103;
			if (_main.GetLionHubVersion() == LIONHUB_OLD) {
				write(m_nsCAM, &nWriteBuf, sizeof(nWriteBuf));
			}
			else if (_main.GetLionHubVersion() == LIONHUB_NEW) {
				spi_write(m_nsCAM, SPI_DEV_LOCK, &nWriteBuf, sizeof(nWriteBuf));
			}
//			printf("[CAM] auto mode \n");
		}
		else if ( /* _state.GetSafetyStatus() && */ _svo.GetCurrentCTLMode() == CTL_MODE_AUTO && _ctl.GetSyncPathFlag()) {
			int nLEDStatus = GetLEDStatus();
			nWriteBuf = nLEDStatus;
			if (_main.GetLionHubVersion() == LIONHUB_OLD) {
				write(m_nsCAM, &nWriteBuf, sizeof(nWriteBuf));
			}
			else if (_main.GetLionHubVersion() == LIONHUB_NEW) {
				spi_write(m_nsCAM, SPI_DEV_LOCK, &nWriteBuf, sizeof(nWriteBuf));
			}
//			printf("[CAM] path mode %d\n", nWriteBuf);
		}
	}
    if (_cps != 13000000)
    	printf("[CAM2] _cps corrupted\n");

	return TRUE;
}

void clsCAM::ExitThread()
{
//	if (m_pfCAM != NULL) ::fclose(m_pfCAM);
	printf("[CAM] quit\n");
}

void clsCAM::SetCamConfig(char *devPort, short size, short flag, int baudrate)
{
	memcpy(m_camConfig.devPort, devPort, size);
	m_camConfig.flag = flag;
	m_camConfig.baudrate = baudrate;
}
