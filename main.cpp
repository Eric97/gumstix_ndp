/*
 * main.cpp
 *  Created on: Mar 15, 2011
 *
 *  Implement file for clsMain, managing task threads
 */

#include <stdio.h>
#include <pthread.h>
#include <sys/neutrino.h>
#include <sys/netmgr.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/trace.h>
#include <process.h>

#include "uav.h"
#include "cmm.h"
#include "im8.h"
#include "daq.h"
#include "cam.h"
#include "ctl.h"
#include "svo.h"
#include "dlg.h"
#include "user.h"
#include "parser.h"
#include "state.h"

extern clsParser _parser;
extern clsCMM _cmm;
extern clsState _state;
extern EQUILIBRIUM _equ_Hover;
////////////////////////////

const char *_configfile[] = {"quadlion.txt", NULL };				//list of possible name of configration files

int main(int argc, char *argv[])
{
	/*
	 * Turn off all filters of trace event
	 */
//	TraceEvent(_NTO_TRACE_DELALLCLASSES);
//	TraceEvent(_NTO_TRACE_CLRCLASSPID, _NTO_TRACE_KERCALL);
//	TraceEvent(_NTO_TRACE_CLRCLASSTID, _NTO_TRACE_KERCALL);
//	TraceEvent(_NTO_TRACE_CLRCLASSPID, _NTO_TRACE_THREAD);
//	TraceEvent(_NTO_TRACE_CLRCLASSTID, _NTO_TRACE_THREAD);
//
//	// add thread events
//	TraceEvent(_NTO_TRACE_ADDCLASS, _NTO_TRACE_THREAD);
//	// add kernel call events
//	TraceEvent(_NTO_TRACE_ADDCLASS, _NTO_TRACE_KERCALL);
//	// start tracing events
//	TraceEvent(_NTO_TRACE_START);


	struct stat fileName;
	if (!stat("/dumps/nusuav.core", &fileName)) {
		if (unlink("/dumps/nusuav.core"))
			printf("[main] Failed to remove core dump file\n");
		else
			printf("[main] Delete core dump file\n");
	}

	if (argc < 2) {
		printf("[main] missing command");
		return 1;
	}

	if (!strcmp(argv[1], "o")) {
		printf("[main] old LionHub\n");
		_main.SetLionHubVersion(LIONHUB_OLD);
	}

	if (!strcmp(argv[1], "n")) {
		_main.SetLionHubVersion(LIONHUB_NEW);
		printf("[main] new LionHub\n");
	}

/*	struct stat devName;
	if (!stat("/dev/serusb1", &devName)) {
		_main.SetLionHubVersion(LIONHUB_NEW);
	}
	else
		_main.SetLionHubVersion(LIONHUB_OLD);*/

    printf("[main] begin thread %d\n", pthread_self());

    //record the starting time
	_cycle0 = ClockCycles();

	//query working directory
    char path[256];
    ::getcwd(path, sizeof(path));
    printf("[main] current work directory: %s\n", path);

    // change to working directory
    chdir("/fs/sd");
    //load configuration from any available configuration file first
	int i=0;
	while (_configfile[i] != NULL) {
		if (_parser.Load(_configfile[i])) {
			printf("[main] Load configuration file - %s\n", _configfile[i]);
			break;
		}
		i++;
	}

	if (_configfile[i] == NULL) {
		printf("[main] No configuration file found, program quit!\n");
		return -1;
	}

	_parser.Parse2();
	_main.Init();

	printf("[main] Helicopter %d\n", _HELICOPTER);
	printf("Version: %s\n", GITVERSION);
	//launch keyboard input program
    _user.StartUserThread(PRIORITY_USER);
    // lauch UART listening (from vision computer)
	_cam.StartInputThread(PRIORITY_CAM);
	
	//open and launch communication & input through wireless modem (RS232)
	if (!_cmm.Open()) {
		printf("[main] CMM open failed\n");

/*		if (!_cmm.InitSocket()) {
			printf("[main] CMM open socket failed\n");
		}
		else if (!_cmm.StartListenThread(PRIORITY_CMM)) {
			printf("[main] CMM start listen failed\n");
		}*/
	}
	_cmm.StartInputThread(PRIORITY_CMM);
/*	else if (!_cmm.StartInputThread(PRIORITY_CMM)) {
		printf("[main] CMM start input thread failed\n");
	}*/



	_ctl.Init();
	_state.Init();
	_cam.Init();

	sched_param param;
	param.sched_priority = 9;
	pthread_setschedparam(pthread_self(), SCHED_FIFO, &param);
	printf("[Main] set priority to 9\n");
	//loading paths
/*	for (i=0; i<=MAX_PATH-1; i++)
	{
		char szFile[64];
		sprintf(szFile, "%d.txt", i+1);
		if (!_path[i].LoadPath(szFile)) break;
	}*/

	// load corresponding path based on UAV ID
	char szFile[64];
	sprintf(szFile, "%d.txt", _HELICOPTER);
	_path[0].LoadPath(szFile);
	_nPath = 50;

    //loading data for simulation
	if (_state.LoadData("a.dat"))
	{
		::printf("[main] Data loaded from 'a.dat'\n");
	}
	else
	{
		::printf("[main] Load data 'a.dat' failed\n");
	}

	_cmm.SendMessage("System started, type \"run\" to launch program, \"quit\" to exit");

	while (1) {
		COMMAND cmd;
		_user.GetCommand(&cmd);
		if (cmd.code == 0) _cmm.GetCommand(&cmd);

		if (cmd.code == COMMAND_QUIT) break;
		else if (cmd.code == COMMAND_TEST) {
		}
		else if (cmd.code == COMMAND_RUN) {
			int nOption = GETLONG(cmd.parameter);

			int nSimulationType = 0;				//no simulation
			if (nOption == 1) nSimulationType = SIMULATIONTYPE_MODEL;
			else if (nOption == 2) nSimulationType = SIMULATIONTYPE_STATE;
			else if (nOption == 3) nSimulationType = SIMULATIONTYPE_DATA;

			printf("[main] Program run, option %d\n", nOption);

			_state.SetSimulationType(nSimulationType);
			_state.SetSimulationStartTime(GetTime());

			if (_state.GetSimulationType() == 1) {
				_parser.GetVariable("_equ_Hover_Sim", &_equ_Hover, sizeof(_equ_Hover));
				_ctl.A2_equ = _equ_Hover;
			}
			else if (_state.GetSimulationType() == 0 || _state.GetSimulationType() == 2) {
				_parser.GetVariable("_equ_Hover", &_equ_Hover, sizeof(_equ_Hover));
				_ctl.A2_equ = _equ_Hover;
			}
			_main.Run();

			_cmm.SendMessage("Program quit");
		}
		sleep(1);
	}

	_cmm.SendMessage("System quit");
	_cmm.Close();
//	TraceEvent(_NTO_TRACE_STOP);
	printf("[main] quit\n");

	return 1;
}

void clsMain::Run()
{
	if (m_fThread & THREADFLAG_CMM) _cmm.StartThread(PRIORITY_CMM);
	if (m_fThread & THREADFLAG_IM8)	_im8.StartThread(PRIORITY_IM8);
//	if (m_fThread & THREADFLAG_DAQ) _daq.StartThread(PRIORITY_DAQ);
	if (m_fThread & THREADFLAG_CAM) _cam.StartThread(PRIORITY_CAM);
	if (m_fThread & THREADFLAG_CTL) _ctl.StartThread(PRIORITY_CTL);
	if (m_fThread & THREADFLAG_SVO) _svo.StartThread(PRIORITY_SVO);
	if (m_fThread & THREADFLAG_DLG) _dlg.StartThread(PRIORITY_DLG);

    m_chMain = ChannelCreate(0);

    sigevent event;
    event.sigev_notify = SIGEV_PULSE;
    event.sigev_coid = ConnectAttach(ND_LOCAL_NODE, 0, m_chMain, _NTO_SIDE_CHANNEL, 0);
    event.sigev_priority = getprio(0);
    event.sigev_code = PULSECODE_MAIN;

    itimerspec timer; timer_t idTimer;
    timer_create(CLOCK_REALTIME, &event, &idTimer);

 	timer.it_value.tv_sec = 0;
    timer.it_value.tv_nsec = PERIOD_MAIN*MILISECOND;				//wait for threads' initializaton work
    timer.it_interval.tv_sec = 0;
    timer.it_interval.tv_nsec = PERIOD_MAIN*MILISECOND;

#if (_DEBUG)
#if (_DEBUG_PERIOD >= 1000)
	timer.it_value.tv_sec = 1;
    timer.it_value.tv_nsec = 0;
    timer.it_interval.tv_sec = 1;
    timer.it_interval.tv_nsec = 0;
#else
	timer.it_value.tv_sec = 0;
    timer.it_value.tv_nsec = _DEBUG_PERIOD*MILISECOND;
    timer.it_interval.tv_sec = 0;
    timer.it_interval.tv_nsec = _DEBUG_PERIOD*MILISECOND;
#endif
#endif

    timer_settime(idTimer, 0, &timer, NULL);

    //for timeout setting, the timeout will be used in waiting task thread
    //the timeout waiting is in milli-second precise and begin from a minimum value of 2 ms
    //e.g. a nsec = n*1000000 timeout setting will yeild a 2+n ms waiting
	uint64_t nsecIM8 = PERIOD_IM8*MILISECOND;
//	uint64_t nsecDAQ = PERIOD_DAQ*MILISECOND;
	uint64_t nsecCAM = PERIOD_CAM*MILISECOND;
	uint64_t nsecCTL = PERIOD_CTL*MILISECOND;
	uint64_t nsecSVO = PERIOD_SVO*MILISECOND;
	uint64_t nsecCMM = PERIOD_CMM*MILISECOND;
	uint64_t nsecDLG = PERIOD_DLG*MILISECOND;

	_pulse pulse;
	for (m_nCount=0; ; m_nCount++) {
        MsgReceivePulse(m_chMain, &pulse, sizeof(_pulse), NULL);

        if (_cps != 13000000)
        	printf("[Main] _cps corrupted\n");

		TIMECOST tc = {0};
		double ts = ::GetTime();

		COMMAND cmd;
		_user.GetCommand(&cmd);
		if (cmd.code == 0) _cmm.GetCommand(&cmd);

		if (cmd.code != 0) {
			if (cmd.code == COMMAND_QUIT) break;
			if (!ProcessCommand(&cmd))
			{
/*				if (cmd.code == COMMAND_FORMATION || cmd.code == COMMAND_COOP_PKT)
					_coop.PutCoopPkt(cmd);
				else _ctl.PutCommand(&cmd);*/
				_ctl.PutCommand(&cmd);
//				_svo.PutCommand(&cmd);
			}
		}

		if ((m_fThread & THREADFLAG_IM8) && _im8.IsReady()) {
	        double t1 = ::GetTime();
			_im8.SendPulse(PRIORITY_IM8, PULSECODE_DATA);
			_im8.WaitForEvent(nsecIM8);
			tc.tIMU = ::GetTime()-t1;
		}

/*		if ((m_fThread & THREADFLAG_IM9) && _im9.IsReady()) {
	        double t1 = ::GetTime();
			_im9.SendPulse(PRIORITY_IM9, PULSECODE_DATA);
			_im9.WaitForEvent(nsecIM9);
			tc.tIMU = ::GetTime()-t1;
		}

		if ((m_fThread & THREADFLAG_DAQ) && _daq.IsReady()) {
	        double t1 = ::GetTime();
			_daq.SendPulse(PRIORITY_DAQ, PULSECODE_DATA);
			_daq.WaitForEvent(nsecDAQ);
			tc.tDAQ = ::GetTime()-t1;
		}*/



		if ((m_fThread & THREADFLAG_CAM) && _cam.IsReady()) {
	        double t1 = ::GetTime();
			_cam.SendPulse(PRIORITY_CAM, PULSECODE_DATA);
			_cam.WaitForEvent(nsecCAM);
			tc.tCAM = ::GetTime()-t1;
		}

		if ((m_fThread & THREADFLAG_CTL) && _ctl.IsReady()) {
	        double t1 = ::GetTime();
			_ctl.SendPulse(PRIORITY_CTL, PULSECODE_DATA);
			_ctl.WaitForEvent(nsecCTL);
			tc.tCTL = ::GetTime()-t1;
		}

		if ((m_fThread & THREADFLAG_SVO) && _svo.IsReady()) {
	        double t1 = ::GetTime();
			_svo.SendPulse(PRIORITY_SVO, PULSECODE_DATA);
			_svo.WaitForEvent(nsecSVO);
			tc.tSVO = ::GetTime()-t1;
		}

		if ((m_fThread & THREADFLAG_CMM) && _cmm.IsReady()) {				//always active
	        double t1 = ::GetTime();
			_cmm.SendPulse(PRIORITY_CMM, PULSECODE_DATA);
			_cmm.WaitForEvent(nsecCMM);
			tc.tCMM = ::GetTime()-t1;
		}

		if ((m_fThread & THREADFLAG_DLG) && _dlg.IsReady()) {
	        double t1 = ::GetTime();
			_dlg.SendPulse(PRIORITY_DLG, PULSECODE_DATA);
			_dlg.WaitForEvent(nsecDLG);
			tc.tDLG = ::GetTime()-t1;
		}
		tc.tMain = ::GetTime()-ts;

		uint64_t nsecTC = 2*MILISECOND;
	    TimerTimeout(CLOCK_REALTIME, _NTO_TIMEOUT_CONDVAR | _NTO_TIMEOUT_MUTEX, NULL, &nsecTC, NULL);
/*		if (pthread_mutex_lock(&m_mtxTC) == 0) {
			if (m_nTC < MAX_TC) {
				m_tTC[m_nTC] = ts;
				m_tc[m_nTC++] = tc;
			}
			pthread_mutex_unlock(&m_mtxTC);
		}*/

/*		if (m_nCount % 50 == 0) {
			printf("[main time] %.3f\n", ::GetTime());
			system("pidin info");
		}*/
#if (_DEBUG & DEBUGFLAG_MAIN)
if (m_nCount % _DEBUG_COUNT == 0) {
		printf("[main] IMU %.3f ms\n[main] DAQ %.3f ms\n[main] CAM %.3f ms\n[main] CTL %.3f ms\n[main] SVO %.3f ms\n[main] CMM %.3f ms\n[main] DLG %.3f\n [main] main %.3f ms\n,",
			tc.tIMU*1000, tc.tDAQ*1000, tc.tCAM*1000, tc.tCTL*1000, tc.tSVO*1000, tc.tCMM*1000, tc.tDLG*1000, tc.tMain*1000);
}
#endif
}	// for loop ends here

	uint64_t nsec = (uint64_t)2000*MILISECOND;

	if (m_fThread & THREADFLAG_IM8) {
		_im8.SendPulse(PRIORITY_IM8, PULSECODE_EXIT);
		_im8.WaitForEvent(nsec);
	}

/*	if (m_fThread & THREADFLAG_IM9) {
		_im9.SendPulse(PRIORITY_IM9, PULSECODE_EXIT);
		_im9.WaitForEvent(nsec);
	}

	if (m_fThread & THREADFLAG_DAQ) {
		_daq.SendPulse(PRIORITY_DAQ, PULSECODE_EXIT);
		_daq.WaitForEvent(nsec);
	}*/

	if (m_fThread & THREADFLAG_CTL) {
		_ctl.SendPulse(PRIORITY_CTL, PULSECODE_EXIT);
		_ctl.WaitForEvent(nsec);
	}

	if (m_fThread & THREADFLAG_SVO) {
		_svo.SendPulse(PRIORITY_SVO, PULSECODE_EXIT);
		_svo.WaitForEvent(nsec);
	}

	if (m_fThread & THREADFLAG_CMM) {
		_cmm.SendPulse(PRIORITY_CMM, PULSECODE_EXIT);
		_cmm.WaitForEvent(nsec);
	}

	if (m_fThread & THREADFLAG_DLG) {
		_dlg.SendPulse(PRIORITY_DLG, PULSECODE_EXIT);
		_dlg.WaitForEvent(nsec);
	}
}

BOOL clsMain::ProcessCommand(COMMAND *pCommand)
{
	char code = pCommand->code;
//	char *para = pCommand->parameter;

	BOOL bProcess = TRUE;
	switch (code) {
	case COMMAND_FILTER: {
		_state.ToggleFilter();
		break; }
	default: bProcess = FALSE;
	}

	return bProcess;
}

clsMain::clsMain()
{
	m_nTC = 0;

	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutex_init(&m_mtxTC, &attr);
}

clsMain::~clsMain()
{
	printf("[clsMain] destruct\n");
}

void clsMain::Init()
{
	double helicopter;
	_parser.GetVariable("_HELICOPTER", &helicopter);
	_HELICOPTER = (int)helicopter;

	_parser.GetVariable("_equ_Hover", &_equ_Hover, sizeof(_equ_Hover));
	_parser.GetVariable("_gravity", &_gravity);
	_parser.GetVariable("_radius", &_radius);

	ID_QUADLION = _HELICOPTER;
	m_fThread = THREADFLAG_ALL & ~THREADFLAG_CAM;				//no camera for general flight test

	if (_HELICOPTER == ID_QUADLION) {
		m_fThread &= ~(THREADFLAG_IM9 | THREADFLAG_IM7 | THREADFLAG_IM6 | THREADFLAG_DAQ | THREADFLAG_URG);
		m_fThread |= THREADFLAG_CAM;
	}
}
