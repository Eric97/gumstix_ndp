//state.h
//this is the header file for helicopter state update and model simulation
#ifndef STATE_H_
#define STATE_H_

#include "uav.h"
#include "cam.h"
#include "svo.h"
#include "im8.h"
#include "ctl.h"
#include "opencv2/opencv.hpp"
#include "navKF/myKalman.h"
#include "singleAxisFusion.h"
#include "mavlink/v1.0/common/mavlink.h"

using namespace cv;

class clsVision;
//simulation
#define SIMULATIONTYPE_MODEL			1
#define SIMULATIONTYPE_STATE			2
#define SIMULATIONTYPE_DATA				3

//#define _SIMULATIONPERTURBANCE				//simulate the perturbance caused by body trembling and other factor like wind
#define _SIMULATIONODE4				//flag of Runge-Kutta algorithm in simulation

//defines for nonlinear model
#define NONLINEAR_MODEL
#define NONLINEARMODEL_PARAS 	15
#define CONTROL_PARAS			4
#define WIND_PARAS				3

#define MAX_STATE		128				//storage size for uav state

//filter class is defined to filter position signals of y ans z
//one filter for one channel
class clsFilter {
public:
	clsFilter();

public:
	void Reset() { t = -1; }
	void Update(double tState, double xm, double um);
	double GetPositionEstimation() { return xmfv; }
	double GetVelocityEstimation() { return ue; }

//parameters
protected:
	double Reta;
	double Reps[2][2];

protected:
	double t;				//time
	double xi;				//integration of velocity(measurement)

	double ee;				//error estimation, error = xi - xm
	double bee;				//bias estimation

	double P[2][2];			//covariance matrices
	double Q[2][2];

	double ue;				//velocity estimation (velocity output)

	double xei;				//integration of estimated velocity (ve)
	double xeif;			//filter xei

	double xmf;				//filter xm (measurement)

	double xif;				//filter xi;

	double xmfv;			//verified xmf (position output)
};

struct RPTSTATE
{
	double ug, vg, wg;
	double xg, yg, zg;
	double xn, yn, zn;
};

class clsState {
public:
	clsState();
	~clsState();

	void Init();

protected:
	int m_nCount;				//counter

	//the current state after filtering
	double m_tState0;
	UAVSTATE m_state0;
	CHKSTATE m_chkState;

	//first updated
	double m_tState1;				//corresponding time
	UAVSTATE m_state1;				//state used for filter
	UAVSTATE m_statePX4;	///< state from PX4
	UTCTIME m_utcTime;		/*!< UTC time from GPS signal */

	clsFilter m_fx;				//filter for x direction
	clsFilter m_fy;				//filter for y direction
	clsFilter m_fz;				//filter for z direction

//	clsVision m_visionFilter;	// kalman filter for m_state0
	//flag for filter
	BOOL m_bFilter;
	BOOL m_bVision;
	BOOL m_bVisionMeasureUpdate, m_bVisionInterval;
	BOOL m_bStateFromVision;
	CAMTELEINFO m_camInfo;
	double X_n_phi_UAV[4];
	double X_n_theta_UAV[4];
protected:
	double _m_Kalman_A_WF[6][6]; 	clsMatrix m_Kalman_A_WF;
	double _m_Kalman_B_WF[6][3]; 	clsMatrix m_Kalman_B_WF;
	double _m_Kalman_C_WF[3][6]; 	clsMatrix m_Kalman_C_WF;
	double _m_Kalman_Q_WF[3][3]; 	clsMatrix m_Kalman_Q_WF;
	double _m_Kalman_R_WF[3][3]; 	clsMatrix m_Kalman_R_WF;
	double _m_Kalman_P_WF[6][6]; 	clsMatrix m_Kalman_P_WF;
	double _m_BQBt_WF[6][6];		clsMatrix m_BQBt_WF;
	double _m_xVision_WF[6];		clsVector m_xVision_WF;

	double _m_Kalman_A_ZAxis[2][2]; clsMatrix m_Kalman_A_ZAxis;
	double _m_Kalman_B_ZAxis[2][1]; clsMatrix m_Kalman_B_ZAxis;
	double _m_Kalman_C_ZAxis[1][2]; clsMatrix m_Kalman_C_ZAxis;
	double _m_Kalman_Q_ZAxis[1][1]; clsMatrix m_Kalman_Q_ZAxis;
	double _m_Kalman_R_ZAxis[1][1]; clsMatrix m_Kalman_R_ZAxis;
	double _m_Kalman_P_ZAxis[2][2]; clsMatrix m_Kalman_P_ZAxis;
	double _m_BQBt_ZAxis[2][2];		clsMatrix m_BQBt_ZAxis;
	double _m_x_ZAxis[2];		clsVector m_x_ZAxis;

public:
	void EnableFilter(BOOL bFilter = TRUE) {
		m_bFilter = bFilter;
		if (!m_bFilter) {
			m_fx.Reset(); m_fy.Reset(); m_fz.Reset();
		}
	}
	BOOL ToggleFilter() { EnableFilter(!m_bFilter); return m_bFilter; }

	BOOL GetVisionIntervalFlag() { return m_bVisionInterval; }
	void SetVisionIntervalFlag() { m_bVisionInterval = TRUE; }
	void ResetVisionIntervalFlag() { m_bVisionInterval = FALSE; }
	void SetVisionMeasurementUpdateFlag() { m_bVisionMeasureUpdate = TRUE; }
	BOOL GetVisionMeasurementUpdateFlag() const { return m_bVisionMeasureUpdate; }
	void ResetVisionMeasurementUpdateFlag()	{ m_bVisionMeasureUpdate = FALSE; }

	BOOL GetStateFromVisionFlag() const { return m_bStateFromVision; }
	void SetStateFromVisionFlag() { m_bStateFromVision = TRUE; }

protected:
	//to store state
	double m_tState[MAX_STATE];
	UAVSTATE m_state[MAX_STATE];
	int m_nState;

	// to store vision estimation state
	double m_tVisionState[MAX_STATE];
	int m_nVisionState;

	pthread_mutex_t m_mtxState, m_mtxVisionState;

	double m_tSIG0;
	HELICOPTERRUDDER m_sig0, m_semiSig, m_manualSig;

	double m_camera;				//deflection angle of the camera (laser gun)
	CAM_PANTILT m_camPanTilt;

	int m_nSIG;
	double m_tSIG[MAX_SIG];
	HELICOPTERRUDDER m_sig[MAX_SIG];

	pthread_mutex_t m_mtxSIG;

	EVENT m_event;

public:
	void SetEvent(int code) { m_event.code = code; }
	void SetEvent(int code, int para) { m_event.code = code; (int &)m_event.info[0] = para; }
	void ClearEvent() { m_event.code = 0; }
	EVENT &GetEvent() { return m_event; }

protected:
	BOOL m_bCoordinated;
	double m_longitude0, m_latitude0, m_altitude0, m_pressure0;
	double m_leaderLon0, m_leaderLat0, m_leaderAlt0;	// leader's gps origin position
	BOOL m_bGPSSent;
	BOOL m_bEulerInited;

protected:
	double m_c0;
//	double m_gvel[3], m_gpos[3];
	RPTSTATE m_RPTState, m_RPTState0;
	BOOL m_bMeasureUpdate;
	UAVSTATE m_KalmanState;

public:
	void Setc0(double c) { m_c0 = c; }
	double Getc0()	{ return m_c0; }
	void SetRPTState0();		// set the RPT state: Pg, Vg given the c0 when cmd is issued
	void SetRPTState();
	UAVSTATE &GetState() { return m_state0; }
	CHKSTATE &GetChkState() { return m_chkState; }
	RPTSTATE &GetRPTState() { return m_RPTState; }
	RPTSTATE &GetRPTState0()	{ return m_RPTState0; }
	UTCTIME	&GetUTCTime()	{ return m_utcTime; }

	double GetStateTime() { return m_tState0; }
	UAVSTATE &GetStateHeightFusion();	// incorporate the height fusion (from GPS,Ultrasonic) into new UAV state

	UAVSTATE &GetState1() { return m_state1; }				//unfiltered

	void Update(double tPack, IM8PACK *pPack);
	void Update1(double tPack, IM8PACK *pPack);

	void UpdateFromPX4(double tPack);
	void Update1FromPX4(double tPack);
	void Filter();				//filter processing after update1
//	BOOL VisionFusion();		// data fusion with vision output
	void Coordinate(double longitude, double latitude, double altitude) {
		m_longitude0 = longitude; m_latitude0 = latitude; m_altitude0 = altitude;
	}
	void ResetCoordinate(double longitude, double latitude) {
		m_longitude0 = longitude; m_latitude0 = latitude;
	}
	void SetInitPressure(uint32_t pressure0) { m_pressure0 = pressure0; }
	double CalculateHeightViaBaro(double pressure);

	BOOL KalmanTimeUpdate(UAVSTATE &uavstate, const UAVSTATE KalmanState);
	BOOL KalmanMeasurementUpdate(UAVSTATE &uavstate, const UAVSTATE KalmanState);
	BOOL KalmanTimeUpdateZAxis(UAVSTATE &uavstate, const UAVSTATE KalmanState);
	BOOL KalmanMeasurementUpdateZAxis(UAVSTATE &uavstate, const UAVSTATE KalmanState);

	void SetbMeasurementUpdate() { m_bMeasureUpdate = TRUE; }
	void GetCoordination(double *plongitude, double *platitude) {
		*plongitude = m_longitude0; *platitude = m_latitude0;
	}

	void GetCoordination(double *plongitude, double *platitude, double *paltitude) {
		*plongitude = m_longitude0; *platitude = m_latitude0; *paltitude = m_altitude0;
	}

	double GetSIGTime() { return m_tSIG0; }
	HELICOPTERRUDDER &GetSIG() { return m_sig0; }
	void GetSIG(HELICOPTERRUDDER *pSIG) { *pSIG = m_sig0; }
	void GetSemiSig(HELICOPTERRUDDER *pSIG) { *pSIG = m_semiSig; }
	void GetManualSig(HELICOPTERRUDDER *pSIG) { *pSIG = m_manualSig; }
	void UpdateSIG(HELICOPTERRUDDER *pSIG);
	void SetCameraPanTilt(CAM_PANTILT camPanTilt) { m_camPanTilt = camPanTilt; }
	CAM_PANTILT &GetCamPanTilt() { return m_camPanTilt; }
	void SetCamera(double camera) { m_camera = camera; }
	double GetCamera() { return m_camera; }

	BOOL Emergency();
	static BOOL Valid(UAVSTATE *pState);				//check the validity of helicopter state

protected:
	double m_tObserve;
	double _m_xe[3];		clsVector m_xe;

	//discrete
	double _m_Ad[3][3];		clsMatrix m_Ad;
	double _m_Bd[3][12];	clsMatrix m_Bd;
	double _m_Cd[3][3];		clsMatrix m_Cd;
	double _m_Dd[3][12];	clsMatrix m_Dd;

	//continuous
public:
	double _m_Ae[3][3];		clsMatrix m_Ae;
	double _m_Be[3][9 /*12*/];	clsMatrix m_Be;
	double _m_Ce[3][3];		clsMatrix m_Ce;
	double _m_De[3][9 /*12*/];	clsMatrix m_De;

protected:
	double _m_Ae2[3][3];	clsMatrix m_Ae2;					//Ae2 = Ae^2
	double _m_Ae3[3][3];	clsMatrix m_Ae3;					//Ae3 = Ae^3

protected:
	double m_tObserve2;
	double _m_xr[4];		clsVector m_xr;

	double _m_Ar[4][4];		clsMatrix m_Ar;					//A matrix for observer
	double _m_Br[4][2];		clsMatrix m_Br;					//B matrix for observer

	double _m_Ar2[4][4];	clsMatrix m_Ar2;
	double _m_Ar3[4][4];	clsMatrix m_Ar3;				//Ar^3

public:
	clsVector &GetRudderState() { return m_xr; }

protected:
	int m_nSimulationType;
	double m_t0Simulation;

	UAVSTATE m_stateSimulation;

	EQUILIBRIUM m_equ;

	BOOL m_bIMUStuck, m_bGPSReset;
	CMM_NAV_STATE m_navState;

public:
	int GetSimulationType() { return m_nSimulationType; }
	void SetSimulationType(int nType) { m_nSimulationType = nType; }
	void SetSimulationStartTime(double t0) { m_t0Simulation = t0; }

	void SetEqu(EQUILIBRIUM equ) { m_equ = equ; }
	EQUILIBRIUM GetEqu() const { return m_equ; }

private:
	BOOL m_bLaserFilterInitialization;
	singleAxisFusion laserZaxisFilter;

public:
	SAFETY_STATUS m_safetyStatus;	///< system safety variable
	bool GetSafetyStatus() { return m_safetyStatus.bGPS && m_safetyStatus.bIMUData && m_safetyStatus.bIMURcv
								&& m_safetyStatus.bPixhawkData && m_safetyStatus.bPixhawkRcv; }
	SAFETY_STATUS &GetAllSafetyStatus() { return m_safetyStatus; }

	void LaserHeightFusion();
	double GetLaserEstiZ() { return laserZaxisFilter.GetEstiState().estx;};
	double GetLaserEstiWg() { return laserZaxisFilter.GetEstiState().estv;};
	double GetLaserEstuAzg() { return laserZaxisFilter.GetEstiState().esta;};

	void UpdatePX4IMUState(mavlink_highres_imu_t *px4Imu);
	void UpdatePX4PosState(mavlink_local_position_ned_t *px4Pos);
	void UpdatePX4AttState(mavlink_attitude_t *px4Att);
	void UpdatePX4GPS(mavlink_global_position_int_t *pGlobalPosInt);
	void UpdateSysStatus(uint16_t mainState, uint16_t navState, uint16_t statistics,
			uint16_t preflightCheck, uint16_t ver, uint16_t totalFlightTime, uint16_t flightTime)
	{
		uint16_t status = statistics;
		uint8_t armStatus = 0;
		if (status & 0x0001)
			armStatus = 1;
		else if (status & 0x0002)
			armStatus = 2;
		else if (status & 0x0003)
			armStatus = 3;

		m_navState.mainState = (uint8_t)mainState;
		m_navState.navState = (uint8_t)navState;
		m_navState.armState = armStatus;

		m_navState.preflightCheck = preflightCheck;
		m_navState.ver = ver;
		m_navState.totalFlightTime = totalFlightTime;
		m_navState.flightTime = flightTime;
	};

	void UpdatePX4UTCTime(uint8_t hour, uint8_t min, uint8_t second, int nSatellites)
	{
		m_utcTime.hour = hour; m_utcTime.minutes = min; m_utcTime.seconds = second;
		m_chkState.nSatellites = nSatellites;
	}

	void GetNEDAcceleration(double &acx, double &acy, double &acz,  double &a, double &b, double &c);
	CMM_NAV_STATE &GetNAVState() { return m_navState; }

	void SetIMUStuckFlag() { m_bIMUStuck = TRUE; }
	int GetIMUStuckFlag() const { return m_bIMUStuck; }

	BOOL GetGPSResetFlag() const { return m_bGPSReset; }
	void SetGPSResetFlag() { m_bGPSReset = TRUE; }
	void ResetGPSResetFlag() { m_bGPSReset = FALSE; }

public:
	void Simulate();

protected:
	void ModelSimulate();
	void StateSimulate();
	void StateSimulationFromPX4();
	void DataSimulate();

	void LinearModelSimulate();

	friend class clsDLG;

protected:
	double Rand();

protected:
	char *m_pData;				//to store loaded data;
	int m_nData;				//data size
	int m_iData;				//current index

public:
	BOOL LoadData(const char *pszFile);

private:
	KALMAN	m_clsNavKF;
};


#endif				//STATE_H_

