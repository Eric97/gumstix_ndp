/*
 *  AES Password Source File
 *  Copyright (C) 2015
 *  xdhuangcheng@gmail.com
 *
 *  Read/Write password from/into keyfile
 *
 *  password.c created day:2015-3-2
 *             last edited day:2015-3-3
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h> 
#include <errno.h>

#include "password.h"

/*********************************************
Function    : write_passwd_to_file
Description : save key into keyfile
Input       : passwd(password), passlen(passlen), outfile(filepath/filename)
Output      : None
Return      : 0 ok, -1 error
*********************************************/
int write_passwd_to_file(unsigned char *passwd, int passlen, const char *outfile)
{
	FILE *outfp = NULL;

	if ((outfp = fopen(outfile, "w")) == NULL)
    {
        fprintf(stderr, "Error opening password file %s : ", outfile);
        perror("");
        // For security reasons, erase the password
        memset(passwd, 0, passlen);
        return  -1;
    }

    if (fwrite(passwd, sizeof(unsigned char), passlen, outfp) != passlen)
    {
        fprintf(stderr, "Error: Could not write password file.\n");
        fclose(outfp);
        return  -1;
    }
    
    fclose(outfp);

    return 0;
}

/*********************************************
Function    : read_passwd_to_file
Description : read key from keyfile
Input       : passwd(password), passlen(passlen), outfile(filepath/filename)
Output      : None
Return      : 0 ok, -1 error
*********************************************/
int read_passwd_from_file(unsigned char *passwd, int passlen, const char *outfile)
{
	FILE *outfp = NULL;

	if ((outfp = fopen(outfile, "r")) == NULL)
    {
        fprintf(stderr, "Error opening password file %s : ", outfile);
        perror("");
        return  -1;
    }

    if (fread(passwd, sizeof(unsigned char), passlen, outfp) != passlen)
    {
        fprintf(stderr, "Error: Could not read password file.\n");
        fclose(outfp);
        return  -1;
    }

    fclose(outfp);

    return 0;
}