/*
 *  AES CFB 128 Crypto Header File
 *  Copyright (C) 2015
 *  xdhuangcheng@gmail.com
 *
 *  Encrypt/Decrypt data
 *
 *  aes_crypto.h created day:2015-3-3
 *               last edited day:2015-3-3
 */
#ifndef _AES_CRYPTO_H
#define _AES_CRYPTO_H

extern "C"{
	void aes_ctf128_encrypt(const char *keyfile, unsigned char *data, int datalen, unsigned char* secretdata);
	void aes_ctf128_decrypt(const char *keyfile, unsigned char *secretdata, int secretdatalen, unsigned char* data);
}
#endif /* aes_crypto.h */
