/*
 *  AES Password Header File
 *  Copyright (C) 2015
 *  xdhuangcheng@gmail.com
 *
 *  Read/Write password from/into keyfile
 *
 *  password.h created day:2015-3-2
 *             last edited day:2015-3-3
 */

#ifndef __AES_PASSWORD_H__
#define __AES_PASSWORD_H__

int write_passwd_to_file(unsigned char *passwd, int passlen, const char *outfile);
int read_passwd_from_file(unsigned char *passwd, int passlen, const char *outfile);

#endif