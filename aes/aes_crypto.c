/*
 *  AES CFB 128 Crypto Source File
 *  Copyright (C) 2015
 *  xdhuangcheng@gmail.com
 *
 *  Encrypt/Decrypt data
 *
 *  aes_crypto.c created day:2015-3-2
 *               last edited day:2015-3-3
 */
#include "aes.h"
#include "password.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*********************************************
Function    : aes_ctf128_encrypt
Description : encrypt byte array(unsigned char *)
Input       : keyfile(password file), data(Input data array), datalen(data arrat length)
Output      : secretdata (Output secretdata)
Return      : None
*********************************************/
void aes_ctf128_encrypt(const char *keyfile, unsigned char *data, int datalen, unsigned char* secretdata)
{
	int roundnum,i;
	aes_context aes_ctx;
 	unsigned char key[16];
 	unsigned char tmpdata[16],tmpsecretdata[16];
 	read_passwd_from_file(key, 16, keyfile);
 	aes_set_key(&aes_ctx, key, 128);

 	for(i=0;i<16;i++)
 	{
 		tmpdata[i]=key[i];
 	}

 	for(roundnum=0;roundnum<=datalen/16;roundnum++)
 	{
 		int thisroundatalength=0;
 		aes_encrypt(&aes_ctx, tmpdata, tmpsecretdata);
 		if((datalen-roundnum*16)<16)
 		{
 			thisroundatalength=datalen-roundnum*16;
 		}
 		else
 		{
 			thisroundatalength=16;
 		}

 		for(i=0;i<thisroundatalength;i++)
 		{
 			secretdata[roundnum*16+i]=data[roundnum*16+i]^tmpsecretdata[i];
 			tmpdata[i]=secretdata[roundnum*16+i];

 			/*
 			if(thisroundatalength<16)
 			{
 				printf("encrypt:");
 				printf("%x ",data[roundnum*16+i]);
 				printf("%x ",tmpsecretdata[i]);
 				printf("%x\n",secretdata[roundnum*16+i]);
 			}
 			*/
 		}
 	}
}

/*********************************************
Function    : aes_ctf128_decrypt
Description : decrypt byte array(unsigned char *)
Input       : keyfile(password file), secretdata(input secretdata array), secretdatadatalen(secretdata arrat length)
Output      : data (output data)
Return      : None
*********************************************/
void aes_ctf128_decrypt(const char *keyfile, unsigned char *secretdata, int secretdatalen, unsigned char* data)
{
	int roundnum,i;
	aes_context aes_ctx;
 	unsigned char key[16];
 	unsigned char tmpdata[16],tmpsecretdata[16];
 	read_passwd_from_file(key, 16, keyfile);
 	aes_set_key(&aes_ctx, key, 128);
 	
 	for(i=0;i<16;i++)
 	{
 		tmpdata[i]=key[i];
 	}

 	for(roundnum=0;roundnum<=secretdatalen/16;roundnum++)
 	{
 		int thisroundatalength=0;
 		aes_encrypt(&aes_ctx, tmpdata, tmpsecretdata);
 		if((secretdatalen-roundnum*16)<16)
 		{
 			thisroundatalength=secretdatalen-roundnum*16;
 		}
 		else
 		{
 			thisroundatalength=16;
 		}

 		for(i=0;i<thisroundatalength;i++)
 		{
 			data[roundnum*16+i]=secretdata[roundnum*16+i]^tmpsecretdata[i];
 			tmpdata[i]=secretdata[roundnum*16+i];

 			/*
 			if(thisroundatalength<16)
 			{
 				printf("decrypt:");
 				printf("%x ",secretdata[roundnum*16+i]);
 				printf("%x ",tmpsecretdata[i]);
 				printf("%x\n",data[roundnum*16+i]);
 			}
 			*/
 		}
 	}	
}