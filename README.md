This program is used for automatic flight control and navigation for multiple UAVs. The code is written with C++ in QNX 6.5.0, and implemented in OMAP3530 processor (Gumstix Overo Fire).

Current system configuration is RTK based path tracking, where multiple UAVs form a predefined pattern with LED lighting in the sky.

Contact: DONG Xiangxu (tsldngx@nus.edu.sg)
