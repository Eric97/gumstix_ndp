#ifndef DLG_H_
#define DLG_H_

#include <sys/types.h>
#include <sys/stat.h>
#include "daq.h"
#include "svo.h"
#include "im8.h"
#include "ctl.h"
#include "main.h"

//DLG
#if (_DEBUG)
#define COUNTLOG_IM6    _DEBUG_COUNT_1				//to test the dtl thread every loop
#define COUNTLOG_IM7    _DEBUG_COUNT_1				//to test the dtl thread every loop
#define COUNTLOG_DAQ    _DEBUG_COUNT_1
#define COUNTLOG_SVO	_DEBUG_COUNT_1
#define COUNTLOG_SIG	_DEBUG_COUNT_1
#define COUNTLOG_CTL	_DEBUG_COUNT_1
#define COUNTLOG_TC		_DEBUG_COUNT_1
#else
#define COUNTLOG_IM6	50
#define COUNTLOG_IM7	50
#define COUNTLOG_DAQ	50
#define COUNTLOG_SVO	50
#define COUNTLOG_SIG	50
#define COUNTLOG_CTL	50
#define COUNTLOG_TC	50
#endif

#if (_DEBUG)
#define COUNT_DLG	_DEBUG_COUNT_1
#else
#define COUNT_DLG	50
#endif

#define MAX_STATE	128
#define MAX_NO_LOGFILE	999

//DLG
#pragma pack(push, 1)
struct DLGHEADER {
	short header;
	char code;				//type of data
	double time;				//time of data
};

#pragma pack(pop)

class clsDLG : public clsThread {
public:
	clsDLG();
	~clsDLG();

public:
	virtual BOOL InitThread();
	virtual int EveryRun();
	virtual void ExitThread();
	BOOL FileExist(const char *filename)
	{
		struct stat buffer;
		return stat(filename, &buffer) == 0;
	}

protected:
	int			m_nIM8;
	double		m_tIM8[MAX_IM8PACK];
	IM8RAWPACK	m_im8Raw[MAX_IM8PACK];
	IM8PACK		m_im8[MAX_IM8PACK];

	int 		m_nState;
	double 		m_tState[MAX_STATE];
	UAVSTATE 	m_state[MAX_STATE];

	int			m_nVisionState;
	double		m_tVisionState[MAX_STATE];

	int			m_nDAQ;
	double		m_tDAQ[MAX_DAQ];
	DAQRAWDATA	m_daqRaw[MAX_DAQ];
	DAQDATA		m_daq[MAX_DAQ];

	int			m_nCTL;
	double		m_tCTL[MAX_CTL];
	CONTROLSTATE m_ctl[MAX_CTL];				//inner loop signal - {u,v,w,p,q,r,ug,vg,wg}

	int			m_nSIG;
	double		m_tSIG[MAX_SIG];
	HELICOPTERRUDDER m_sig[MAX_SIG];

	int 		m_nSVO;
	double 		m_tSVO[MAX_SVO];
	SVORAWDATA	m_svoRaw[MAX_SVO];
	SVODATA		m_svo[MAX_SVO];

	int m_nTC;
	double		m_tTC[MAX_TC];
	TIMECOST	m_tc[MAX_TC];

	char m_szFile[256];
	char m_szFileb[256];
	char m_szFilea[256];

	BOOL m_bBlackBox;
	FILE *m_pfLog, *m_pfLogb;
	FILE *m_pfLoga; 	// file pointer to laser.txt
};

#endif				//DLG_H_
